exports.connect = {
    'myLocalMySql': {
        'adapter': 'mysql', 
        'host': 'localhost',
        'port': 3306,
        'user': 'root',
        'password': '123456',
        'database': 'bosch_clean'
    },
    /*
    'myLocalRedis': {
        'adapter': 'redis',
        'port': 6379,
        'host': 'localhost',
    }
    */
};