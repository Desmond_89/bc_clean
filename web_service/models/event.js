var Waterline = require('waterline');
var ormError = require('../lib/handleOrmError');

var Event = Waterline.Collection.extend({
    'identity': 'event',
    'connection': 'myLocalMySql',
    'tableName': 'device_event', 
    'attributes': {
        'eventId': {
            'columnName': 'device_event_id',
            'type': 'integer', 
            'primaryKey': true,
            'unique': true
        },
        'deviceId': {
            'columnName': 'device_id',
            'type': 'string',
            'maxLength': 16
        },
        'event': {
            'columnName': 'event',
            'type': 'string',
            'maxLength': 16
        },
        'time': {
            'columnName': 'time',
            'type': 'string',
            'maxLength': 16
        },
        'arg': {
            'columnName': 'event_argument',
            'type': 'string',
            'maxLength': 64
        }
    },
    'autoPK': true, 
    'autoCreatedAt': false,
    'autoUpdatedAt': false
});

module.exports = Event;