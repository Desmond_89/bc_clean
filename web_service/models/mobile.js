var Waterline = require('waterline');
var ormError = require('../lib/handleOrmError');

var Mobile = Waterline.Collection.extend({
    'identity': 'mobile',
    'connection': 'myLocalMySql',
    'tableName': 'mobile', 
    'attributes': {
        'mobileId': {
            'columnName': 'mobile_id',
            'type': 'integer',
            'autoIncrement': true,
            'primaryKey': true,
            'unique': true
        }, 
        'mobileUUID': {
            'columnName': 'mobile_uuid',
            'type': 'string',
            'required': true,
            'maxLength': 64,
            'unique': true
        },
        'apnsToken': {
            'columnName': 'apns_token',
            'type': 'string',
            'maxLength': 64,
            // 'unique': true
        },
        'appLanguage': {
            'columnName': 'app_language',
            'type': 'string',
            'maxLength': 8,
        },
        'appVersion': {
            'columnName': 'app_version',
            'type': 'string',
            'maxLength': 8,
        }, 
        'lastUseTime': {
            'columnName': 'last_use_time',
            'type': 'integer'
        }
    },
    'autoPK': false, 
    'autoCreatedAt': false,
    'autoUpdatedAt': false,
    'updateOrCreate': function(input, callback){
        // check exist
        var model = this;
        model.findOne({
            mobileUUID: input.mobileUUID
        }, function(err, mobile){
            if (err) {
                callback(ormError.get(err));
            }
            else {
                if (mobile !== undefined) {
                    // update
                    model.update({
                        mobileUUID: input.mobileUUID
                    },
                    input,
                    function(err2, newMobiles){
                        if (err2) {
                            callback(ormError.get(err2));
                        }
                        else {
                            callback(null, newMobiles.pop());
                        }
                    });
                }
                else{
                    // create
                    model.create(input, function(err3, newMobile){
                        if (err3) {
                            callback(ormError.get(err3));
                        }
                        else {
                            callback(null, newMobile);
                        }
                    });
                }
            }
        });
    }
});

module.exports = Mobile;