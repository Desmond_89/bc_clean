var Waterline = require('waterline');
var ormError = require('../lib/handleOrmError');
var appConfig = require('../config/app').config;
var async = require('async');

var Relation = Waterline.Collection.extend({
    'identity': 'relation',
    'connection': 'myLocalMySql',
    'tableName': 'relationship', 
    'attributes': {
        'relationId': {
            'columnName': 'relation_id',
            'type': 'integer',
            'autoIncrement': true,
            'primaryKey': true,
            'unique': true
        }, 
        'user': {
            'columnName': 'user_id',
            'model': 'user',
        },
        'device': {
            'columnName': 'device_id',
            'model': 'device',
        },
        'isDefault': {
            'columnName': 'is_default',
            'type': 'integer'
        }
    },
    'autoPK': false, 
    'autoCreatedAt': false,
    'autoUpdatedAt': false,
    'resetWithUser': function(userId, callback){
        this.destroy({'user': userId})
        .exec(callback);
    },
    'checkAndCreate': function(userId, def, device, callback){
        var model = this;
        // check device add users
        model.find({
            'device': device.deviceId
        }, function(err, relations){
            if (err) {
                callback(ormError.get(err));
            }
            else {
                if (relations.length >= appConfig.deviceUsersCountLimit) {
                    // return already users list
                    var cString = device.deviceId + " already have 5 users: ";
                    var userArray = [];
                    for (var i in relations) {
                        var theId = relations[i]['user'];
                        userArray.push(theId);
                    }
                    async.mapSeries(
                        userArray, function(theUserId, cb){
                            // get username and nickname
                            process.app.models.user.findOne({userId: theUserId}, function(err, user){
                               cString += user['username'] + "(" + user['nickname'] + "), ";
                               cb(); 
                            });
                        },
                        function(err) {
                            callback(cString.substring(0, cString.length - 2) + ".");
                        });
                    // callback('you could not add device \'' + device.deviceId + '\' because over limiting value');
                }
                else {
                    // update
                    model.findOrCreate({
                        'user': userId,
                        'device': device.deviceId,
                        'isDefault': (device.deviceId == def) ? 1 : 0
                    })
                    .exec(function(err2, relation){
                        if (err2) {
                            callback(ormError.get(err2));
                        }
                        else {
                            callback(null, relation);
                        }
                    });
                }
            }
        });
    }
});

module.exports = Relation;