var Waterline = require('waterline');
var ormError = require('../lib/handleOrmError');
var moment = require('moment');

// make reset code
function makeCode(num){
    var str = '';
    for(var i=0; i<num; i++){
        str += Math.floor(Math.random() * 10);
    }
    return str;
}

var ResetCode = Waterline.Collection.extend({
    'identity': 'resetcode',
    'connection': 'myLocalMySql',
    'tableName': 'reset_code', 
    'attributes': {
        'username': {
            'columnName': 'username',
            'type': 'string',
            'primaryKey': true,
            'required': true,
            'maxLength': 64,
            'unique': true
        },
        'resetCode': {
            'columnName': 'reset_code',
            'type': 'string',
            'maxLength': 64
        },
        'time': {
            'columnName': 'time',
            'type': 'integer'
        },
    },
    'autoPK': false, 
    'autoCreatedAt': false,
    'autoUpdatedAt': false,
    'make': function(username, callback){
        // check exist
        var model = this;
        model.findOne({
            username: username
        }, function(err, code){
            if (code === undefined) {
                // create
                model.create({
                    username: username,
                    resetCode: makeCode(6),
                    time: moment().format('YYYYMMDDHHmmss')
                }, function(err2, resetcode){
                    if (err2) {
                        callback(ormError.get(err2));
                    }
                    else {
                        callback(null, resetcode);
                    }
                });
            }
            else {
                // update
                model.update({
                    username: username
                }, {
                    resetCode: makeCode(6),
                    time: moment().format('YYYYMMDDHHmmss')
                }, function(err3, resetcodes){
                    if (err3) {
                        callback(ormError.get(err3));
                    }
                    else {
                        callback(null, resetcodes.pop());
                    }
                });    
            }
        });
    }
});

module.exports = ResetCode;