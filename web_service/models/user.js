var Waterline = require('waterline');
var uuid = require('node-uuid');
// var logger = require('../lib/log');
var ormError = require('../lib/handleOrmError');

var User = Waterline.Collection.extend({
    'identity': 'user',
    'connection': 'myLocalMySql',
    'tableName': 'user', 
    'attributes': {
        'userId': {
            'columnName': 'user_id',
            'type': 'integer',
            'autoIncrement': true,
            'primaryKey': true,
            'unique': true
        }, 
        'username': {
            'columnName': 'username',
            // 'type': 'email',
            'type': 'string',
            'required': true,
            'maxLength': 64,
            'unique': true
        },
        'password': {
            'columnName': 'password',
            'type': 'string',
            'required': true,
            'maxLength': 128
        },
        'nickname': {
            'columnName': 'nickname',
            'type': 'string',
            'maxLength': 64
        },
        'age': {
            'columnName': 'age',
            'type': 'integer'
        },
        'gender': {
            'columnName': 'gender',
            'type': 'integer'
        },
        'avatarUrl': {
            'columnName': 'avatar_url',
            'type': 'string',
            'maxLength': 128
        },
        'address': {
            'columnName': 'address',
            'type': 'string',
            'maxLength': 128
        },
        'userType': {
            'columnName': 'user_type',
            'type': 'integer'
        },
        'apiKey': {
            'columnName': 'api_key',
            'type': 'string',
            'maxLength': 36,
            'unique': true
        },
        'lastUseTime': {
            'columnName': 'last_use_time',
            'type': 'integer'
        },
        'available': {
            'columnName': 'available',
            'type': 'integer'
        },
        // to Current Mobile
        'mobile': {
            'columnName': 'mobile_id',
            model: 'mobile'
        }, 
        // to Devices relationship 
        'relation': {
            'collection': 'relation',
            'via': 'user'
        }
    },
    'autoPK': false, 
    'autoCreatedAt': false,
    'autoUpdatedAt': false,
    'signin': function(input, callback){
        // check exist
        var model = this;
        model.findOne({
            username: input.username
        }, function(err, user){
            if (user !== undefined) {
                if (user.available) {
                    // repetition signin error
                    callback('\'' + user.username + '\' already exists');
                }
                else {
                    // unavailable account - reset
                    model.update({
                        username: input.username,
                    }, {
                        password: input.password,
                        available: 1,    // true
                        apiKey: uuid.v4()
                    }, function(err2, newUsers){
                        if (err2) {
                            callback(ormError.get(err2));
                        }
                        else {
                            callback(null, newUsers.pop());
                        }
                    });
                }
            }
            else {
                // no exist - create new
                model.create({
                    username: input.username,
                    password: input.password,
                    userType: 1,    // default app user
                    available: 1,   // true
                    apiKey: uuid.v4()
                }, function(err3, newUser){
                    if (err3) {
                        callback(ormError.get(err3));
                    }
                    else {
                        callback(null, newUser);
                    }
                });
            }
        });
    }, 
    'login': function(input, callback){
        var model = this;
        // find user by username
        this.findOne({
            'username': input.username
        }, function(err, user) {
            if (user === undefined) {
                // user not exist
                callback('\'' + input.username + '\' is not exist!');
            }
            else {
                if (user.available) {
                    // validate Password
                    if (user.password === input.password) {
                        // verify ok - make token and save
                        model.update({
                            'userId': user.userId
                        }, {
                            'apiKey': uuid.v4()
                        }, function(err2, newUsers) {
                            if (err2) {
                                callback(ormError.get(err));
                            }
                            else {
                                callback(null, newUsers.pop());
                            }
                        });
                    }
                    else {
                        // verify failed
                        callback('Incorrect Password!');
                    }
                }
                else {
                    // account unavailable
                    callback('\'' + input.username + '\' is not exist!');
                }
            }
        });
    },
    'loginWeChat': function(input, callback){
        var model = this;
        // find user by username
        this.findOne({
            username: input.username
        }, function(err, user){
            if (user === undefined) {
                // user not exist and create one
                model.create({
                    username: input.username,
                    password: input.password,
                    userType: input.user_type,
                    available: 1,   // true
                    apiKey: uuid.v4()
                }, function(err2, newUser){
                    if (err2) {
                        callback(ormError.get(err2));
                    }
                    else {
                        callback(null, newUser);
                    }
                });
            }
            else {
                // update api key
                model.update({
                    'userId': user.userId
                }, {
                    'apiKey': uuid.v4()
                }, function(err2, newUsers) {
                    if (err2) {
                        callback(ormError.get(err2));
                    }
                    else {
                        callback(null, newUsers.pop());
                    }
                });
            }
        });
    }, 
    'logout': function(username, callback){
        this.update({
            username: username
        },{
            apiKey: null
        }, function(err) {
            if (err) {
                callback(ormError.get(err));
            }
            else {
                callback(null);
            }
        });
    }, 
    'validateToken': function(token, callback){
        // find user by token
        this.findOne({
            'apiKey': token
        }, function(err, user) {
            if (err) {
                callback(ormError.get(err));
            }
            else {
                if (user === undefined) {
                    // no login user
                    callback('Unauthorized');
                }
                else {
                    callback(null, user.username);
                }
            }
        });
    }
});

module.exports = User;