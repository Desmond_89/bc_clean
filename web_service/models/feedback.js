var Waterline = require('waterline');
var ormError = require('../lib/handleOrmError');

var Feedback = Waterline.Collection.extend({
    'identity': 'feedback',
    'connection': 'myLocalMySql',
    'tableName': 'feedback', 
    'attributes': {
        'feedbackId': {
            'columnName': 'feedback_id',
            'type': 'integer',
            'autoIncrement': true,
            'primaryKey': true,
            'unique': true
        }, 
        'user': {
            'columnName': 'user_id',
            'model': 'user',
        },
        'device': {
            'columnName': 'device_id',
            'model': 'device',
        },
        'message': {
            'columnName': 'message',
            'type': 'string',
            'maxLength': 256,
        },
        'timestamp': {
            'columnName': 'timestamp',
            'type': 'integer'
        }
    },
    'autoPK': false, 
    'autoCreatedAt': false,
    'autoUpdatedAt': false
});

module.exports = Feedback;