var Waterline = require('waterline');
var ormError = require('../lib/handleOrmError');

var Device = Waterline.Collection.extend({
    'identity': 'device',
    'connection': 'myLocalMySql',
    'tableName': 'device', 
    'attributes': {
        'deviceId': {
            'columnName': 'device_id',
            'type': 'string',
            'maxLength': 16, 
            'primaryKey': true,
            'unique': true
        },
        'deviceName': {
            'columnName': 'device_name',
            'type': 'string',
            'maxLength': 32
        },
        'hardware': {
            'columnName': 'hardware_version',
            'type': 'string',
            'maxLength': 16
        },
        'software': {
            'columnName': 'software_version',
            'type': 'string',
            'maxLength': 16
        },
        'smart': {
            'columnName': 'smart_learning',
            'type': 'integer'
        },
        'aqiUnit': {
            'columnName': 'aqi_unit',
            'type': 'string',
            'maxLength': 16
        },
        'tempUnit': {
            'columnName': 'temp_unit',
            'type': 'string',
            'maxLength': 16
        },
        'country': {
            'columnName': 'country',
            'type': 'string',
            'maxLength': 32
        }, 
        'alarmLevel': {
            'columnName': 'alarm_level',
             'type': 'string',
            'maxLength': 16
        },
        'alarmAqi': {
            'columnName': 'alarm_aqi',
            'type': 'integer'
        },
        'noDisturb': {
            'columnName': 'no_disturb',
            'type': 'string',
            'maxLength': 512
        },
        'dndStart': {
            'columnName': 'dnd_start',
            'type': 'time',
        },
        'dndEnd': {
            'columnName': 'dnd_end',
            'type': 'time'
        },
        'brightness': {
            'columnName': 'brightness',
            'type': 'integer'
        },
        'lightingTime': {
            'columnName': 'lighting_time',
            'type': 'integer'
        },
        'volume': {
            'columnName': 'volume',
            'type': 'integer'
        },
        'lastComm': {
            'columnName': 'last_comm',
            'type': 'integer'
        },
        'city': {
            'columnName': 'city',
            'type': 'string',
            'maxLength': 32
        },
        'wifi': {
            'columnName': 'wifi',
            'type': 'string',
            'maxLength': 32
        }, 
        // one to many
        'relation': {
            'collection': 'relation',
            'via': 'device'
        }
    },
    'autoPK': false, 
    'autoCreatedAt': false,
    'autoUpdatedAt': false,
    'updateOrCreate': function(input, callback){
        var model = this;
        if (input.deviceId === undefined) {
            callback('no device id');
        }
        else {
            // find device exist
            model.findOne({
                deviceId: input.deviceId
            }, function(err, device){
                if (err) {
                    callback(ormError.get(err));
                }
                else {
                    if (device !== undefined) {
                        // exist - update device info
                        model.update({
                            deviceId: device.deviceId
                        }, input, function(err2, newDevices){
                            if (err2) {
                                callback(ormError.get(err2));
                            }
                            else {
                                callback(null, newDevices.pop());
                            }
                        });
                    }
                    else {
                        // no exist - create new
                        model.create(input, function(err3, newDevice){
                            if (err3) {
                                callback(ormError.get(err3));
                            }
                            else {
                                callback(null, newDevice);
                            }
                        });
                    }
                }
            });
        }
        
    }
});

module.exports = Device;