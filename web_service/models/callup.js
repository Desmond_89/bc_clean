var Waterline = require('waterline');
var ormError = require('../lib/handleOrmError');

var Callup = Waterline.Collection.extend({
    'identity': 'callup',
    'connection': 'myLocalMySql',
    'tableName': 'call_up', 
    'attributes': {
        'callUpId': {
            'columnName': 'call_up_id',
            'type': 'integer', 
            'primaryKey': true,
            'unique': true
        },
        'deviceId': {
            'columnName': 'device_id',
            'type': 'string',
            'maxLength': 16
        },
        'callUpType': {
            'columnName': 'call_up_type',
            'type': 'string',
            'maxLength': 16
        },
        'callUpTime': {
            'columnName': 'call_up_time',
            'type': 'string',
            'maxLength': 16
        }
    },
    'autoPK': true, 
    'autoCreatedAt': false,
    'autoUpdatedAt': false
});

module.exports = Callup;