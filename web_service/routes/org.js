var header = require('../lib/header.js');
var async = require('async');
var format = require('../lib/formatData');
var fs = require('fs');
var mime = require('mime');

var AVATAR = 'picture';

function getPostItem(req, res, item) {
    if (req.body === undefined) {
        res.send(400, new Error('No Post Body!'));
    }
    var result = req.body[item];
    if (result === undefined) {
        res.send(400, new Error('No \'' + item + '\' in Post Body!'));
    }
    return result;
}

function mapArray(inputArr, matching){
    var result = new Array;
    for(var i in inputArr){
        var item = inputArr[i].DevInfo;
        // console.log(item);
        if (item === undefined) {
            return {'err': 'Invalid Format'};
        }
        else {
            var obj = {};
            for (var key in matching){
                if (item[key] !== undefined) {
                    obj[matching[key]] = item[key];
                }
            }
            result.push(obj);
        }
    }
    // console.log(result);
    return {'err': null, 'result': result};
}

function validateApiKey(req, res, user, callback) {
    var token = req.header('API-KEY');
    if (token === undefined) {
        res.send(400, new Error('No API-KEY!'));
    }
    else {
        user.validateToken(token, function(err, model){
            if (model === undefined) {
                res.send(400, new Error('Unauthorized'));
            }
            else {
                callback(null, model);
            }
        });
    }
}

function handleOrmError(err) {
    var result = new Array;
    for (var item in err.invalidAttributes){
        var err_item = err.invalidAttributes[item];
        // console.log(err_item);
        for(var key in err_item){
            var err_key = err_item[key];
            // console.log(err_key);
            var rule = err_key.rule;
            var value = err_key.value;
            switch(rule){
                case 'unique': {
                    result.push(value + ' already exists');
                } 
                break;
                case 'email': {
                    result.push('\'username\' should be email');
                } 
                break;
                case 'maxLength': {
                    result.push('Input is too long');
                }
                case 'integer': {
                    result.push('Input should be integer');
                }
                break;
                default:
                    result.push(value + ' break the rule ' + rule);
            }
        }
    }
    return result;
}
var handlerList = [
    'accountHandler',
    'Bhandler',
    'anotherHandler'
                   ];
function registAllHanders(server, handlerList) {
    for (module in handlerList) {
        var h = require('./'+module);
        registHandler(server, h);
    }
}
var account = require('./accountHandler');

function registHandler(server, hander){
    for(url in hander.keys()) {
        for(method in handler[url].keys()){
            server[method](url, handler[url][method]);
        }
    }
}

module.exports = function(server) {
    
    // hello world
    server.get('/hello', function(req, res){
        res.send({'err': null});
    });
    
    // signin
    server.post('/api/account', function(req, res){
        var headerAuth = header.headerCheck(req);
        if (headerAuth.err) {
            res.send(400, new Error(headerAuth.err));
        }
        else {
            // get post            
            var username = getPostItem(req, res, 'username');
            var password = getPostItem(req, res, 'password');
            // save account
            server.models.user.create({
                'username': username,
                'password': password
            }, function(err, model) {
                if(err){
                   var errerMessage = handleOrmError(err);
                   res.send(400, new Error(errerMessage));
                }
                else {
                    res.send({'userid': model.username});
                }
            });
        }
    });
    
    // change password
    server.put('/api/account', function(req, res){
        var headerAuth = header.headerCheck(req);
        if (headerAuth.err) {
            res.send(400, new Error(headerAuth.err));
        }
        else {
            var userModel = server.models.user;
            validateApiKey(req, res, userModel, function(err, user){
                // console.log(user);
                // get put
                var password = getPostItem(req, res, 'password');
                server.models.user.update({
                        'userId': user.userId
                    }, {
                       'password': password
                    }, function(err, newUsers) {
                    if (err) {
                        var errerMessage = handleOrmError(err);
                        res.send(400, new Error(errerMessage));
                    }
                    else {
                        // console.log(newUsers);
                        var newUser = newUsers.pop();
                        res.send({
                            'api_key': newUser.apiKey,
                            'userid': newUser.username, 
                        });
                    }
                });
            });
        }
    });
    
    // login
    server.post('/api/login', function(req, res){
        var headerAuth = header.headerCheck(req);
        if (headerAuth.err) {
            res.send(400, new Error(headerAuth.err));
        }
        else {
            // get post            
            var username = getPostItem(req, res, 'username');
            var password = getPostItem(req, res, 'password');
            server.models.user.login(username, password, function(err, token){
                if (err) {
                    res.send(400, new Error(err));
                }
                else {
                    // save api-key
                    res.send({'api_key': token});
                }
            });
        }
    });
    
    // logout
    server.del('/api/login', function(req, res){
        var headerAuth = header.headerCheck(req);
        if (headerAuth.err) {
            res.send(400, new Error(headerAuth.err));
        }
        else {
            var userModel = server.models.user;
            validateApiKey(req, res, userModel, function(err, user){
                userModel.update(
                    user, {
                       'apiKey': '' 
                    }, function(err) {
                    if (err) {
                        res.send(400, new Error(err));
                    }
                    else {
                        res.send({'success': 'YES'});
                    }
                });
            });
        }
    });
    
    // update user profile or add device
    server.put('/api/userinfo', function(req, res){
        var headerAuth = header.headerCheck(req);
        if (headerAuth.err) {
            res.send(400, new Error(headerAuth.err));
        }
        else {
            var userModel = server.models.user;
            validateApiKey(req, res, userModel, function(err, user){
                // get profile
                var profile = req.body.profile;
                if (profile !== undefined){
                    // change gender
                    if (profile.gender == 'male') {
                        profile.gender = 1;
                    }
                    else{
                        profile.gender = 0;
                    }
                    // set user profile
                    userModel.update({
                        'userId': user.userId
                    },
                    profile,
                    function(err2, newUser){
                        if (err2) {
                            var errerMessage = handleOrmError(err2);
                            res.send(400, new Error(errerMessage));
                        }
                    });
                }
                // get devices list
                var devicesList = req.body.devices;
                if (devicesList !== undefined) {
                    // get device info
                    var r = mapArray(devicesList, {
                        'DevID': 'deviceId',
                        'DevName': 'deviceName',
                        'SwVer': 'hardware',
                        'HwVer': 'software'
                    });
                    if (r.err) {
                        res.send(400, new Error(r.err));
                    }
                    else {
                        // check exist or create devices
                        var devicesInfo = r.result;
                        async.map(devicesInfo, server.models.device.findOrCreate.bind(server.models.device), function(err3, newDevices) {
                            if (err3) {
                                var errerMessage = handleOrmError(err3);
                                res.send(400, new Error(errerMessage));
                            }
                            // console.log(newDevices);
                            // remove all devices relationship with this user
                            server.models.relation.resetWithUser(user.userId, function(err4){
                                if (err4) {
                                    var errerMessage = handleOrmError(err4);
                                    res.send(400, new Error(errerMessage));
                                }
                                else {
                                    // add new devices
                                    async.map(
                                        newDevices, 
                                        server.models.relation.checkAndCreate.bind(server.models.relation, user.userId),
                                        function(err5, newRelations) {
                                            if (err5) {
                                                var errerMessage = handleOrmError(err5);
                                                res.send(400, new Error(errerMessage));
                                            }
                                            else{
                                                // console.log(newRelations);
                                                res.send({'success': 'YES'});
                                            }
                                        }
                                    );
                                }
                            });
                        });  
                    }
                }
            });
        }
    });
    
    // update user profile or add device
    server.get('/api/userinfo', function(req, res){
        var headerAuth = header.headerCheck(req);
        if (headerAuth.err) {
            res.send(400, new Error(headerAuth.err));
        }
        else {
            var userModel = server.models.user;
            validateApiKey(req, res, userModel, function(err, user){
                // get devices list
                server.models.relation.find({
                    'user': user.userId
                }, function(err, relation){
                    if (relation !== undefined){
                        // get device
                        // console.log(relation);
                        var devicesId = new Array;
                        for (var key in relation) {
                            devicesId.push(relation[key].device);
                        }
                        server.models.device.find(devicesId, function(err, devices){
                            if (devices !== undefined) {
                                // console.log(devices);
                                var devicesList = new Array;
                                for (var i in devices){
                                    var device = devices[i];
                                    var item = {
                                        'DevInfo': {
                                            'DevID': device.deviceId, 
                                            'DevName': device.deviceName,
                                            'SwVer': device.software,
                                            'HwVer': device.hardware,
                                            'DevStatus': 'Online'
                                        }, 
                                        'unhealthy_air_m3': 1200
                                    };
                                    devicesList.push(item);
                                }
                                res.send({
                                    'userinfo': {
                                        'userid': user.username,
                                        'profile': {
                                            'nickname': user.nickname, 
                                            'age': user.age, 
                                            'gender': (user.gender) ? 'male' : 'female',
                                            'address': user.address
                                        }
                                    },
                                    'devices': devicesList
                                });
                            }
                        });
                    }
                });
            });
        }
    });
    
    
    // 测试上传照片
    server.post('/api/picture', function(req, res){
        var headerAuth = header.headerCheck(req);
        if (headerAuth.err) {
            res.send(400, new Error(headerAuth.err));
        }
        else {
            var userModel = server.models.user;
            validateApiKey(req, res, userModel, function(err, user){
                // 接收上传照片
                if (!req.files){
                    res.send(400, new Error('No Picture!'));
                }
                var file = req.files[AVATAR];
                if (file) {
                    // console.log(file);
                    // size
                    if(file.size > 800 * 1024){
                        res.send(400, new Error('Picture Size Great Than 800K!'));
                    }
                    // type
                    else if (file.type != 'image/jpeg' &&
                             file.type != 'image/pjpeg' &&
                             file.type != 'image/x-png' &&
                             file.type != 'image/png') {
                        res.send(400, new Error('Picture Type is not jpeg or png!'));
                    }
                    // save as
                    else{
                        var fileExt=(/[.]/.exec(file.name)) ? /[^.]+$/.exec(file.name.toLowerCase()) : '';
                        var url = 'upload/'+ user.userId + '.' + fileExt;
                        fs.renameSync(file.path, url);
                        // update user info
                        server.models.user.update({
                            'userId': user.userId
                        }, {
                           'avatarUrl': url
                        }, function(err, newUsers) {
                            if (err) {
                                var errerMessage = handleOrmError(err);
                                res.send(400, new Error(errerMessage));
                            }
                            else {
                                res.send('upload ok.');
                            }
                        });
                    }
                }
                else {
                    res.send(400, new Error('Upload File is Null!'));
                }
            });
        }
    });
    
    // 测试下载照片
    server.get('/api/picture', function(req, res){
        var headerAuth = header.headerCheck(req);
        if (headerAuth.err) {
            res.send(400, new Error(headerAuth.err));
        }
        else {
            var userModel = server.models.user;
            validateApiKey(req, res, userModel, function(err, user){
                // console.log(user.avatarUrl);
                try {
                    var img = fs.readFileSync(user.avatarUrl);
                    var type = mime.lookup(user.avatarUrl);
                    res.writeHead(200, {'Content-Type': type});
                    res.write(img, 'binary');
                    res.end();
                }
                catch(e) {
                    res.send(400, new Error('Picture not Found'));
                }
            });
        }
    });
    
    // Device Data
    server.get('/api/devicedata', function(req, res){
        var headerAuth = header.headerCheck(req);
        if (headerAuth.err) {
            res.send(400, new Error(headerAuth.err));
        }
        else {
            var userModel = server.models.user;
            validateApiKey(req, res, userModel, function(err, user){
                // get qurty string
                var deviceId = req.query.deviceid;
                var dataType = req.query.data_type;
                var time = req.query.time;
                // data type
                switch(dataType)
                {
                    // 大包年数据
                    case 'PackageDataYear':{
                        format.packageDataYear(deviceId, time, function(err, data){
                            if (err) {
                                 res.send({'err': err});
                            }
                            else{
                                res.send(data);
                            }
                        });
                    }
                    break;
                    // 大包天数据
                    case 'PackageDataDay':{
                        format.packageDataDay(deviceId, time, function(err, data){ 
                            if (err) {
                                 res.send({'err': err});
                            }
                            else{
                                res.send(data);
                            }
                        });
                    }
                    break;
                    // 当前数据
                    case 'CurrentlyData':{
                        format.currentlyData(deviceId, function(err, data){ 
                            if (err) {
                                 res.send({'err': err});
                            }
                            else{
                                res.send(data);
                            }
                        });
                    }
                    break;
                    default:
                        res.send({'err': dataType + ' is not defined!'});
                }
            });
        }
    });
}

