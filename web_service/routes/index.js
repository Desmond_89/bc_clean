var controllerList = [
    'helloController',
    'account',
    'login',
    'userinfo',
    'deviceData',
    'picture',
    'apnsToken',
    'feedback',
    'resetPassword',
    'setting',
    'cleanData'
];

// register controller in router
function register(server, controller){
    // all urls
    for(var url in controller) {
        // all methods
        for(var method in controller[url]){
            server[method](url, controller[url][method]);
        }
    }
}

module.exports = function(server) {
    for (var key in controllerList) {
        var handle = require('../controller/' + controllerList[key]);
        register(server, handle);
    }
}