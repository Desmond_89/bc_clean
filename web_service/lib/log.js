var bunyan = require('bunyan');

function procName() {
  return process.title + '.' + process.pid;
}

var logger = bunyan.createLogger({
  name: procName(),
  streams: [
    {
      level: 'info',
      path: '/var/log/' + procName() + '-info.log'            // log INFO and above to stdout
    },
    {
      type: 'rotating-file',
      path: '/var/log/' + procName() + '-day.log',
      period: '1d',   // daily rotation
      level: 'debug',
      count: 3        // keep 3 back copies
    },
    {
      level: 'error',
      path: '/var/log/' + procName() + '-error.log'  // log ERROR and above to a file
    }
  ]
});

module.exports = logger;