var moment = require('moment');
var async = require('async');

var logger = require('./log');
var RTDataManager = require('../../backend_service/lib/RTDataManager').RTDataManager;
var deviceInfo = require('./deviceInfo');

// return -1 for null value
function returnNull(value){
    if (value == 0) {
        return -1;
    }
    else {
        return value;
    }
}

// add 0 less 10
function addZero(value) {
    if (value < 10) {
        return '0' + value;
    }
    else {
        return value;
    }
}

// return minutes avg or current data
function currAvg(deviceId, time, callback){    
    var hourData = RTDataManager.hourData;
    hourData.get(deviceId, time, function(err, hourlyData){
        var avg = 0;
        if (hourlyData !== null) {
            // calculating average`
            var minuteData = hourlyData.minute;
            var count = 0, sum = 0;
            for (var minute in minuteData){
                if (minute < 59 && minuteData[minute].aqi !== 0){
                    sum += minuteData[minute].aqi;
                    count++;
                }
            }
            if (count == 0) {
                avg = 0;
            }
            else{
                avg = Math.floor(sum / count);
            }
            
            if (avg == 0) {
                var curr = RTDataManager.currentData;
                curr.get(deviceId, function(err, currentData){
                    
                    // return current data
                    var ret = -1;
                    if (currentData !== null) {
                        if (currentData[0] > (time - 60*1000) &&
                            currentData[0] < (time + 60*1000)) {
                            ret = returnNull(currentData[1][0]);
                        }
                        else{
                            ret = null;
                        }
                    }
                    else{
                        ret = null;
                    }
                    callback(ret);
                });
            }
            else{
                // return minutes avg
                callback(returnNull(avg));   
            }
        }
        else{
            var curr = RTDataManager.currentData;
            curr.get(deviceId, function(err, currentData){
                // return current data
		var ret = -1;
                if (currentData !== null) {
                    if (currentData[0] > (time - 60*1000) &&
                        currentData[0] < (time + 60*1000)) {
                        ret = returnNull(currentData[1][0]);
                    }
		    else{
			ret = null;
		    }
                }
		else{
		    ret = null;
		}
		callback(ret);
            });
        }
    });
}

// get year data
function getYearAvg(deviceId, year, callback){
console.log("getYearAvg at ", new Date()); // 0201
    var yearTs = moment(year, 'YYYY').valueOf();
    var yearData = RTDataManager.yearData;
    var avg = null;
    yearData.get(deviceId, yearTs, function(err, packageYear) {
        if (err) {
            // logger.warn('get year data from redis error', err);
	    callback(err, avg);
        }
	if (packageYear !== null) {
            var monthlyData = packageYear.month;
	    var count = 0, sum = 0;
            for (var month in monthlyData){
		if (monthlyData[month].aqi !== 0){
		    sum += parseInt(monthlyData[month].aqi);
		    count++;
		}
		
		if (count == 0) {
		    avg = null;
		}
		else{
		    avg = returnNull(Math.floor(sum / count));
		}
	    }
	    callback(null, avg);
	}
	else{
	    callback(null, avg);
	}
    });	
}

// return one year data
function getYearlyData(deviceId, year, callback){
logger.info("getYearlyata at ", year); // 0201
    
    var yearTs = moment(year, 'YYYY').valueOf();
    
    var yearData = RTDataManager.yearData;
    yearData.get(deviceId, yearTs, function(err, packageYear) {
        if (err) {
            logger.warn('get year data from redis error', err);
        }
	// logger.info('yearData.get result:', packageYear);
        var ret = {};
	// check last year is null?
	var first = null; // first not null day in 2 years
	getYearAvg(deviceId, parseInt(year)-1, function(err, lastAvg){
	    if (lastAvg !== null && lastAvg !== 0 && lastAvg !== -1) {
		first = [0, 0];
	    }
	    
	    if (packageYear !== null) {
		// year layer
		ret['AQI'] = returnNull(packageYear.aqi);
		ret['Date'] = year;
		ret['monthly'] = [];
		// month layer
		var monthlyData = packageYear.month;
		// logger.info(monthlyData);
		var count = 0, sum = 0;
		for (var month in monthlyData){
		    // check months count
		    var monthMax = moment().month();
		    if (year < moment().year()) {
			    monthMax = 11;	// query last year data
		    }
		    if (month > 11 ||  month > monthMax){
			continue;
		    }
logger.info('get data: year:' + year + 'month: ' + month + ", monthMax:", monthMax);
		    var monthObj = {};
		    monthObj['AQI'] = returnNull(monthlyData[month].aqi);
		    monthObj['Date'] = moment(year, 'YYYY').month(parseInt(month)).format('MMM YYYY');
		    monthObj['daily'] = [];
		    
		    // calc year avg
		    if (monthlyData[month].aqi !== 0){
			sum += parseInt(monthlyData[month].aqi);
			count++;
		    }
		    
		    // daily layer
		    var dailyData = monthlyData[month].day;
		    for (var day in dailyData){
			// check days count
			var dayString = '';
			var pd = moment((parseInt(day) + 1) + ' ' + monthObj['Date'], 'DD MMM YYYY');
			if (
			    pd.isValid() && 
			    pd.format('MMM') == moment(monthObj['Date'], 'MMM YYYY').format('MMM') && 
			    pd.isBefore(moment())	// <=
			){
			    dayString = pd.format('Do MMM YYYY');
			}
			else {
			    continue;
			}
			var dayObj = {};
			dayObj['AQI'] = returnNull(dailyData[day].aqi);
			// get first day
			if (first == null && dayObj['AQI'] !== -1){
			    first = [month, day];
			}
			dayObj['Date'] = dayString;
			// nodisturb
			dayObj['isSetNoDisturb'] = [];
			for (var i in dailyData[day].disturb) {
			    var disturb = dailyData[day].disturb[i];
			    // check null
			    if ( disturb.toString() !== [0, 0, 0, 0].toString()) {
				dayObj['isSetNoDisturb'].push({
				    form: addZero(disturb[0]) + '' + addZero(disturb[1]), 
				    to: addZero(disturb[2]) + '' + addZero(disturb[3])
				});
			    }
			}
			dayObj['rank'] = dailyData[day].rank;
			// check null
			if (first == null){
			    monthObj['daily'].push(null);
			}
			else{
			    monthObj['daily'].push(dayObj);
			}
		    }
		    // reverse day
		    monthObj['daily'].reverse();
		    // check null
		    if (first == null){
			ret['monthly'].push(null);
		    }
		    else{
			ret['monthly'].push(monthObj);
		    }
		}
		
		if (count == 0) {
		    ret['AQI'] = 0;
		}
		else{
		    ret['AQI'] = returnNull(Math.floor(sum / count));
		}
		
		// reverse month
		ret['monthly'].reverse();
		
		// check last data == -1
		currAvg(deviceId, moment().valueOf(), function(avgData){
		    // check month data
		    if (ret.monthly[0] !== null) {
			if (ret.monthly[0]['AQI'] == -1 && avgData !== null) {
			    ret.monthly[0]['AQI'] = avgData;
			}
			// check day 
			if (ret.monthly[0]['daily'][0] !== null) {
			     if (ret.monthly[0]['daily'][0]['AQI'] == -1 && avgData !== null) {
				ret.monthly[0]['daily'][0]['AQI'] = avgData;
			    }
			}
		    }
		    else{
			if (avgData !== null && moment(yearTs).year() == moment().year()) {
			    ret.monthly[0] = {
				"AQI": avgData,
				"Date": moment(moment().valueOf()).format('MMM YYYY'),
				"daily": [{
				    "AQI": avgData,
				    "Date": moment(moment().valueOf()).format('Do MMM YYYY'),
				    "isSetNoDisturb": [],
				    "rank": []
				}]
			    };
			}
		    }
		    
		    callback(null, ret);
		});
	    }
	    else {
		currAvg(deviceId, moment().valueOf(), function(avgData){
		    if (avgData == null || moment(yearTs).year() !== moment().year()) {
			// no data
			callback(null, null);
		    }
		    else{
			callback(null, {
			    "AQI": avgData,
			    "Date": moment(moment().valueOf()).format('YYYY'),
			    "monthly": [{
				"AQI": avgData,
				"Date": moment(moment().valueOf()).format('MMM YYYY'),
				"daily": [{
				    "AQI": avgData,
				    "Date": moment(moment().valueOf()).format('Do MMM YYYY'),
				    "isSetNoDisturb": [],
				    "rank": []
				}]
			    }]
			});
		    }
		});
	    }
	});
    });
}

// return one day data
function getDailyData(deviceId, day, callback) {
console.log("getDailyData at ", new Date()); // 0201
    var dayTs = moment(day, 'YYYY-MM-DD').valueOf();
    
    var dayData = RTDataManager.dayData;
    dayData.get(deviceId, dayTs, function(err, dailyData) {
	
	if (err) {
            logger.warn('get day data from redis error', err);
        }
	
        var ret = {};
        if (dailyData !== null) {
            // day layer
            ret['AQI'] = returnNull(dailyData.aqi);
            ret['Date'] = moment(day, 'YYYY-MM-DD').format('Do MMM YYYY');
            ret['hourly'] = [];
            // hour layer
            var hourlyData = dailyData.hour;
            for (var hour in hourlyData){
                // check hours count
                if (hour > 23 || moment(day + ' ' + hour, 'YYYY-MM-DD H').isAfter(moment()))
                    continue;
                var hourObj = {};
                hourObj['AQI'] = returnNull(hourlyData[hour].aqi);
                hourObj['Date'] = moment(day + ' ' + hour, 'YYYY-MM-DD H').format('HH:mm, Do MMM YYYY');
                ret['hourly'].push(hourObj);
            }
            // reverse hour
            ret['hourly'].reverse();
	    
	    // check last data == -1
	    currAvg(deviceId, moment().valueOf(), function(avgData){
		
		// check current data
		if (ret.hourly[0] !== null) {
		    if (ret.hourly[0]['Date'] == moment().format('HH:00, Do MMM YYYY') && 
			ret.hourly[0]['AQI'] == -1 && avgData !== null) {
			ret.hourly[0]['AQI'] = avgData;
		    }
		}
                else{
                    if (avgData !== null && moment(dayTs).dayOfYear() == moment().dayOfYear()) {
                        ret.hourly[0] = {
			    "AQI": avgData,
			    "Date": moment(moment().valueOf()).format('HH:00, Do MMM YYYY')
			};
                    }
                }
		
		// check last hour
		currAvg(deviceId, moment().valueOf() - 60*60*1000, function(avgData2){
		    if (ret.hourly[1]['Date'] == moment(moment().valueOf() - 60*60*1000).format('HH:00, Do MMM YYYY') &&
			ret.hourly[1]['AQI'] == -1 && avgData2 !== null) {
			ret.hourly[1]['AQI'] = avgData2;
		    }
		    callback(null, ret);
		});
	    });
        }
        else {
	    currAvg(deviceId, moment().valueOf(), function(avgData){
		if (avgData == null || moment(dayTs).dayOfYear() !== moment().dayOfYear()) {
		    // no data
                    callback(null, null);
		}
		else{
		    callback(null, {
			"AQI": avgData,
			"Date": moment(moment().valueOf()).format('Do MMM YYYY'),
			"hourly": [{
			    "AQI": avgData,
			    "Date": moment(moment().valueOf()).format('HH:00, Do MMM YYYY'),
			}]
		    });
		}
	    });
        }
    });
}

// return one hour data
function getHourlyData(deviceId, hour, callback) {
    var hourTs = moment(hour, 'YYYY-MM-DD HH:mm').valueOf();
    // get hourly data
    var hourData = RTDataManager.hourData;
    hourData.get(deviceId, hourTs, function(err, hourlyData) {
        if (err) {
            logger.error(err);
        }
        // return data
        var ret = {};
        if (hourlyData !== null) {
            // hour layer
            ret['AQI'] = returnNull(hourlyData.aqi);
            ret['Date'] = moment(hour, 'YYYY-MM-DD HH:mm').format('HH:mm, Do MMM YYYY');
            ret['minutely'] = [];
            // hour layer
            var minutelyData = hourlyData.minute;
            for (var minute in minutelyData){
                // check minutes count
                if (minute > 59 || moment(hourTs + minute * 60 * 1000).isAfter(moment()))
                    continue;
                var minuteObj = {};
                minuteObj['AQI'] = returnNull(minutelyData[minute].aqi);
                minuteObj['Date'] = moment(hourTs + minute * 60 * 1000).format('HH:mm, Do MMM YYYY');
                ret['minutely'].push(minuteObj);
            }
            // reverse hour
            ret['minutely'].reverse();
            callback(null, ret);
        }
        else {
            callback(null, null);
        }
    });
}

// return all months and days value of a device in 1 year
function packageDataYear(deviceId, time, callback) {
    var ret = {
        success: 'YES',
        DevInfo: null,
        Payloads: null
    };
     // get device info
    deviceInfo.get(deviceId, function(err, device){
        if (err) {
            callback(err);
        }
        else {
            // return device info
            ret.DevInfo = device;
            // payloads 
            getYearlyData(deviceId, time, function(err, yearData){
                ret.Payloads = {
                    type: "PackageDataYear",
		    time: time,
                    // monthly: yearData.monthly
                };
                if (yearData !== null) {
                    ret.Payloads['monthly'] = yearData.monthly;
                }
                else {
                    ret.Payloads['monthly'] = null;
                }
                callback(null, ret);
            });
        }
    });
}

// return all months and days value of a device in 2 years
function PackageData2Years(deviceId, time, callback) {
logger.info('PackageData2Years at '); //0201
    var ret = {
        success: 'YES',
        DevInfo: null,
        Payloads: null
    };
     // get device info
    deviceInfo.get(deviceId, function(err, device){
        if (err) {
logger.info('deviceInfo.get error:', err); // 0201
            callback(err);
        }
        else {
            // return device info
            ret.DevInfo = device;
            // payloads
            async.map([time, (time-1).toString()], function(year, callback){
logger.info('try to getYearlyData', year);
                 getYearlyData(deviceId, year, callback);
            }, function(err, results){
                ret.Payloads = {
                    type: "PackageData2Years",
		    time: time,
                    yearly: results
                };
                callback(null, ret); 
            });
        }
    });
}

// return all hours and mintues value of a device in one day
function packageDataDay(deviceId, time, callback) {
    var ret = {
        success: 'YES',
        DevInfo: null,
        Payloads: null
    };
     // get device info
    deviceInfo.get(deviceId, function(err, device){
        if (err) {
            callback(err);
        }
        else {
            // return device info
            ret.DevInfo = device;
            ret.Payloads = {
                type: "packageDataDay",
                time: time
            };
            // payloads
            var hourArray = [];
            for(var i=23; i>=0; i--){
                if (moment(time + ' ' + i + ':00', 'YYYY-MM-DD HH:mm').isBefore(moment())) {
                    hourArray.push(time + ' ' + i + ':00');
                }
            }
            // get all minutes in 24 hours
            async.map(hourArray, function(hour, callback){
                 getHourlyData(deviceId, hour, callback);
            }, function(err, results){
                if (results !== null) {
                    // get hours data
                    getDailyData(deviceId, time, function(err, dayData){
                        if (dayData) {
                            for (var key in dayData.hourly) {
                                if (results[key] !== null) {
                                    if (results[key]['Date'] == dayData.hourly[key]['Date']) {
                                        results[key]['AQI'] = dayData.hourly[key]['AQI'];
                                    }
                                }
                            }
                        }
                        ret.Payloads['hourly'] = results;
                        callback(null, ret);
                    });
                }
                else {
                    ret.Payloads['hourly'] = null;
                    callback(null, ret);
                }
            });
        }
    });
}

// return all hour data in a day
function HourlyDataDay(deviceId, time, callback) {
    var ret = {
        success: 'YES',
        DevInfo: null,
        Payloads: null
    };
     // get device info
    deviceInfo.get(deviceId, function(err, device){
        if (err) {
            callback(err);
        }
        else {
            // return device info
            ret.DevInfo = device;
            // payloads
            getDailyData(deviceId, time, function(err, dailyData){
                ret.Payloads = {
                    type: "HourlyDataInOneDay",
		    time: time
                };
                if (dailyData !== null) {
                    ret.Payloads['hourly'] = dailyData.hourly;
                }
                else {
                    ret.Payloads['hourly'] = null;
                }
                callback(null, ret);
            });
        }
    });
}

// return all mintues data in hour
function MinutelyDataDay(deviceId, time, callback) {
    var ret = {
        success: 'YES',
        DevInfo: null,
        Payloads: null
    };
     // get device info
    deviceInfo.get(deviceId, function(err, device){
        if (err) {
            callback(err);
        }
        else {
            // return device info
            ret.DevInfo = device;
            // payloads
            getHourlyData(deviceId, time, function(err, hourlyData){
                ret.Payloads = {
                    type: "MinutelyDataInOneHour",
		    time: time
                };
                if (hourlyData !== null) {
                    ret.Payloads['minutely'] = hourlyData.minutely;
                }
                else {
                    ret.Payloads['minutely'] = null;
                }
                callback(null, ret);
            });
        }
    });
}

// return real time value of a device
function currentlyData(deviceId, callback) {
    var ret = {
        success: 'YES',
        DevInfo: null, 
        Payloads: {
            type: 'CurrentlyData',
            AQI: null, 
            Temp: null, 
            Humi: null, 
            timestamp: null, 
            outdoorsAQI: null,
            rank: null
        }
    };
    // get device info
    deviceInfo.get(deviceId, function(err, device){
        if (err) {
            callback(err);
        }
        else {
            // return device info
            ret.DevInfo = device;
            // get current data
            var curr = RTDataManager.currentData;
            curr.get(deviceId, function(err, currentData){
                // return current data
                if (currentData !== null) {
                    ret.Payloads.AQI = currentData[1][0];
                    ret.Payloads.Temp = currentData[1][1];
                    ret.Payloads.Humi = currentData[1][2];
                    ret.Payloads.timestamp = Math.round(currentData[0]/1000);
                }
                // get rank
                var rank = RTDataManager.rank;
                rank.get(deviceId, function(err, rankArray){
                    if (rankArray !== null) {
                        ret.Payloads.rank = rankArray;
                    }
                    // get reference AQI
                    // if (device['city']) {
                        var refAqi = RTDataManager.refAqi;
                        refAqi.get(device['city'], function(err, aqi){
                            // console.log(aqi);
                            if (aqi !== null) {
                                ret.Payloads.outdoorsAQI = aqi;
                            }
                            // return
                            callback(null, ret);
                        });
                    // }
                });
            });
        }
    });
}

module.exports = {
    PackageData2Years: PackageData2Years, 
    packageDataYear: packageDataYear,
    HourlyDataDay: HourlyDataDay,
    MinutelyDataDay: MinutelyDataDay, 
    packageDataDay: packageDataDay,
    currentlyData : currentlyData
};
