var crypto = require('crypto');
var logger = require('../lib/log');
var RTDataManager = require('../../backend_service/lib/RTDataManager').RTDataManager;

// client_id : client_secret
var clientDict = {
    'b153e898-3531-4bee-8f20-e5329e37a353': 'cd8b5315-369d-4fca-ae72-169a9d563469',
    '25977866-09b7-076b-6409-2a537785c08b': '1dac01ed-5669-87f3-0eff-c7e993bbd3b7'
};

// check Authorization in header
function checkAuth(req) {
    // get header factor and make plain string
    var content = req.method + '\n';
    var headerList = ['Content-Type', 'Content-MD5', 'UTC-Timestamp', 'Nonce'];
    for (var i = 0; i < headerList.length; i++) {
        var item = req.header(headerList[i]);
        if (item !== undefined) {
            content += item + '\n';
        }
        else{
            // miss factor
            return {err: 'Header ' + headerList[i] + ' None!'};
        }
    };
    content += req.route.path + '\n';   // plain string
    
    logger.info(req.method, req.route.path);
    
    // get Authorization and separate client_id
    var got = '';
    if (req.header('Authorization')) {
        // get Client Id
        got = req.header('Authorization');
        var index1 = got.indexOf(' ');
        var index2 = got.indexOf(':');
        var clientId = got.slice(index1 + 1, index2);
    }
    else {
        // miss Authorization
        return {err: 'Header Authorization None!'};
    }
    
    // check client_id, Authorization
    var hmacBase64, authorization;
    var result = {};
    if (clientDict[clientId]) {
        hmacBase64 = crypto.createHmac('sha256', clientDict[clientId]).update(content).digest('base64');
        authorization = 'Clean ' + clientId + ':' + hmacBase64;
        if (authorization !== got) {
            result = {err: 'Authorization Error! Expected:' + authorization + ''};
        }
        else{
            result = {err: null};
        }
    }
    else{
        result = {err: 'Client Id Error!'};
    }
    
    return result;
}

// check and handle error header request
function checkHeader(req, res, callback) {
    var result = checkAuth(req);
    if (result.err) {
        res.send(400, new Error(result.err));
    }
    else {
        callback();
    }
}

// get item in post body, send error when null
function getPostItem(req, res, item) {
    var errorMessage;
    if (req.body === undefined) {
        errorMessage = 'No Post Body!';
        res.send(400, new Error(errorMessage));
    }
    var result = req.body[item];
    if (result === undefined) {
        errorMessage = 'No \'' + item + '\' in Post Body!';
        res.send(400, new Error(errorMessage));
    }
    return result;
}

// get item in query string, send error when null
function getQueryItem(req, res, item) {
    var errorMessage;
    if (req.query === undefined) {
        errorMessage = 'No Query String!';
        res.send(400, new Error(errorMessage));
    }
    var result = req.query[item];
    if (result === undefined) {
        errorMessage = 'No \'' + item + '\' in Query String!';
        res.send(400, new Error(errorMessage));
    }
    return result;
}

// get api-key in header and return login user
function validateApiKey(req, res, user, callback) {
    var token = req.header('API-KEY');
    if (token === undefined) {
        var errorMessage = 'No API-KEY!';
        res.send(400, new Error(errorMessage));
    }
    else {
        // from redis
        var apiKey = RTDataManager.apiKey;
        apiKey.getUser(token, function(err, username){
            if (username !== null) {
                console.log('from redis');
                callback(null, username);
            }
            else {
                // from mysql
                user.validateToken(token, function(err2, username){
                    if (err2) {
                        res.send(400, new Error(err2));
                    }
                    else{
                        console.log('from mysql');
                        callback(null, username);
                    }
                });
            }
        });
    }
}

// get items in post body
function getItemsInPost(req, items, requested) {
    var result = {
        err: null,
        res: {}
    };
    // check post body
    if (req.body === undefined) {
        result.err = 'No Post Body!';
    }
    else {
        for (var key in items){
            // check requested item
            var item = req.body[items[key]];
            if (item === undefined) {
                if (requested) {
                    // throw error
                    result.err = 'No \'' + items[key] + '\' in Post Body!';
                    break;
                }
                else {
                    // ignore
                    continue;
                }
            }
            else {
                result.res[items[key]] = item;
            }
        }
    }
    return result;
}

// check username - device relationship in redis
function checkRelationship(username, deviceid, callback){
    // read device list in redis
    var userData = RTDataManager.userData;
    userData.getDevices(username, function(err, devices){
        // check
        if (devices == null) {
            callback(err, false);
        }
        else{
            if (devices[deviceid]) {
                callback(err, true);
            }
            else{
                callback(err, false);
            }
        }
    });
}

function checkDeviceOwner(req, res, username, deviceid, callback) {
    checkRelationship(username, deviceid, function(err, isOwner){
        if (isOwner) {
            callback(err, isOwner);
        }
        else{
            res.send(400, new Error(deviceid + ' do not belong to ' + username + '! '));
        }
    });
}

module.exports = {
    checkHeader: checkHeader,
    getPostItem: getPostItem,
    getQueryItem: getQueryItem, 
    validateApiKey: validateApiKey,
    getItemsInPost: getItemsInPost,
    checkDeviceOwner: checkDeviceOwner
};