// get and make error message from waterline
function handleOrmError(errors) {
    var result = new Array;
    for (var item in errors.invalidAttributes){
        var err_item = errors.invalidAttributes[item];
        // console.log(err_item);
        for(var key in err_item){
            var err_key = err_item[key];
            // console.log(err_key);
            var rule = err_key.rule;
            var value = err_key.value;
            switch(rule){
                case 'unique': {
                    result.push(value + ' already exists');
                } 
                break;
                case 'email': {
                    result.push('\'' + item + '\' should be email');
                } 
                break;
                case 'maxLength': {
                    result.push('\'' + item + '\' is too long');
                }
                break;
                case 'integer': {
                    result.push('Input should be integer');
                }
                break;
                default:
                    result.push('\'' + item + '\' break the rule ' + rule);
            }
        }
    }
    return result;
}

module.exports = {
    get: handleOrmError
};