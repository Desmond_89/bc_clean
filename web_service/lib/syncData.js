/* sync data from mysql to redis */

var orm = require('./orm');
var RTDataManager = require('../../backend_service/lib/RTDataManager').RTDataManager;
var async = require('async');

var PARALLEL_COUNT = 100;

// sync user data
function syncUser(db) {
    // read data from mysql
    db.user.find({
        available: 1    // all available accounts
    }, function(err, users){
        // console.log(users);
        if (users.length !== 0) {   // not null
            async.eachLimit(users, PARALLEL_COUNT, function(user, callback){
                // update redis
                var userData = RTDataManager.userData;
                userData.setData(user.username, user, function(err){
                    if (err) {
                        console.log(err);
                    }
                    else{
                        console.log('Set User Info: ' + user.username);
                    }
                    callback();
                });
            });
        }
    });
}

// sync device data
function syncDevice(db) {
    // read data from mysql
    db.device.find({
        // find all
    }, function(err, devices){
        // console.log(devices);
        if (devices.length !== 0) {   // not null
            async.eachLimit(devices, PARALLEL_COUNT, function(device, callback){
                // update redis
                var deviceData = RTDataManager.deviceData;
                // change format
                device['SwVer'] = device['software'];
                device['HwVer'] = device['hardware'];
                device['DevName'] = device['deviceName'];
                device['DevID'] = device['deviceId'];
                deviceData.setData(device.deviceId, device, function(err){
                    if (err) {
                        console.log(err);
                    }
                    else{
                        console.log('Set Device Info: ' + device.deviceId);
                    }
                    callback();
                });
            });
        }
    });
}

// sync relationship
function syncRelation(db) {
    // for user
    db.user.find({
        available: 1    // all available accounts
    }, function(err, users){
        if (users.length !== 0) {   // not null
            async.eachLimit(users, PARALLEL_COUNT, function(user, callback){
                db.relation.find({
                    user: user.userId
                }, function(err, relations){
                    var userData = RTDataManager.userData;
                    var devicesList = {};
                    for (var key in relations) {
                        devicesList[relations[key].device] = {
                            isDefault: relations[key].isDefault
                        };
                    }
                    userData.setDevices(user.username, devicesList, function(err){
                        console.log(user.username + ' set devices ' + devicesList);
                    });
                    callback();
                });
            });
        }
    });
    // for device
    db.device.find({
        // find all
    }, function(err, devices){
        if (devices.length !== 0) {   // not null
            async.eachLimit(devices, PARALLEL_COUNT, function(device, callback){
                db.relation.find({
                    device: device.deviceId
                }, function(err, relations){
                    // get username
                    async.map(relations, function(r, usercb){
                        db.user.findOne({
                            userId: r.user
                        }, function(err, user){
                            usercb(err, user.username);
                        });
                    }, function(err, results){
                        var deviceData = RTDataManager.deviceData;
                        var usersList = {};
                        for (var key in results) {
                            usersList[results[key]] = {};
                        }
                        deviceData.setUsers(device.deviceId, usersList, function(err){
                            console.log(device.deviceId + ' set users ' + usersList);
                        });
                        callback();
                    });
                });
            });
        }
    });
}

// waterline orm
orm(function(err, models){
    if(err) {
        throw err;
    }
    // db handle
    db = models.collections;
    
    // sync user data
    syncUser(db);
    
    // sync devices data
    syncDevice(db);
    
    // sync relationship data
    syncRelation(db);
});
