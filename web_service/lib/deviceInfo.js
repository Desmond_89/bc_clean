/* get device property, status and city */
var logger = require('../lib/log');
var moment = require('moment');
var RTDataManager = require('../../backend_service/lib/RTDataManager').RTDataManager;

function getDeviceInfo(deviceId, callback) {
    var ret = {
        DevID: deviceId,
        DevStatus: null,
    };
    // check device id
    if(typeof(deviceId) == "undefined")  {
        callback('no device id!');
    }
    // get device info from redis
    deviceData = RTDataManager.deviceData;
    deviceData.getData(deviceId, function(err, deviceInfo){
        if (err) {
            logger.error(err);
        }
        // check device info integrity
        if (deviceInfo === null) {
            // get device info from mysql
            process.app.models.device.findOne({deviceId: deviceId}, function(err, sqlDevice){
                if (err) {
                    logger.error(err);
                }
                // console.log(sqlDevice);
                // return device info
                if (sqlDevice !== undefined) {
                    ret['HwVer'] = sqlDevice.hardware;
                    ret['SwVer'] = sqlDevice.software;
                    ret['DevName'] = sqlDevice.deviceName;
                    ret['city'] = sqlDevice.city;
                }
                // get device status
                deviceData.getStatus(deviceId, function(err, deviceStatus){
                    if (err) {
                        logger.error(err);
                    }
                    if (deviceStatus !== null) {
                        ret.DevStatus = deviceStatus;
                    }
                    // return
                    callback(null, ret);
                });
            });
        }
        else {
            // return device info
            ret['HwVer'] = deviceInfo['HwVer'];
            ret['SwVer'] = deviceInfo['SwVer'];
            ret['DevName'] = deviceInfo['DevName'];
            ret['city'] = (deviceInfo['city']) ? deviceInfo['city'] : (deviceInfo['ipCity']);
            // get device status
            deviceData.getStatus(deviceId, function(err, deviceStatus){
                if (err) {
                    logger.error(err);
                }
                if (deviceStatus !== null) {
                    ret.DevStatus = deviceStatus;
                }
                // return
                callback(null, ret);
            });
        }
    });
}

module.exports = {
    get: getDeviceInfo
};