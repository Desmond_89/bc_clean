/*
 * Use Waterline v0.10 as ORM Component
 */

var Waterline = require('waterline');
// Instantiate a new instance of the ORM
var orm = new Waterline();

/* CONFIG */
var connectConfig = require('../config/orm.js').connect;

var mysqlAdapter = require('sails-mysql'),
    redisAdapter = require('sails-redis');
 
var ormConfig = {
    // Setup Adapters
    adapters: {
      'default': mysqlAdapter,
      // 'redis': redisAdapter,
      'mysql': mysqlAdapter
    },
    // Build Connections Config
    'connections': connectConfig,
    'defaults': {
        'migrate': 'safe'
    }
};

/* MODELS */
var User = require('../models/user.js');
var Relation = require('../models/relationship.js');
var Device = require('../models/device.js');
var Mobile = require('../models/mobile.js');
var Feedback = require('../models/feedback.js');
var ResetCode = require('../models/resetcode.js');
var Event = require('../models/event.js');
var Callup = require('../models/callup.js');
orm.loadCollection(User);
orm.loadCollection(Relation);
orm.loadCollection(Device);
orm.loadCollection(Mobile);
orm.loadCollection(Feedback);
orm.loadCollection(ResetCode);
orm.loadCollection(Event);
orm.loadCollection(Callup);

/* USE WATERLINE */
module.exports = function(callback){
    orm.initialize(ormConfig, callback);
}