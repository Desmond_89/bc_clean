process.title = 'webServer';
// 加载模块
var restify = require('restify');

// 生成 restify 实例
var server = restify.createServer();

// 加载中间件
server.use(restify.gzipResponse());
server.use(restify.queryParser());
server.use(restify.bodyParser({
    maxBodySize: 800*1024,
    // uploadDir: '/upload',
    uploadDir: '/home/desmond/bc_clean/web_service/upload', 
    keepExtensions: true,
}));

// 路由控制
var routes = require('./routes/index');
routes(server);

// waterline as orm
var orm = require('./lib/orm');
orm(function(err, models){
    if(err) throw err;
    
    process.app = {};
    process.app.models = models.collections;
    process.app.connections = models.connections;
    
    // 启动工程并侦听端口
    server.listen(1337, function() {
        console.log('%s listening at %s', server.name, server.url);
    });
});