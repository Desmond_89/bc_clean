var handle = require('../lib/handleRequest');
var logger = require('../lib/log');
var ormError = require('../lib/handleOrmError');
var uuid = require('node-uuid');
// realtime data manager
var RTDataManager = require('../../backend_service/lib/RTDataManager').RTDataManager;
var MSG = require('../../backend_service/lib/RTMessage');

// register
function register(req, res) {
    handle.checkHeader(req, res, function(){
        // get user input
        var input = handle.getItemsInPost(req, ['userid', 'password'], true);
        if (input.err) {
            res.send(400, new Error(input.err));
        }
        else {
            // input.userid => user.username
            input.res['username'] = input.res['userid'];
            delete input.res['userid'];
            var accountInfo = input.res;
            // create it in mysql
             process.app.models.user.signin(accountInfo, function(err, user){
                if (err) {
                    res.send(400, new Error(err));
                }
                else{
                    // sync it in redis
                    var userData = RTDataManager.userData;
                    userData.setData(accountInfo.username, {
                        username: accountInfo.username, 
                        password: accountInfo.password,
                        userType: user.userType
                    }, function(err2, ret2){
                        // sync api key
                        userData.setApiKey(accountInfo.username, user.apiKey, function(err3, ret3){
                            // check test
                            /*
                            userData.getData(accountInfo.username, function(err4, redisUser){
                                console.log(redisUser);
                            });
                            */
                        });
                    });
                    
                    // send email
                    var mail = MSG.mailQueue;
                    var signinEmail = {
                        to: accountInfo.username, 
                        subject: 'Registered Successfully', 
                        text: 'Registered Successfully'
                    }
                    mail.append(signinEmail, function(err, mailQueue){});
                    
                    // response
                    res.send({
                        success: 'YES', 
                        userid: user.username,
                        api_key: user.apiKey
                    });
                }
            });
        }
    });
}

// change password
function changePassword(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            // get user input
            var input = handle.getItemsInPost(req, ['old_pwd', 'password'], true);
            if (input.err) {
                res.send(400, new Error(input.err));
            }
            else {
                // use mysql
                process.app.models.user.update({
                    username: username, 
                    password: input.res['old_pwd']
                }, {
                    password: input.res['password'],
                    apiKey: uuid.v4()
                }, 
                function(err, newUsers) {
                    if (err) {
                        var errerMessage = ormError.get(err);
                        res.send(400, new Error(errerMessage));
                    }
                    else {
                        if (newUsers.length == 0) {
                            res.send(400, new Error('Incorrect Password!'));
                        }
                        else {
                            var newUser = newUsers.pop();
                            // sync in redis
                            var userData = RTDataManager.userData;
                            userData.setData(newUser.username, {
                                password: newUser.password,
                            }, function(err2, ret2){
                                userData.setApiKey(newUser.username, newUser.apiKey, function(err3, ret3){});
                            });
                            // response
                            res.send({
                                success: 'YES', 
                                api_key: newUser.apiKey,
                                userid: newUser.username, 
                            });   
                        }
                    }
                });   
            }
        });
    });
}

// unavaliable account
function unavaliable(req, res){
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            // get user id in mysql
            process.app.models.user.findOne({
                username: username
            }, function(err, user){
                if (user == undefined) {
                    res.send(400, new Error(username + 'is not exist!'));
                }
                else {
                    // remove relation
                    process.app.models.relation.resetWithUser(user.userId, function(err2){
                        if (err2) {
                            var errerMessage = ormError.get(err2);
                            res.send(400, new Error(errerMessage));
                        }
                        else {
                            // set available and logout
                            process.app.models.user.update({
                                userId: user.userId
                            },{
                                apiKey: null,
                                available: 0    // false
                            }, function(err) {
                                if (err) {
                                    var errorMessage = ormError.get(err);
                                    res.send(400, new Error(errorMessage));
                                }
                                else {
                                    // sync in redis
                                    var apiKey = RTDataManager.apiKey;
                                    apiKey.clear(username);
                                    res.send({
                                        success: 'YES', 
                                        userId: user.username
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });
    });
}

module.exports = {
    '/api/account': {
        post: register,
        put: changePassword,
        del: unavaliable
    }
};