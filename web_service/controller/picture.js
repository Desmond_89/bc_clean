var handle = require('../lib/handleRequest');
var logger = require('../lib/log');
var fs = require('fs');
var mime = require('mime');

var AVATAR = 'picture';

function uploadPicture(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            // �����ϴ���Ƭ
            if (!req.files){
                res.send(400, new Error('No Picture!'));
            }
            var file = req.files[AVATAR];
            if (file) {
                // logger.info('upload picture: ', file);
                // size
                if(file.size > 800 * 1024){
                    res.send(400, new Error('Picture Size Great Than 800K!'));
                }
                // type
                else if (file.type != 'image/jpeg' &&
                         file.type != 'image/pjpeg' &&
                         file.type != 'image/x-png' &&
                         file.type != 'image/png') {
                    res.send(400, new Error('Picture Type is not jpeg or png!'));
                }
                // save as
                else{
                    var fileExt = (/[.]/.exec(file.name)) ? /[^.]+$/.exec(file.name.toLowerCase()) : '';
                    var url = '/home/desmond/bc_clean/web_service/upload/' + username + '.' + fileExt;
                    logger.info('upload file: ', file);
                    logger.info('upload picture: ', file.path, url);
                    // setTimeout(function(){
                        // save as
                        fs.renameSync(file.path, url);
                        /*
                        fs.renameSync(
                            '/home/desmond/bc_clean/web_service/upload/upload_9df83567e56ef984028ed00167dbd23d.jpg',
                            '/home/desmond/bc_clean/web_service/upload/test.jpg'
                        );
                        */
                        
                        // update user info
                        process.app.models.user.update({
                            'username': username
                        }, {
                           'avatarUrl': url
                        }, function(err, newUsers) {
                            if (err) {
                                var errerMessage = handleOrmError(err);
                                res.send(400, new Error(errerMessage));
                            }
                            else {
                                res.send('upload ok.');
                            }
                        });
                    // }, 1000);
                }
            }
            else {
                res.send(400, new Error('Upload File is Null!'));
            }
        });
    });
}

function downloadPicture(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            process.app.models.user.findOne({
                username: username
            }, function(err, user){
                if (user == undefined) {
                    res.send(400, new Error(username + 'is not exist!'));
                }
                else {
                    try {
                        var img = fs.readFileSync(user.avatarUrl);
                        var type = mime.lookup(user.avatarUrl);
                        res.writeHead(200, {'Content-Type': type});
                        res.write(img, 'binary');
                        res.end();
                    }
                    catch(e) {
                        res.send(400, new Error('Picture not Found'));
                    }
                }
            });
        });
    });
}

module.exports = {
    '/api/picture': {
        post: uploadPicture,
        get: downloadPicture
    }
};