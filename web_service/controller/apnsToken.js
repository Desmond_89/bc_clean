var handle = require('../lib/handleRequest');
var logger = require('../lib/log');
var moment = require('moment');
var ormError = require('../lib/handleOrmError');
var RTDataManager = require('../../backend_service/lib/RTDataManager').RTDataManager;

function setAPNsToken(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            // get fields
            var input = handle.getItemsInPost(req, ['uuid', 'app_ver', 'token', 'language'], true);
            if (input.err) {
                res.send(400, new Error(input.err));
            }
            else {
                // find or create mobile
                process.app.models.mobile.updateOrCreate({
                    mobileUUID: input.res['uuid'],
                    appVersion: input.res['app_ver'],
                    apnsToken: input.res['token'],
                    appLanguage: input.res['language'],
                    lastUseTime: moment(new Date()).format('YYYYMMDDHHmmmm')
                }, function(err, mobile){
                    if (err) {
                        res.send(400, new Error(err));
                    }
                    else{
                        // update user info 
                        process.app.models.user.update({
                            username: username
                        },{
                            mobile: mobile.mobileId
                        }, function(err2) {
                            if (err2) {
                                var errorMessage = ormError.get(err2);
                                res.send(400, new Error(errorMessage));
                            }
                            else {
                                // sync in redis
                                var userData = RTDataManager.userData;
                                userData.setData(username, {
                                    'mobile': mobile.mobileId, 
                                    'apns_token': input.res['token']
                                }, function(err3, res2){
                                });
                                // response
                                res.send({
                                    success: 'YES',
                                    /*
                                    uuid: input.res['uuid'],
                                    app_ver: input.res['app_ver'],
                                    token: input.res['token'],
                                    language: input.res['language']
                                    */
                                });
                            }
                        });
                    }
                });   
            }
        });
    });
}

module.exports = {
    '/api/apnsinfo': {
        post: setAPNsToken,
    }
};