var handle = require('../lib/handleRequest');
var logger = require('../lib/log');
var ormError = require('../lib/handleOrmError');
var appConfig = require('../config/app.js').config;
var RTDataManager = require('../../backend_service/lib/RTDataManager').RTDataManager;
var MSG = require('../../backend_service/lib/RTMessage');
var async = require('async');
var moment = require('moment');

// map user profile
function mapUserProfile(profile) {
    var items = {
        nickname: 'nickname',
        age: 'age',
        gender: 'gender',
        address: 'address'
    };
    var result = {};
    for (var key in items) {
        var k = items[key];
        if (profile[k] == undefined) {
            return {err: 'No ' + k + ' in Post Body!'};
        }
        else {
            result[key] = profile[k];
        }
    }
    // gender
    if (result['gender'] == 'male') {
        result['gender'] = 1;
    }
    else{
        result['gender'] = 0;
    }
    return {
        err: null,
        res: result
    };
}

function mapDevicesList(devicesList){
    var items = {
        DevID: 'deviceId',
        DevName: 'deviceName',
        SwVer: 'software',
        HwVer: 'hardware', 
        wifi: 'wifi'
    };
    var devicesArray = [];
    var def = null;
    for (var key in devicesList) {
        var device = devicesList[key];
        // check required item
        if (device['DevInfo'] == undefined) {
            return {err: 'No DevInfo in Post Body!'};
        }
        else {
            if (device['DevInfo']['DevID'] == undefined) {
                return {err: 'No DevID in Post Body!'};
            }
            else if (device['DevInfo']['isDefault'] == undefined) {
                return {err: 'No isDefault in Post Body!'};
            }
            else {
                var deviceObj = {};
                for (var i in items) {
                    var deviceParam = items[i];
                    if (device['DevInfo'][i] !== undefined) {
                        deviceObj[deviceParam] = device['DevInfo'][i];
                    }
                }
                // city
                if (device['city'] !== undefined) {
                    deviceObj['city'] = device['city'];
                }
                else {
                    // request parse city
                    MSG.cityParseQueue.append(deviceObj['deviceId']);
                }
                devicesArray.push(deviceObj);
                // default
                if (device['DevInfo']['isDefault'] == 'YES') {
                    def = deviceObj.deviceId;
                }
            }
        }
    }
    // console.log(devicesArray);
    return {
        err: null,
        res: devicesArray,
        def: def
    };
}

// set user profile and return new user
function setUserProfile(user, userProfile, callback) {
    if (userProfile == undefined) {
        // response only
        callback(null, user);
    }
    else {
        // update user profile
        var profile = mapUserProfile(userProfile);
        if (profile.err == null) {
            // update in mysql
            process.app.models.user.update({
                userId: user.userId
            },
            profile.res,
            function (err, newUsers){
                if (err) {
                    callback(ormError.get(err));
                }
                else {
                    // sync in redis ...
                    // response new
                    callback(null, newUsers.pop());
                }
            });
        }
        else {
            callback(profile.err);
        }
    }
}

// cache devices change
// return {add: {device1: {}, device2: {}}, remove: {device3: {}, device4: {}}}
function cacheChange(user, devicesList, callback) {
    process.app.models.relation.find({
        user: user.userId
    }, function(err, relations){
        var previous = {};
        if (relations !== undefined) {
            for (var i in relations) {
                var device = relations[i]['device'];
                previous[device] = {};
            }
        }
        // console.log(previous);
        var after = {};
        for (var i in devicesList) {
            // console.log(devicesList);
            var device = devicesList[i]['deviceId'];
            after[device] = {};
        }
        // console.log(after);
        var result = {
            add : {},
            remove: {}
        };
        for (var a in after) {
            if (previous[a] == undefined) {
                result.add[a] = {};
            }
        }
        for (var p in previous) {
            if (after[p] == undefined) {
                result.remove[p] = {};
            }
        }
        // console.log(result);
        callback(result);
    });
}

// sync user devices
function syncRelation(user, newR, change){
    // console.log(change);
    var userData = RTDataManager.userData;
    var deviceData = RTDataManager.deviceData;
    var devices = {};
    for (var i in newR) {
        var device = newR[i]['device'];
        devices[device] = {
            isDefault: newR[i]['isDefault']
        };
    }
    // user - devices
    userData.setDevices(user, devices, function(){});
    // device - users
    var addKeys = [];
    for (var addId in change['add']) {
        addKeys.push(addId);
    }
    async.eachSeries(addKeys, function(addId, callback){
        deviceData.getUsers(addId, function(err, addObj){
            if (addObj == null) {
                addObj = {};
            }
            addObj[user] = {};
            deviceData.setUsers(addId, addObj, function(){
                logger.info('redis:', user, 'add', addId);
                callback();
            });
        }); 
    });
    
    var removeKeys = [];
    for (var removeId in change['remove']) {
        removeKeys.push(removeId);
    }
    async.eachSeries(removeKeys, function(removeId, callback){
        deviceData.getUsers(removeId, function(err, removeObj){
            if (removeObj == null) {
                removeObj = {};
            }
            else {
                delete removeObj[user];
            }
            deviceData.setUsers(removeId, removeObj, function(){
                logger.info('redis:', user, 'remove', removeObj);
                callback();
            });
        }); 
    });
}

// set user devices list and return new
function setUserDevices(user, devicesList, callback) {
    if (devicesList == undefined) {
        // response only
        process.app.models.relation.find({
            user: user.userId
        }, function(err, relations){
            if (relations !== undefined) {
                // console.log(relations);
                callback(null, relations);
            }
            else {
                callback('can not read devices list!');
            }
        });
    }
    else {
        // map devices list
        var devices = mapDevicesList(devicesList);
        // cache devices change
        cacheChange(user, devices.res, function(change){
            if (devices.err == null) {
                // check exist or create devices
                async.map(
                    devices.res,
                    process.app.models.device.updateOrCreate.bind(process.app.models.device),
                    function(err, newDevices) {
                        if (err) {
                            callback(err);
                        }
                        else {
                            // sync device city & wifi
                            var deviceData = RTDataManager.deviceData;
                            for (var i in newDevices) {
                                deviceData.setDataItem(newDevices[i]['deviceId'], 'city', newDevices[i]['city']);
                                if (newDevices[i]['wifi']) {
                                    deviceData.setDataItem(newDevices[i]['deviceId'], 'wifi', newDevices[i]['wifi']);
                                }
                            }
                            // check user - devices count limit
                            if (newDevices.length > appConfig.userDevicesCountLimit) {
                                // return devices list
                                var cString = "You've added more than 5 devices: ";
                                for (var i in newDevices) {
                                    cString += newDevices[i]['deviceId'] + ", ";
                                }
                                callback(cString.substring(0, cString.length - 2) + ".");
                            }
                            else{
                                // remove all devices relationship with this user
                                process.app.models.relation.resetWithUser(user.userId, function(err){
                                    if (err) {
                                        callback(ormError.get(err));
                                    }
                                    else {
                                        // add new devices relationship
                                        async.mapSeries(
                                            newDevices, 
                                            process.app.models.relation.checkAndCreate.bind(process.app.models.relation, user.userId, devices.def),
                                            function(err, newRelations) {
                                                if (err) {
                                                    callback(err);
                                                }
                                                else{
                                                    // console.log(newRelations);
                                                    // sync in redis
                                                    syncRelation(user.username, newRelations, change);
                                                    // response
                                                    callback(null, newRelations);
                                                }
                                            }
                                        );
                                    }
                                });    
                            }
                        }
                    }
                );
            }
            else {
                callback(devices.err);
            }
        });
        
    }
}

// map response device
function mapResDevices(username, devices, callback) {
    // console.log(devices);
    if (devices == undefined) {
        callback(null, []);
    }
    async.mapSeries(devices, function(device, c1){
        var item = {
            DevInfo: {
                DevID: device.deviceId, 
                DevName: device.deviceName,
                SwVer: device.software,
                HwVer: device.hardware,
                wifi: device.wifi,
                isDefault: null, 
                DevStatus: null
            },
            online_time: '', 
            unhealthy_air_m3: '', 
            city: null
        };
        // get device status
        getDeviceStatus(device.deviceId, function(err, status){
            // console.log(status);
            if (status) {
                item.DevInfo.DevStatus = status;
            }
            // get city
            getDeviceInfo(device, function(err, obj){
                // console.log(obj);
                if (obj.city !== '' && obj.city !== null) {
                    item.city = obj['city'];
                }
                else {
                    item.city = obj['ipCity'];
                }
                // get isDefault
                getIsDefault(username, device.deviceId, function(err, isDefault){
                    item.DevInfo.isDefault = (isDefault) ? 'YES' : 'NO';
                    // get online time
                    getOnlineTime(device.deviceId, function(err, times){
                        if (times) {
                            item['online_time'] = parseInt(times[0] / 1000 / 60 / 60 ) + ' Hour(s)';
                            item['unhealthy_air_m3'] = (parseInt(times[1] / 1000 / 60 / 60 ) / 24 * 12);
                        }
                        c1(null, item);
                    });
                });
            });
        });
    }, function(err, results){
        // console.log(results);
        if (results) {
            callback(null, results);
        }
        else {
            callback(null, []);
        }
    });
}

function setUserInfo(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            // get user in mysql
            process.app.models.user.findOne({
                username: username
            }, function(err, user){
                if (user == undefined) {
                    res.send(400, new Error(username + 'is not exist!'));
                }
                else {
                    // get user profile
                    var profile = req.body.profile;
                    // get user device
                    var devicesList = req.body.devices;
                    // update process
                    setUserProfile(user, profile, function(err, newUser){
                        if (err) {
                            res.send(400, new Error(err));
                        }
                        else {
                            setUserDevices(user, devicesList, function(err, newList){
                                if (err) {
                                    res.send(400, new Error(err));
                                }
                                else {
                                    // find devices
                                    var idArray = [];
                                    for (var i in newList) {
                                        idArray.push({
                                            deviceId: newList[i]['device']
                                        });
                                    }
                                    process.app.models.device.find(idArray, function(err, devices){
                                        mapResDevices(user.username, devices, function(err, resDevices){
                                           // response
                                            res.send({
                                                success: 'YES',
                                                userinfo: {
                                                    userid: user.username,
                                                    profile: {
                                                        nickname: newUser.nickname, 
                                                        age: newUser.age, 
                                                        gender: (newUser.gender) ? 'male' : 'female',
                                                        address: newUser.address
                                                    }
                                                },
                                                devices: resDevices
                                            }); 
                                        });
                                    });
                                }
                            });
                        }
                    });
                }
            }); 
        });
    });
}

function getUserInfo(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            // get user id in mysql
            process.app.models.user.findOne({
                username: username
            }, function(err, user){
                if (user == undefined) {
                    res.send(400, new Error(username + 'is not exist!'));
                }
                else {
                    process.app.models.relation.find({
                        user: user.userId
                    }, function(err, relations){
                        if (relations !== undefined) {
                            // find devices
                            var idArray = [];
                            for (var i in relations) {
                                idArray.push({
                                    deviceId: relations[i]['device']
                                });
                            }
                            process.app.models.device.find(idArray, function(err, devices){
                                mapResDevices(user.username, devices, function(err, resDevices){
                                    // response
                                    res.send({
                                        success: 'YES',
                                        userinfo: {
                                            userid: user.username,
                                            profile: {
                                                nickname: user.nickname, 
                                                age: user.age, 
                                                gender: (user.gender) ? 'male' : 'female',
                                                address: user.address
                                            }
                                        },
                                        devices: resDevices
                                    }); 
                                });
                            });
                        }
                        else {
                            res.send(400, new Error('can not read devices list!'));
                        }
                    });
                }
            });
        });
    });
}

function getDeviceStatus(deviceId, callback) {
    var deviceData = RTDataManager.deviceData;
    deviceData.getStatus(deviceId, callback);
}

function getDeviceInfo(device, callback) {
    var deviceData = RTDataManager.deviceData;
    deviceData.getData(device.deviceId, function(err, obj){
        if (obj == null) {
            obj = {};
        }
        if (obj['city'] == null) {
            obj['city'] = device.city;
        }
        callback(err, obj);
    });
}

function getIsDefault(username, deviceId, callback){
    var userData = RTDataManager.userData;
    userData.getDevices(username, function(err, devices){
        if (devices == null) {
            callback(null, null);
        }
        else if (devices[deviceId].isDefault == 1) {
            callback(null, true);
        }
        else {
            callback(null, false);
        }
    });
}

function getOnlineTime(deviceId, callback) {
    /*
    process.app.models.event.find({
        deviceId: deviceId,
        event: 'online'
    }, function(err, events){
        // console.log(events);
        if (events && events.length > 0) {
            callback(null, events[0].time);
            // get unhealthy
        }
        else {
            callback(err, null);
        }
    });
    */
    process.app.models.event.query(
        "SELECT * FROM clean.device_event WHERE device_id = '" +
        deviceId +
        "' AND event IN ('online', 'offline', 'Pm25AlertPre', 'Pm25AlertOff') ORDER BY time, device_event_id;"
    , function(err, results){
        // console.log(results);
        if (results) {
            var onlineTime = 0;
            var unhealthy = 0;
            var onlineTimeNow = 0;
            
            // temporary
            /*
            if (results && results.length > 0) {
                onlineTime = moment().valueOf() - moment(results[0].time).valueOf();
            }
            */
            
            var alertOn = null;
            var online = null
            for (var key in results) {
                var eventItem = results[key];
                
                // online time
                if (eventItem.event == 'online') {
                    online = eventItem.time;
                    // calculate online time with current time
                    var diffTime = moment().valueOf() - moment(online).valueOf();
                    onlineTimeNow = onlineTime + parseInt(diffTime);
                }
                if (eventItem.event == 'offline' && online !== null) {
                    onlineTimeNow = 0;
                    var diffTime = moment(eventItem.time).valueOf() - moment(online).valueOf();
                    onlineTime += parseInt(diffTime);
                    
                    // logger.info('calc online time: ', online, eventItem.time, onlineTime / 1000 / 60 / 60);
                    
                    online = null;
                    diffTime = 0;
                }
                
                // unhealthy time
                if (eventItem.event == 'Pm25AlertPre') {
                    // console.log(key, eventItem.time, 'alertOn');
                    alertOn = eventItem.time;
                }
                
                if (eventItem.event == 'Pm25AlertOff' && alertOn !== null) {
                    // console.log(key, eventItem.time, alertOn, 'calc');
                    var diffTime = moment(eventItem.time).valueOf() - moment(alertOn).valueOf();
                    unhealthy += parseInt(diffTime);
                    alertOn = null;
                    diffTime = 0;
                }
            }
            callback(err, [(onlineTimeNow == 0) ? onlineTime : onlineTimeNow, unhealthy]);
        }
        else {
            callback(err, null);
        }
    });
}

module.exports = {
    '/api/userinfo': {
        get: getUserInfo,
        put: setUserInfo
    }
};