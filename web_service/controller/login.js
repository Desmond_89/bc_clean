var handle = require('../lib/handleRequest');
var logger = require('../lib/log');
var RTDataManager = require('../../backend_service/lib/RTDataManager').RTDataManager;

var wechat_password = '0cdd0ad2130da107a3a8924d0293e126';
var weibo_password = 'dbd6166a0456a5aba8374a30f90738c0';

function login(req, res) {
    handle.checkHeader(req, res, function(){
        // get user input
        var input = handle.getItemsInPost(req, ['userid', 'password', 'user_type'], true);
        if (input.err) {
            res.send(400, new Error(input.err));
        }
        else {
            switch(input.res['user_type'])
            {
                case 'wechat':
                {
                    if (input.res['password'] === wechat_password) {
                        process.app.models.user.loginWeChat({
                            username: input.res['userid'],
                            password: input.res['password'],
                            user_type: 2
                        }, function(err2, user){
                            if (err2) {
                                res.send(400, new Error(err2));
                            }
                            else{
                                // sync api key in redis
                                var userData = RTDataManager.userData;
                                userData.setData(input.res['userid'], {
                                    username: input.res['userid'], 
                                    password: wechat_password, 
                                    userType: 2
                                }, function(err3, ret3){
                                    userData.setApiKey(input.res['userid'], user.apiKey, function(err4, ret4){});
                                });
    
                                // response
                                res.send({
                                    success: 'YES',
                                    userid: input.res['userid'],
                                    api_key: user.apiKey
                                });
                            }
                        });
                    }
                    else {
                        res.send(400, new Error('Incorrect Password!'));
                    }
                    break;   
                }
                case 'weibo':
                {
                    if (input.res['password'] === weibo_password) {
                        process.app.models.user.loginWeChat({
                            username: input.res['userid'],
                            password: input.res['password'],
                            user_type: 3
                        }, function(err2, user){
                            if (err2) {
                                res.send(400, new Error(err2));
                            }
                            else{
                                // sync api key in redis
                                var userData = RTDataManager.userData;
                                userData.setData(input.res['userid'], {
                                    username: input.res['userid'], 
                                    password: weibo_password, 
                                    userType: 3
                                }, function(err3, ret3){
                                    userData.setApiKey(input.res['userid'], user.apiKey, function(err4, ret4){});
                                });
    
                                // response
                                res.send({
                                    success: 'YES',
                                    userid: input.res['userid'],
                                    api_key: user.apiKey
                                });
                            }
                        });
                    }
                    else {
                        res.send(400, new Error('Incorrect Password!'));
                    }
                    break;   
                }
                default:
                {
                    // login with mysql
                    process.app.models.user.login({
                        username: input.res['userid'],
                        password: input.res['password']
                    }, function(err, user){
                        if (err) {
                            res.send(400, new Error(err));
                        }
                        else{
                            // sync api key in redis
                            var userData = RTDataManager.userData;
                            userData.setApiKey(user.username, user.apiKey, function(err2, ret2){});

                            // response
                            res.send({
                                success: 'YES',
                                userid: user.username,
                                api_key: user.apiKey
                            });
                        }
                    });
                }
            }
        }
    });
}

function logout(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            // clear api-key in mysql
            process.app.models.user.logout(username, function(err){
                if (err) {
                    res.send(400, new Error(err));
                }
                else{
                    // sync in redis
                    var apiKey = RTDataManager.apiKey;
                    apiKey.clear(username);
                    // clear apns_token for alert
                    var userData = RTDataManager.userData;
                    userData.setData(username, {
                        'mobile': null,
                        'apns_token': null
                    });
                    // response
                    res.send({
                        success: 'YES',
                        userid: username
                    });
                }
            });
        });
    });
}

module.exports = {
    '/api/login': {
        post: login,
        del: logout
    }
};