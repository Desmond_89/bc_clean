/* test say hello */
var handle = require('../lib/handleRequest');
var logger = require('../lib/log');

function helloGet(req, res) {
    handle.checkHeader(req, res, function(){
        res.send(200, handle.getQueryItem(req, res, 'hello'));
    });
}

function helloPost(req, res) {
    handle.checkHeader(req, res, function(){
        res.send(200, handle.getPostItem(req, res, 'hello'));
    });
}

function helloPut(req, res) {
    res.send({method: 'put'});
}

function helloDelete(req, res) {
    res.send({method: 'delete'});
}

module.exports = {
    '/api/hello': {
        get: helloGet,
        post: helloPost,
        put: helloPut,
        del: helloDelete
    }
};