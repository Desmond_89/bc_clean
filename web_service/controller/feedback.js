var handle = require('../lib/handleRequest');
var logger = require('../lib/log');
var moment = require('moment');
var ormError = require('../lib/handleOrmError');

function postFeedback(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            // get user id in mysql
            process.app.models.user.findOne({
                username: username
            }, function(err, user){
                if (user == undefined) {
                    res.send(400, new Error(username + 'is not exist!'));
                }
                else {
                    // get fields
                    var timestamp = moment(new Date()).format('YYYYMMDDHHmmmm'); // handle.getPostItem(req, res, 'timestamp');
                    var deviceId = handle.getPostItem(req, res, 'deviceId');
                    var message = handle.getPostItem(req, res, 'msg');
                    // find or create
                    process.app.models.feedback.create({
                        user: user.userId,
                        timestamp: timestamp,
                        message: message,
                        device: deviceId
                    }, function(err, feedback){
                        if (err) {
                            res.send(400, new Error('Device ' + deviceId + ' is not exist!'));
                        }
                        else{
                            // console.log(feedback);
                            res.send({success: 'YES'});
                        }
                    });
                }
            });
        });
    });
}

function getFeedback(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            var errorMessage;
            // get user id in mysql
            process.app.models.user.findOne({
                username: username
            }, function(err, user){
                if (user == undefined) {
                    res.send(400, new Error(username + 'is not exist!'));
                }
                else {
                    // get feedback list
                    process.app.models.feedback.find({
                        user: user.userId
                    }, function(err, feedbacks){
                        if (err) {
                            errorMessage = ormError.get(err);
                            logger.error(errorMessage);
                            res.send(400, new Error(errorMessage));
                        }
                        else{
                            if (feedbacks === undefined) {
                                res.send({feedbacks:[]});
                            }
                            else {
                                var result = new Array;
                                for (var key in feedbacks){
                                    var feedback = feedbacks[key];
                                    result.push({
                                        timestamp: moment(feedback.timestamp).format('YYYYMMDDHHmmmm'),
                                        deviceId: feedback.device, 
                                        msg: feedback.message
                                    });
                                }
                                res.send({
                                    success: 'YES',
                                    feedbacks: result
                                });
                            }
                        }
                    });
                }
            });
        });
    });
}

module.exports = {
    '/api/feedback': {
        post: postFeedback,
        get: getFeedback
    }
};