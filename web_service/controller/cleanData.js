var handle = require('../lib/handleRequest');
var logger = require('../lib/log');
var format = require('../lib/formatData');

function getCleanData(req, res) {
    // check header
    handle.checkHeader(req, res, function(){
        // get qurty string
        var username = handle.getQueryItem(req, res, 'username');
        if (!username) {
            return;
        }
        var deviceId = handle.getQueryItem(req, res, 'deviceid');
        if (!deviceId) {
            return;
        }
        var dataType = handle.getQueryItem(req, res, 'data_type');
        if (!dataType) {
            return;
        }
        // check device owner
        handle.checkDeviceOwner(req, res, username, deviceId, function(err, isOwner){
            // query time
            var time = req.query.time;
            // data type
            switch(dataType)
            {
                // 大包年数据
                case 'PackageDataYear':{
                    logger.info('PackageDataYear', deviceId, time);
                    var start = +new Date(); 
                    format.packageDataYear(deviceId, time, function(err, data){
                        if (err) {
                            res.send(400, new Error(err));
                        }
                        else{
                            var stop = +new Date();
                            console.log('PackageDataYear run: ' + (stop - start));
                            res.send(data);
                        }
                    });
                }
                break;
                // 大包年数据 - 两自然年
                case 'PackageData2Years':{
                    logger.info('PackageData2Years', deviceId, time);
                    var start = +new Date(); 
                    format.PackageData2Years(deviceId, time, function(err, data){
                        if (err) {
                            res.send(400, new Error(err));
                        }
                        else{
                            var stop = +new Date();
                            console.log('PackageData2Years run: ' + (stop - start));
                            res.send(data);
                        }
                    });
                }
                break;
                // 天内小时数据
                case 'HourlyDataInOneDay':{
                    logger.info('HourlyDataInOneDay', deviceId, time);
                    var start = +new Date(); 
                    format.HourlyDataDay(deviceId, time, function(err, data){
                        if (err) {
                            res.send(400, new Error(err));
                        }
                        else{
                            var stop = +new Date();
                            console.log('HourlyDataInOneDay run: ' + (stop - start));
                            res.send(data);
                        }
                    });
                }
                break;
                // 小时内分钟数据
                case 'MinutelyDataInOneHour':{
                    logger.info('MinutelyDataInOneHour', deviceId, time);
                    var start = +new Date(); 
                    format.MinutelyDataDay(deviceId, time, function(err, data){
                        if (err) {
                            res.send(400, new Error(err));
                        }
                        else{
                            var stop = +new Date();
                            console.log('MinutelyDataInOneHour run: ' + (stop - start));
                            res.send(data);
                        }
                    });
                }
                break;
                // 大包天数据
                case 'PackageDataDay':{
                    logger.info('PackageDataDay', deviceId, time);
                    var start = +new Date(); 
                    format.packageDataDay(deviceId, time, function(err, data){ 
                        if (err) {
                            res.send(400, new Error(err));
                        }
                        else{
                            var stop = +new Date();
                            console.log('PackageDataDay run: ' + (stop - start));
                            res.send(data);
                        }
                    });
                }
                break;
                // 当前数据
                case 'CurrentlyData':{
                    logger.info('CurrentlyData', deviceId);
                    var start = +new Date(); 
                    format.currentlyData(deviceId, function(err, data){ 
                        if (err) {
                            res.send(400, new Error(err));
                        }
                        else{
                            var stop = +new Date();
                            console.log('CurrentlyData run: ' + (stop - start));
                            res.send(data);
                        }
                    });
                }
                break;
                default:
                    res.send({'err': dataType + ' is not defined!'});
            }
        });
    });
}

module.exports = {
    '/api/cleandata': {
        get: getCleanData
    }
};