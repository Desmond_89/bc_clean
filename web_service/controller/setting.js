var handle = require('../lib/handleRequest');
var logger = require('../lib/log');
var ormError = require('../lib/handleOrmError');
var RTDataManager = require('../../backend_service/lib/RTDataManager').RTDataManager;
var MSG = require('../../backend_service/lib/RTMessage');
var moment = require('moment');
var async = require('async');

function mapDeviceParam(deviceInfo){
    var basic = {
        DevID: 'deviceId',
        DevName: 'deviceName',
        SwVer: 'software', 
        HwVer: 'hardware'
    };
    var alarm = {
        level: 'alarmLevel',
        aqi: 'alarmAqi'
    };
    var units = {
        temp: 'tempUnit',
        aqi: 'aqiUnit',
        country: 'country'
    };
    var deviceObj = {};
    // check required item
    if (deviceInfo['DevInfo'] == undefined) {
        return {err: 'No DevInfo in Post Body!'};
    }
    else {
        if (deviceInfo['DevInfo']['DevID'] == undefined) {
            return {err: 'No DevID in Post Body!'};
        }
        else {
            // basic
            for (var i in basic) {
                var deviceParam = basic[i];
                if (deviceInfo['DevInfo'][i] !== undefined) {
                    deviceObj[deviceParam] = deviceInfo['DevInfo'][i];
                }
            }
            // no disturb
            if (deviceInfo['no_disturb'] !== undefined) {
                deviceObj['noDisturb'] = JSON.stringify(deviceInfo['no_disturb']);
            }
            // smart
            if (deviceInfo['smart_learning'] !== undefined) {
                deviceObj['smart'] = (deviceInfo['smart_learning'] == 'YES') ? 1 : 0;
            }
            // alarm
            if (deviceInfo['alarm_level'] !== undefined) {
                for (var i in alarm) {
                    var deviceParam = alarm[i];
                    if (deviceInfo['alarm_level'][i] !== undefined) {
                        deviceObj[deviceParam] = deviceInfo['alarm_level'][i];
                    }
                }
            }
            // units
            if (deviceInfo['units'] !== undefined) {
                for (var i in units) {
                    var deviceParam = units[i];
                    if (deviceInfo['units'][i] !== undefined) {
                        deviceObj[deviceParam] = deviceInfo['units'][i];
                    }
                }
            }
            // volume
            if (deviceInfo['volume'] !== undefined) {
                deviceObj['volume'] = deviceInfo['volume'];
            }
            // display_off
            if (deviceInfo['display_off'] !== undefined) {
                deviceObj['lightingTime'] = deviceInfo['display_off'];
            }
            // brightness
            if (deviceInfo['brightness'] !== undefined) {
                deviceObj['brightness'] = deviceInfo['brightness'];
            }
        }
    }
    // console.log(deviceObj);
    return {
        err: null,
        res: deviceObj
    };
}

// get device disturb
function getDisturb(disturbString) {
    var disturbObj = JSON.parse(disturbString);
    var result = [];
    for (var key in disturbObj) {
        var disturb = disturbObj[key];
        if (disturb['switch'] == 'ON') {
            switch (disturb.repeat) {
                case 'everyday': {
                    result[0] = [
                        (disturb.from.slice(0, 2)),
                        (disturb.from.slice(2)),
                        (disturb.to.slice(0, 2)),
                        (disturb.to.slice(2))
                    ];
                }
                break;
                case 'never': {
                    result[1] = [
                        disturb.from,
                        disturb.to
                    ];
                }
                break;
                /*
                default: {
                    var type = disturb.repeat.slice(6);
                    // console.log(type);
                    if (type == moment().format('dddd').toLowerCase()) {
                        result.push([
                            (disturb.from.slice(0, 2)),
                            (disturb.from.slice(2)),
                            (disturb.to.slice(0, 2)),
                            (disturb.to.slice(2))
                        ]);
                    }
                }
                */
            }   
        }
    }
    // check length
    for (var i = 0; i < 5; i++) {
        if (result[i] == null || typeof(result[i]) == 'undefined') {
            result[i] = [];
        }
    }
    return result;
}

function setDevice(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            if (req.body === undefined) {
                res.send(400, new Error('No Post Body!'));
            }
            else {
                var deviceInfo = mapDeviceParam(req.body);
                if (deviceInfo.err == null) {
                    // update Device Info
                    process.app.models.device.update({
                        deviceId: deviceInfo.res.deviceId
                    }, deviceInfo.res, function(err, newDevices){
                        if (err) {
                            var errorMessage = ormError.get(err);
                            res.send(400, new Error(errorMessage));
                        }
                        else {
                            if (newDevices.length == 0) {
                                res.send(400, new Error('\'' + deviceInfo.res.deviceId + '\' does not exist!'));
                            }
                            else {
                                var newDevice = newDevices.pop();
                                var info = {
                                    HwVer : newDevice.hardware, 
                                    SwVer : newDevice.software, 
                                    DevID : newDevice.deviceId,
                                    DevName : newDevice.deviceName
                                };
                              
                                // sync in redis
                                syncSettings(newDevice);
                                // send cmd                                
                                var config = {
                                    SiDayBe: '000000000000', 
                                    SiDayEn: '000000000000',
                                    SiHrBe: 22,
                                    SiMinBe: 0,
                                    SiHrEn: 6,
                                    SiMinEn: 0,
                                    Pm25Throd: (newDevice.alarmAqi == null) ? 75 : newDevice.alarmAqi,
                                    Pm25Unit: (newDevice.aqiUnit == 'a') ? 'AQICN' : 'MG',
                                    TempUnit: (newDevice.tempUnit == 'f') ? true : false,
                                    ScnBright: (newDevice.brightness == null) ? 100 : newDevice.brightness,
                                    ScnTime: (newDevice.lightingTime == null) ? 60 : newDevice.lightingTime,
                                    VolMax: (newDevice.volume == null) ? 100 : newDevice.volume
                                };
                                
                                var d = [];
                                if (newDevice.noDisturb !== null) {
                                    d = getDisturb(newDevice.noDisturb);
                                    // console.log(d);
                                }
                                if (d.length > 0 && d[0].length > 0) {
                                    config.SiHrBe = parseInt(d[0][0]);
                                    config.SiMinBe = parseInt(d[0][1]);
                                    config.SiHrEn = parseInt(d[0][2]);
                                    config.SiMinEn = parseInt(d[0][3]);
                                }
                                if (d.length > 0 && d[1].length > 0) {
                                    config.SiDayBe = d[1][0];
                                    config.SiDayEn = d[1][1];
                                }
                                logger.info('settings send cmd: ', config);
                                MSG.deviceCmd.commands.userConfig(newDevice.deviceId, config);
                                // response
                                getUsers(newDevice.deviceId, function(userList){
                                    res.send({
                                        success: 'YES',
                                        DevInfo: info,
                                        no_disturb: JSON.parse(newDevice.noDisturb), 
                                        smart_learning: (newDevice.smart) ? 'YES' : 'NO', 
                                        alarm_level: {
                                            level: newDevice.alarmLevel,
                                            aqi: newDevice.alarmAqi
                                        },
                                        units: {
                                            temp: newDevice.tempUnit, 
                                            aqi: newDevice.aqiUnit, 
                                            country: newDevice.country
                                        },
                                        volume: newDevice.volume,
                                        display_off: newDevice.lightingTime,
                                        brightness: newDevice.brightness,
                                        users: userList
                                    });
                                });
                            }
                        }
                    });
                }
                else {     
                    res.send(400, new Error(deviceInfo.err));
                }
            }
        });
    });
}

// snyc device settings in redis
function syncSettings(device) {
    var deviceData = RTDataManager.deviceData;
    deviceData.getData(device.deviceId, function(err, deviceObj){
        // console.log(deviceObj);
        if (deviceObj == null) {
            deviceObj = {};
        }
        // param mapping
        deviceObj['DevID'] = device.deviceId;
        deviceObj['SwVer'] = device.software;
        deviceObj['HwVer'] = device.hardware;
        deviceObj['DevName'] = device.deviceName;
        deviceObj['smart'] = device.smart;
        deviceObj['aqiUnit'] = device.aqiUnit;
        deviceObj['tempUnit'] = device.tempUnit;
        deviceObj['country'] = device.country;
        deviceObj['alarmLevel'] = device.alarmLevel;
        deviceObj['alarmAqi'] = device.alarmAqi;
        deviceObj['brightness'] = device.brightness;
        deviceObj['lightingTime'] = device.lightingTime;
        deviceObj['volume'] = device.volume;
        if (device.noDisturb == null) {
            deviceObj['noDisturb'] = '[]';
        }
        else {
            deviceObj['noDisturb'] = device.noDisturb;
        }
        // console.log(deviceObj);
        // update
        deviceData.setData(device.deviceId, deviceObj, function(){});
    });
}

function getDeviceinfo(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            // get qurty string
            var deviceId = handle.getQueryItem(req, res, 'deviceid');
            process.app.models.device.findOne({
                deviceId: deviceId
            }, function(err, newDevice){
                if (err) {
                    var errorMessage = ormError.get(err);
                    res.send(400, new Error(errorMessage));
                }
                else {
                    if (newDevice === undefined) {
                        res.send(400, new Error('\'' + deviceId + '\' does not exist!'));
                    }
                    else {
                        // init new device param
                        if (newDevice.noDisturb == null){
                            newDevice.noDisturb = '[{"repeat":"never","to":"150101010000","switch":"OFF","from":"150101010000"},{"repeat":"everyday","to":"0600","switch":"ON","from":"2200"}]';
                        }
                        if (newDevice.alarmLevel == null){
                            newDevice.alarmLevel = 'Normal';
                        }
                        if (newDevice.alarmAqi == null){
                            newDevice.alarmAqi = 75;
                        }
                        if (newDevice.tempUnit == null){
                            newDevice.tempUnit = 'c';
                        }
                        if (newDevice.aqiUnit == null){
                            newDevice.aqiUnit = 'u';
                        }
                        if (newDevice.country == null){
                            newDevice.country = 'cn';
                        }
                        if (newDevice.volume == null){
                            newDevice.volume = 100;
                        }
                        if (newDevice.lightingTime == null){
                            newDevice.lightingTime = 60;
                        }
                        if (newDevice.brightness == null){
                            newDevice.brightness = 100;
                        }
                        
                        getUsers(deviceId, function(userList){
                           res.send({
                                success: 'YES',
                                DevInfo: {
                                  HwVer : newDevice.hardware, 
                                  SwVer : newDevice.software, 
                                  DevID : deviceId,
                                  DevName : newDevice.deviceName
                                },
                                no_disturb: JSON.parse(newDevice.noDisturb), 
                                smart_learning: (newDevice.smart) ? 'YES' : 'NO', 
                                alarm_level: {
                                    level: newDevice.alarmLevel,
                                    aqi: newDevice.alarmAqi
                                },
                                units: {
                                    temp: newDevice.tempUnit, 
                                    aqi: newDevice.aqiUnit, 
                                    country: newDevice.country
                                },
                                volume: newDevice.volume,
                                display_off: newDevice.lightingTime,
                                brightness: newDevice.brightness,
                                users: userList
                            }); 
                        });
                    }
                }
            });
        });
    });
}

// mute
function mute(req, res) {
    handle.checkHeader(req, res, function(){
        // auth by api-key
        handle.validateApiKey(req, res, process.app.models.user, function(err, username){
            var input = handle.getItemsInPost(req, ['deviceid', 'judge_result', 'alarm_timestamp'], true);
            if (input.err) {
                res.send(400, new Error(input.err));
            }
            else{
                // console.log(input.res);
                MSG.deviceCmd.commands.mute(input.res.deviceid);
                res.send({success: 'YES'});
            }
        });
    });
}

// get users  list and usename
function getUsers(deviceId, callback) {
    process.app.models.relation.find({
        device: deviceId
    }, function(err, relations){
        if (err) {
            callback({});
        }
        else {
            // return already users list
            var reArray = {};
            var userArray = [];
            for (var i in relations) {
                var theId = relations[i]['user'];
                userArray.push(theId);
            }
            // callback(userArray);
            async.mapSeries(
                userArray,
                function(theUserId, cb){
                    // get username and nickname
                    process.app.models.user.findOne({userId: theUserId}, function(err, user){
                        if (!err) {
                            reArray[user['username']] = user['nickname'];
                        }
                        cb(); 
                    });
                },
                function(err) {
                    callback(reArray);
                }
            );
        }
    });
}

module.exports = {
    '/api/settings': {
        put: setDevice,
        get: getDeviceinfo
    },
    '/api/mute': {
        post: mute
    }
};