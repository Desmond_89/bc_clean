var handle = require('../lib/handleRequest');
var logger = require('../lib/log');
var ormError = require('../lib/handleOrmError');
var RTDataManager = require('../../backend_service/lib/RTDataManager').RTDataManager;
var MSG = require('../../backend_service/lib/RTMessage');
var moment = require('moment');
var uuid = require('node-uuid');

// check valid time
function checkTime(timeString){
    var timestamp = moment(timeString).valueOf();
    if (timestamp >= (Date.now() - 10*60*1000)) {
        return true;
    }
    else {
        return false;
    }
}

function validateCode(req, res) {
    handle.checkHeader(req, res, function(){
        var username = handle.getQueryItem(req, res, 'user_id');
        // email
        var mail = MSG.mailQueue;
        var codeEmail = {
            to: username, 
            subject: 'Reset Password', 
            text: ''
        };
        // check exist from redis
        var userData = RTDataManager.userData;
        userData.getData(username, function(err, user){
            // console.log(user);
            if (user !== null) {
                if (user['userType'] == 1) {
                    // set code
                    process.app.models.resetcode.make(username, function(err2, resetcode){
                        // console.log(resetcode);
                        if (err2) {
                            res.send(400, new Error(err2));
                        }
                        else {
                            // send code email
                            codeEmail.text = '重置密码，验证码：' + resetcode.resetCode + '，有效时间10分钟。';
                            mail.append(codeEmail, function(err3, mailQueue){});
                            res.send({'success': 'YES'}); 
                        }
                    });
                }
                else {
                    res.send(400, new Error(username + ' can not reset password!'));
                }
            }
            else {
                // check exist from mysql
                process.app.models.user.findOne({
                    username: username,
                    userType: 1
                }, function(err3, mysqlUser){
                    if (mysqlUser !== undefined) {
                        // set code
                        process.app.models.resetcode.make(username, function(err2, resetcode){
                            // console.log(resetcode);
                            if (err2) {
                                res.send(400, new Error(err2));
                            }
                            else {
                                // send code email
                                codeEmail.text = '重置密码，验证码：' + resetcode.resetCode + '，有效时间10分钟。';
                                mail.append(codeEmail, function(err3, mailQueue){});
                                res.send({'success': 'YES'}); 
                            }
                        });
                    }
                    else {
                        res.send(400, new Error(username + ' is not exist!'));
                    }
                });
            }
        });
    });
}

function checkcode(req, res) {
    handle.checkHeader(req, res, function(){
        // get user input
        var input = handle.getItemsInPost(req, ['userid', 'code', 'password'], true);
        if (input.err) {
            res.send(400, new Error(input.err));
        }
        else {
            // check code
            process.app.models.resetcode.findOne({
                username: input.res['userid']
            }, function(err, code){
                if (code !== undefined) {
                    if (code.resetCode == input.res['code'] && checkTime(code.time)) {
                        // reset password
                        process.app.models.user.update({
                            username: input.res['userid'],
                            userType: 1, 
                        }, {
                            password: input.res['password'],
                            apiKey: uuid.v4()
                        }, function(err2, newUsers) {
                            if (err2) {
                                var errerMessage = ormError.get(err2);
                                res.send(400, new Error(errerMessage));
                            }
                            else {
                                var newUser = newUsers.pop();
                                // sync in redis
                                var userData = RTDataManager.userData;
                                userData.setData(input.res['userid'], {
                                    username: input.res['userid'], 
                                    password: input.res['password'],
                                }, function(err3, ret3){
                                    // sync api key
                                    userData.setApiKey(input.res['userid'], newUser.apiKey, function(err4, ret4){});
                                });
                                // clear reset code
                                process.app.models.resetcode.destroy({username: input.res['userid']}).exec(function(){});
                                // response
                                res.send({
                                    success: 'YES',
                                    userid: input.res['userid'],
                                    api_key: newUser.apiKey
                                });
                            }
                        });
                    }
                    else{
                        res.send(400, new Error('Invalid code!'));
                    }
                }
                else{
                    res.send(400, new Error('Invalid code!'));
                }
            });
        }
    });
}

module.exports = {
    '/api/validatecode': {
        get: validateCode
    },
    '/api/checkcode': {
        post: checkcode
    }
};