var http = require('http');
var fs = require('fs');
var crypto = require('crypto');

var clientDict = {
    'b153e898-3531-4bee-8f20-e5329e37a353': 'cd8b5315-369d-4fca-ae72-169a9d563469'
};

var request = http.request({
    hostname : 'localhost',
    // hostname: '120.24.93.163',
    port : '1337',
    path : '/api/picture',
    method : 'POST',
}, function (response) {
    
    console.log('STATUS: ' + response.statusCode);
    // console.log('HEADERS: ' + JSON.stringify(response.headers));
    
    var data = '';
    response.on('data', function(chunk) {
        data += chunk.toString();
    });
    response.on('end', function() {
        console.log(data);
    });
})
.on('error', function(e) {
    console.log("error: " + e.message);
});

var boundaryKey = 'uploadtest';// Math.random().toString(16); // random string
var type = 'multipart/form-data; boundary=' + boundaryKey + '';
var enddata = '--' + boundaryKey + '--';

var payload = '--' + boundaryKey + '\r\n' +
              'Content-Type: image/jpeg\r\n' +
              'Content-Disposition: form-data; name="picture"; filename="zm1.jpg"\r\n' +
              'Content-Transfer-Encoding: binary\r\n\r\n';

var md5 = crypto.createHash('md5').update(payload).digest('base64');
var timestamp = Math.round(new Date().getTime()/1000);
var random = Math.floor(Math.random() * ( 1000 + 1));
var key = 'cd8b5315-369d-4fca-ae72-169a9d563469';
var clientId = 'b153e898-3531-4bee-8f20-e5329e37a353';

// create authorization
content = 'POST\n' +
          type + '\n' +
          md5 + '\n' +
          timestamp + '\n' +
          random + '\n' +
          '/api/picture\n';
console.log(content);
var hmacBase64 = crypto.createHmac('sha256', clientDict[clientId]).update(content).digest('base64');
var authorization = 'Clean ' + clientId + ':' + hmacBase64;
console.log(authorization);


request.setHeader('Content-Type', type);
request.setHeader('Content-MD5', md5);
request.setHeader('UTC-Timestamp', timestamp);
request.setHeader('Nonce', random);
request.setHeader('Authorization', authorization);
request.setHeader('api-key', '1');
// request.setHeader('Content-Length', Buffer.byteLength(payload) + Buffer.byteLength(enddata) + fileStream.size);

console.log('header');

request.write(payload);

console.log('write');

var fileStream = fs.createReadStream('../upload/zm1.jpg', { bufferSize: 4 * 1024 })
    .on('end', function() {
        console.log('end');
        request.end('\r\n' + enddata); 
    })
    .pipe(request, {end: false});
    