process.title = 'commServer';
var os = require('os');
var util = require('util');
var resources = require('./lib/resources');
var logger = resources.logger;
var RTDATA = require('./lib/RTDataManager').RTDataManager;
// var RTMessage = require('./lib/RTMessage');
var sendMail = require('./lib/mailHelper');
var port = 7777;

require('./lib/processNotify');
logger.info('start ' + process.title);
var NetServer = require('./lib/netServer');
require('./lib/snapshot');

function startNetServer() {
	new NetServer().start(port);
	logger.info('commServer started on port ' + port);
}

// ---------------init-----------------
// reset RTDATA
RTDATA.init(startNetServer);
// ---------------init end-------------

