var MSG = require('../lib/RTMessage');
var CMDs = require('../lib/BCDevice').requestCmds;
var l = console.log;


describe('RTMessage', function() {
	var client;
	describe('device commands', function() {
		var cmdListener = MSG.deviceCmd.listen();
		var device = '93000200B074';
		var userConfig = {
			SiDayBe: '150108203622',
			SiDayEn: '150109203622',
			SiHrBe: 6,
			SiHrEn: 22,
			Pm25Throd: 80,
			Pm25Unit: 'MG',
			TempUnit: false,
			ScnBright: 90,
			ScnTime: 10,
			VolMax: 95
		};
		var count =0
		var count2 = 10;
		var t = Date.now();
		var reso = 'Min';
		var unit = 'Day';
		it('should get commands which been sent', function(done){
			// l(MSG.deviceCmd.commands);
			// syncTime: syncTime
			// userConfig: userConfig
			// recQuery: recQuery
			// mute: mute
			cmdListener.on('message', function (cmdMsg) {
				l('recv device cmd:', cmdMsg);
				cmdMsg.DevID.should.equal(device);
				switch(cmdMsg.cmd) {
					case 'mute':
						cmdMsg.args.length.should.equal(0);
						count++;
						break;
					case 'userConfig':
						// l('userconfig args: ', cmdMsg.args);
						JSON.stringify(cmdMsg.args[0]).should.equal(JSON.stringify(userConfig));
						count += 2;
						break;
					case 'recQuery':
						// l('userconfig args: ', cmdMsg.args);
						cmdMsg.args[0].should.equal(t);
						cmdMsg.args[1].should.equal(reso);
						cmdMsg.args[2].should.equal(unit);
						count += 4;
						break;
					case 'syncTime':
						// l('userconfig args: ', cmdMsg.args);
						count += 3;
						break;
					default:
						l('recieve unrecognized cmd:', cmdMsg.cmd);
				}
				if (count === count2)
					done();
			});
			MSG.deviceCmd.commands.mute(device);
			MSG.deviceCmd.commands.userConfig(device, userConfig);
			MSG.deviceCmd.commands.syncTime(device);
			MSG.deviceCmd.commands.recQuery(device, t, reso, unit);
		});
	});
	describe('APNsQueue', function() {
		var APNsMessage = {
			token: 'iphone APNs token',
			title: 'alert',
			o2: {
				a: 1,
				b: 'something'
			}
		};
		it('should get message which been put', function(done){
			MSG.APNsQueue.pop(function getAPNsMessage(err, msgObj) {
				l('get apns message object:', msgObj);
				JSON.stringify(msgObj).should.equal(JSON.stringify(APNsMessage));
				done();
			});
			MSG.APNsQueue.append(APNsMessage);
		});
	});
	describe('deviceAlertQueue', function() {
		it('should get message which been put', function(done){
			done();
		});
	});
	describe('mailQueue', function() {
		it('should get message which been put', function(done){
			done();
		});
	});
});