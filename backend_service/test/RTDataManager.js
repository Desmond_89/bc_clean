var P = require('../lib/RTDataManager');
var RTDataManager = P.RTDataManager;
var T = P.test;
var log = require('debug')('RTTest');
var util = require('util');
var moment = require('moment');
var async = require('async');

function od(o) {
	log(util.inspect(o,{depth: null}));
}

function oe(o1, o2) {
	JSON.stringify(o1).should.equal(JSON.stringify(o2));
}

describe('RTDataManager', function() {
	describe('currentData', function() {
		it('should get the same value of set', function(done){
			var device = '7300G100B074';
			var data = [88, 22, 66];
			var curr = RTDataManager.currentData;
			curr.set(device, data, Date.now(), function(err, res){
				curr.get(device, function(err, res) {
					log('res: ', res);
					res.should.be.instanceof(Array).and.have.lengthOf(2);
					JSON.stringify(res[1]).should.equal(JSON.stringify(data));
					done();
				});
			});
		});
		it('get data from a device of no data', function(done) {
			var device = 'xx730fdf0G1df00B0df74';
			var curr = RTDataManager.currentData;
			curr.get(device, function(err, res) {
				(res === null).should.be.true;
				done();
			});
		});
	});
	describe('yearData', function() {
		it('should get the same value of set', function(done) {
			// 
			var device = '7300G100B074';
			var year = RTDataManager.yearData;
			var month = RTDataManager.monthData;
			var day = RTDataManager.dayData;
			var data = {
				aqi: 123,
				disturb: [[1,2,3,4], [1,2,3,4], [1,2,3,4], [1,2,3,4], [1,2,3,4]],
				rank: [23, 45]
			};
			var monthData = 56;
			year.create(device, Date.now(), function(err, resCreate) {
				day.set(device, Date.now(), data, function(err, res){
					month.set(device, Date.now(), monthData, function(err, resM) {
						year.get(device, Date.now(), function(err, resYear) {
							var now = moment();
							var MM = now.month();
							var DD = now.date()-1;
							// od(resYear.month[MM]);
							od(resYear.month[MM].aqi);
							resYear.month[MM].aqi.should.equal(monthData);
							od(resYear.month[MM].day[DD]);
							oe(resYear.month[MM].day[DD], data);
							done();
						});
					});
				});

			});
		});
		it('get year data from a device of no data', function(done) {
			var device = 'xx730fdf0G1df00B0df74';
			var year = RTDataManager.yearData;
			year.get(device, Date.now(), function(err, res) {
				(res === null).should.be.true;
				done();
			});
		});
	});
	describe('dayData', function() {
		it('should get the same value of set', function(done) {
			var device = 'dev_123456';
			var day = RTDataManager.dayData;
			var hour = RTDataManager.hourData;
			var now = moment();
			var nowT = now.valueOf();
			var minute = RTDataManager.minuteData;
			var mData = 123;
			var hData = 234;
			day.create(device, nowT, function(err, res1){
				hour.set(device, nowT, hData, function(err, res2){
					day.get(device, nowT, function(err, res3) {
						od(res3);
						res3.hour[now.hour()].aqi.should.equal(hData);
						done();
					});
				});
			});
		});
		it('get day data from a device of no data', function(done) {
			var device = 'xx730fdf0G1df00B0df74';
			var day = RTDataManager.dayData;
			day.get(device, Date.now(), function(err, res) {
				(res === null).should.be.true;
				done();
			});
		});
	});

	describe('hourData', function() {
		it('should get the same value of set', function(done) {
			var device = '7300G100B074';
			var day = RTDataManager.dayData;
			var hour = RTDataManager.hourData;
			var now = moment();
			var nowT = now.valueOf();
			var minute = RTDataManager.minuteData;
			var mData = 123;
			var hData = 234;
			hour.create(device, nowT, function(err, res1){
				minute.set(device, nowT, mData, function(err, res2){
					hour.get(device, nowT, function(err, res3) {
						od(res3);
						res3.minute[now.minute()].aqi.should.equal(mData);
						done();
					});
				});
			});
		});
	});
	// describe('disturb', function() {
	// 	it('should get the same value of set', function(done) {
	// 		var device = '7300G100B074';
	// 		var disturb = RTDataManager.disturb;
	// 		disturb.set(device, Date.now(), function(err, res) {
	// 			disturb.get(device, Date.now(), function(err, resD) {
	// 				// 
	// 				// od(resD);
	// 				var dayIndex = moment().dayOfYear();
	// 				// log('len: ', resD.length);
	// 				// log(dayIndex-1, ':', resD[dayIndex-1]);
	// 				// log(dayIndex, ':', resD[dayIndex]);
	// 				// log(dayIndex+1, ':', resD[dayIndex+1]);
	// 				// resD[dayIndex-1].should.be.Boolean.and.equal(false);
	// 				resD[dayIndex].should.be.Boolean.and.equal(true);
	// 				// resD[dayIndex+1].should.be.Boolean.and.equal(false);
	// 				done();
	// 			});
	// 		});
	// 	});
	// 	it('get disturb data from a device of no data', function(done) {
	// 		var device = 'xx730fdf0G1df00B0df74';
	// 		var disturb = RTDataManager.disturb;
	// 		disturb.get(device, Date.now(), function(err, res) {
	// 			(res === null).should.be.true;
	// 			done();
	// 		});
	// 	});
	// });

	describe('rank', function() {
		it('rank of aqi', function(done) {
			var device = '7300G100B074';
			var rank = RTDataManager.rank;
			rank.get(device, function(err, resR) {
				log('err:', err);
				// 
				log('rank of ', device, ' is ', resR);
				resR[0].should.within(0,1);
				done();
			});
		});
		it('rank of unknown aqi', function(done) {
			var device = '730sdf0G1sdf00dfB074';
			var rank = RTDataManager.rank;
			rank.get(device, function(err, resR) {
				// 
				log('rank of ', device, ' is ', resR);
				(resR[0] === null).should.be.true;
				done();
			});
		});
	});

	describe('outside aqi reference', function() {
		it('should get the same value of set', function(done) {
			var refAqi = RTDataManager.refAqi;
			var city = 'Shanghai';
			var aqi = 130;
			var data = {};
			data[city] = aqi;
			refAqi.set(data, function(err, res){
				refAqi.get(city, function(err, resA){
					resA.should.be.Number.and.equal(aqi);
					done();
				});
			});
		});

		it('unknown city', function(done) {
			var refAqi = RTDataManager.refAqi;
			var city = 'zdfsdfShansdfgsfdhai';
			var aqi = 130;
			var data = {};
			data[city] = aqi;
			refAqi.get(city, function(err, resA){
				log('aqi of unknown city: ', resA);
				(resA === null).should.be.true;
				done();
			});
		});
	});
	// describe('performance - write redis', function() {
	// 	it('should less than 6 seconds', function(done) {
	// 		var runTimes = 1000;
	// 		var begin = new Date().getTime();
	// 		var device = '7300G100B074';
	// 		var day = RTDataManager.dayData;
	// 		var minute = RTDataManager.minuteData;
	// 		var dataArray = new Array;
	// 		for (var i=0; i < runTimes; i++) {
	// 			dataArray.push([[88, 22, 66]]);
	// 		}
	// 		async.map(dataArray, function(item, callback){
	// 			minute.set(device, Date.now(), item, function(err, minuteR){
	// 				day.get(device, Date.now(), function(err, dayR) {
	// 					var minuteIndex = moment().hour()*60 + moment().minute();
	// 					oe(dayR.minuteData[minuteIndex], item[0]);
	// 					callback(err, null);
	// 				});
	// 			});
	// 		}, function(err, results){
	// 			// 
	// 			var end = new Date().getTime();
	// 			var spend = end - begin;
	// 			log('run ', runTimes, ' set/get spend: ', spend, 'ms');
	// 			(spend < 12*1000).should.be.true;
	// 			done();
	// 		});
	// 	});
	// 	it('write without callback should be quicker', function(done) {
	// 		var runTimes = 10000;
	// 		var begin = new Date().getTime();
	// 		var device = '7300G100B074';
	// 		var day = RTDataManager.dayData;
	// 		var minute = RTDataManager.minuteData;
	// 		// var dataArray = new Array;
	// 		var value = [88, 22, 66];
	// 		for (var i=0; i < runTimes; i++) {
	// 			// dataArray.push([[88, 22, 66]]);
	// 			minute.set(device, Date.now(), [value]);
	// 		}
	// 		var end = new Date().getTime();
	// 		var spend = end - begin;
	// 		log('run ', runTimes, ' set without callback spend: ', spend, 'ms');
	// 		(spend < 12*1000).should.be.true;
	// 		done();
	// 	});
	// });
});