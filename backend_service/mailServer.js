// send mail service here
process.title = 'mailServer';
var MSG = require('./lib/RTMessage');
var resources = require('./lib/resources');
// var client = resources.getRedis();
var logger = resources.logger;

var debug = require('debug')('mailServer');

var mailer = require('nodemailer');

require('./lib/processNotify');
logger.info('start mail server');

var transporter = mailer.createTransport({
	port: 25,
	host: 'smtp.bosch-smartlife.com',
	auth: {
		user: 'Bosch-IAQ@bosch-smartlife.com',
		pass: 'Bosch1234'
	}
});

function listenMailQueue(handleMailMessage) {
	MSG.mailQueue.pop(function(err, apnsRequest) {
		setImmediate(handleMailMessage,err, apnsRequest);
		setImmediate(listenMailQueue, handleMailMessage);
	});
}

function handlerMailRequest(err, msgObj) {
	if(err)logger.warn('pop email message error: ' + err);
	sendMail(msgObj);
}

function sendMail(msgObj, cb) {
	logger.info('send mail:'+msgObj);
	var time = process.hrtime();
	// 
	// {
		// from: 'Bosch-IAQ@bosch-smartlife.com',
		// to: 'john.wei.zhao@aliyun.com, desmond.zhang@aliyun.com',
		// subject: 'greeting from BOSCH IAQ mailer ' + Date.now(),
		// text: 'It works!'
		// // html: '<b>World</b>'
	// }
	msgObj.from = 'Bosch-IAQ@bosch-smartlife.com';
	transporter.sendMail(msgObj, function(err, info) {
		if (err) logger.warn('send mail error:'+err);
		logger.info(info);
		var diff = process.hrtime(time);
		debug('send mail took %d ms', diff[0] * 1e3 + diff[1]/1e6);
		if(cb) cb();
	});
}

listenMailQueue(handlerMailRequest);

logger.info('mail server started.');
