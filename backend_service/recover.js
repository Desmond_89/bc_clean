var RTDataManager = require('./lib/RTDataManager').RTDataManager;

var async = require('async');
var moment = require('moment');

PARALLEL_COUNT = 20;

// only save real devices list
function recoverReal(){
    var deviceList = [
        '030083009174', 
        '220083009174', 
        '330083009174', 
        '520053009174', 
        '620083009174', 
        '710043009174', 
        '8300B100B074', 
        '910053009174', 
        '920083009174', 
        '9300D100B074', 
        'C300F2004174', 
        'F200C2004174', 
        'G10053009174', 
        'G10083009174'
    ];
    recover(deviceList);
}

// recover
function recover(list){
    yearData = RTDataManager.yearData;
    dayData = RTDataManager.dayData;
    hourData = RTDataManager.hourData;
    // recover from oss
    async.eachLimit(list, PARALLEL_COUNT, function(deviceId, callback){
        if (deviceId) {
            // day
            /*
            dayData.get(deviceId, moment().valueOf(), function(err, resDay){
                // console.log(resDay);
                dayData.reload(deviceId, moment().valueOf(), function(err){
                    dayData.get(deviceId, moment().valueOf(), function(err, resDay2){
                        if (resDay.toString() == resDay2.toString()) {
                            console.log('recover day:', deviceId);
                        }
                        else{
                            console.log(resDay);
                            console.log(resDay2);
                        }
                    });
                });
            });
            */
            dayData.reload(deviceId, moment().valueOf(), function(err){
                console.log('recover day:', deviceId);
            });
            
            
            // year
            /*
            yearData.get(deviceId, moment().valueOf(), function(err, resDay){
                // console.log(resDay);
                yearData.reload(deviceId, moment().valueOf(), function(err){
                    yearData.get(deviceId, moment().valueOf(), function(err, resDay2){
                        if (resDay != null) {
                            if (resDay.toString() == resDay2.toString()) {
                                console.log('recover year:', deviceId);
                            }
                            else{
                                console.log(resDay);
                                console.log(resDay2);
                            }
                        }
                        else{
                            console.log(resDay2);
                        }
                    });
                });
            });
            */
            yearData.reload(deviceId, moment().valueOf(), function(err){
                console.log('recover year:', deviceId);
            });
        }
    });
}

recoverReal();