// todo: application service here
process.title = 'appServer';
var resources = require('./lib/resources');
var logger = resources.logger;

require('./lib/processNotify');
logger.info('app server started.');

// 1. save data periodically
var saveOss = require('./lib/saveOss');
saveOss.run();

// 2. get reference aqi periodically
var refAqi = require('./lib/referenceAqi');
refAqi.task();

// 3. parse ip to city if city is not set
require('./lib/ipParseCity');

// 4. calculate average value, minute, hour, day, month, year
var calcAvg = require('./lib/calcAvg');


// 5. apns on time
var apns = require('./lib/apnsOnTime');
apns.run();

// 6. save event
var saveEvent = require('./lib/saveEvent');

// 7. call up
var callUp = require('./lib/callUp');

// 8. clear redis
var clearRedis = require('./lib/clearRedis');
clearRedis.run();

// waterline orm
var orm = require('../web_service/lib/orm');
orm(function(err, models){
    if(err) {
        throw err;
    }
    // db handle
    db = models.collections;
	
    // 4. calculate average value
    calcAvg.run(db);
    
    // 5. apns event
    apns.send(db);
    
    // 6. save event
    saveEvent.run(db);
	
    // 7. call up
    callUp.run(db);
});
