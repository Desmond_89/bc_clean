var RTDataManager = require('./RTDataManager').RTDataManager;
var logger = require('./logging');

var async = require('async');
var moment = require('moment');

var later = require('later');
later.date.localTime();

PARALLEL_COUNT = 100;

// 
function save(db) {
    // get devices list
    var deviceEvent = RTDataManager.deviceEvent;
    deviceEvent.list(function(err, list){
        // console.log(list);
        
        // for test
        // list = ['620083009174'];
        
        if (list) {
            // traverse
            async.eachLimit(list, PARALLEL_COUNT, function(deviceId, callback){
                // get current events
                deviceEvent.get(deviceId, function(err, events){
                    // console.log(events);
                    if (events) {
                        // make obj
                        var input = [];
                        for (var key in events) {
                            var event = JSON.parse(events[key]);
                            var obj = {
                                deviceId: deviceId, 
                                event: '', 
                                time: '', 
                                arg: null, 
                            };
                            if (event[0]) {
                                obj.time = moment(parseInt(event[0])).format('YYMMDDHHmmss');
                            }
                            if (event[1]['Type']) {
                                obj.event = event[1]['Type'];
                            }
                            if (obj.event.indexOf('Pm25') >= 0) {
                                obj.arg = event[1]['pm25'];
                            }
                            if (obj.event !== '') {
                                input.push(obj);
                            }
                        }
                        // console.log(input);
                        // save in db
                        db.event.create(input, function(err){
                            if (err) {
                                // console.log(err);
                                logger.info('save events error: ', err);
                            }
                            else {
                                // console.log('save events ' + deviceId);
                                logger.info('save events: ', deviceId);
                                callback();
                                // clear events
                                deviceEvent.clear(deviceId, function(err){
                                    if (!err) {
                                        // console.log('clear events ' + deviceId);
                                        logger.info('clear events: ', deviceId);
                                    }
                                    callback();
                                });
                            }
                        });
                    }
                });
            });
        }
    });
}

// 
function saveEvent(db) {
    // first run
    save(db);
    // run on time
    later.setInterval(function(){
        save(db);
    }, later.parse.cron('30 * * * *'));
}

// for test
// saveEvent();

module.exports = {
    run: saveEvent
};