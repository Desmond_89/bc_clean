var RTDataManager = require('./RTDataManager').RTDataManager;
var MSG = require('./RTMessage');
var logger = require('./logging');

var async = require('async');
var moment = require('moment');
var later = require('later');

later.date.localTime();
var PARALLEL_COUNT = 10;

/*
	callTimer = {
		deviceId: timerObject
	}
*/
var callTimer = {};
var synTimer = {};

// format timestamp to YYMMDDHHmmss
function formatDate(time){
	return moment(parseInt(time)).format('YYMMDDHHmmss');
}

// manage task timer with deviceId
function getTimer(deviceId, type){
	var timerObj = null;
	switch(type){
		case 'callup': {
			timerObj = callTimer;
		}
		break;
		case 'sync': {
			timerObj = synTimer;
		}
		break;
	}
	return (typeof(timerObj[deviceId]) == "undefined") ? null : timerObj[deviceId];
}

// process entry
function main(db) {
	// receive device event
	var deviceEvents = MSG.deviceEventChannel; 
	deviceEvents.sub(function(eventObj) {
		logger.info('receive device event:', eventObj);
		var deviceId = eventObj['DevID'];
		var event = eventObj['event'];
		// device 'online'
		if (deviceId && event == 'online'){
			// call up 
			callup(deviceId, db);
			// save SwVer
			saveSwVer(deviceId);
			// sync time every 24 hours
			synTimer[deviceId] = setTimeout(sendSyncTime, 24*60*60*1000, deviceId);
		}
		// device 'offline'
		else if (deviceId && event == 'offline'){
			// clear call up timer
			var timer = getTimer(deviceId, 'callup');
			if (timer){
				clearTimeout(timer);
				logger.info('clear call up timer when offline:', deviceId);
			}
			// clear syncTime timer
			var timer2 = getTimer(deviceId, 'sync');
			if (timer2){
				clearTimeout(timer2);
				logger.info('clear sync timer when offline:', deviceId);
			}
		}
	});
	
	// receive history message
	var resHistory = MSG.receiveHistoryChannel; 
	resHistory.sub(function(recObj) {
		var deviceId = recObj['DevID'];
		var timestamp = moment(recObj['Date'], 'YYMMDDHHmmss').valueOf();
		logger.info('receive history:', deviceId, formatDate(timestamp));
		// receive process
		receiveProcess(deviceId, timestamp, db);
	});
}

// save device SwVer from redis to mysql
function saveSwVer(deviceId){
	var deviceData = RTDataManager.deviceData;
	deviceData.getDataItem(deviceId, 'SwVer', function(err, value){
		if (value) {
			db.device.update({deviceId: deviceId}, {software: value}, function(err, newDevice){
				if (!err){
					logger.info('update SwVer:', deviceId, value);
				}
			});
		}
	});
}

// send sync command
function sendSyncTime(deviceId){
	MSG.deviceCmd.commands.syncTime(deviceId);
}

// exec call up task one by one interval 5 seconds
function callup(deviceId, db){
	// clear previous process timer
	var timer = getTimer(deviceId, 'callup');
	if (timer){
		clearTimeout(timer);
	}
	// process
	getCallupTask(deviceId, db, function(err, callupObj){
		if (callupObj){
			sendCallupCommand(callupObj, db, function(err){
				// set self-timeout to make process go on where no response
				callTimer[deviceId] = setTimeout(callup, 5*1000, deviceId, db);
			});
		}
	});
}

// get first call up task near 2 months
/*
	callupObj: {
		callUpId: 3235,
		deviceId: 330083009174, 
		callUpType: minute,  
		callUpTime: 1440208800000
	}
 */
function getCallupTask(deviceId, db, callback){
	// query in mysql
	db.callup.findOne({
		where: {
			deviceId: deviceId, 
			callUpType: 'minute'
		},
		limit: 1,
		sort: 'callUpTime'
	}, function(err, callupObj){
		if (err) {
			logger.warn('query callup task error:', err);
			callback(null, null);
		}
		else{
			if (callupObj){
				var itemTime = parseInt(callupObj['callUpTime']);
				if (moment(itemTime).month() >= (moment().month() - 1)){	// near 2 month
					logger.info('get call up task:', callupObj['callUpId'], deviceId, formatDate(itemTime));
					callback(null, callupObj);
				}
				else{
					logger.info('no call up task in 2 month:', deviceId);
					callback(null, null);
				}
			}
			else{
				logger.info('no call up task:', deviceId);
				callback(null, null);
			}
		}
	});
}

// send call up commend
function sendCallupCommand(callupObj, db, callback){
	var deviceId = callupObj['deviceId'];
	var timestamp = parseInt(callupObj['callUpTime']);
	// create hour table before call up
	var hourData = RTDataManager.hourData;
	hourData.create(deviceId, timestamp, function(err){
		if (err){
			logger.warn('call up, create hour table error:', err);
		}
		// send command
		MSG.deviceCmd.commands.recQuery(deviceId, timestamp, 'Min', 'Hour');
		logger.info('send recQuery command:', deviceId, formatDate(timestamp));
		callback(null);
	});
}

// receive process
function receiveProcess(deviceId, timestamp, db){
	// get integral point hour timestamp
	var time = moment(moment(parseInt(timestamp)).format('YYMMDDHH0000'), 'YYMMDDHHmmss').valueOf();
	// delete call up task when xx:00 package data
	if (moment(timestamp).format('mm') == '00'){
		deleteTask(deviceId, 'minute', time, db, function(err){
			// run next call up task
			callup(deviceId, db);
		});
	}
	// re-calculate average when xx:50 package data
	if (moment(timestamp).format('mm') == '50'){
		// wait 1 second to set minute data
		setTimeout(getRecalcTask, 1000, deviceId, time, db);
	}
}

// delete call up / re-calc task in mysql db 
function deleteTask(deviceId, type, timestamp, db, callback){
	// delete in mysql
	db.callup.destroy({
		deviceId: deviceId, 
		callUpType: type, 
		callUpTime: timestamp
	}, function(err){
		if (err){
			logger.warn('delete task error:', err);
		}
		else{
			logger.info('delete task:', deviceId, type, formatDate(timestamp));
		}
		callback(null);
	});
}

// check task type
function isTask(task, target){
	var res = null;
	for (var key in task){
		var type = task[key]['callUpType'];
		if (type == target){
			res = task[key];
			break;
		}
	}
	return res;
}

// sort task order by 'hour', 'day', 'month'
function sortTask(task){
	var res = [];
	// hour first
	var hour = isTask(task, 'hour');
	if (hour){
		res.push(hour);
	}
	// day sceond
	var day = isTask(task, 'day');
	if (day){
		res.push(day);
	}	
	// month third
	var month = isTask(task, 'month');
	if (month){
		res.push(month);
	}
	// logger.info('sortTask:', res);
	return res;
}

// check re-calculation task
function getRecalcTask(deviceId, timestamp, db){
	// save oss
	saveOss('minute', deviceId, timestamp);
	// query in mysql
	db.callup.find({
		deviceId: deviceId,
		callUpTime: timestamp
	}, function(err, recalcObj){
		if (recalcObj){
			// make task run in turn
			var recalcArray = sortTask(recalcObj);
			async.eachSeries(recalcArray, function(recalc, cb){
				var type = recalc['callUpType'];
				logger.info('exec recalc task:', deviceId, type, formatDate(timestamp));
				if (type == 'hour'){
					reCalcHour(deviceId, timestamp, function(err){
						if (!err){
							// delete hour recalc task
							deleteTask(deviceId, 'hour', timestamp, db, function(){});
							// save oss
							saveOss('hour', deviceId, timestamp);
						}
						async.setImmediate(function(){
							cb(null);
						});
					});
				}
				if (type == 'day'){
					reCalcDay(deviceId, timestamp, function(err){
						if (!err){
							// delete day recalc task
							deleteTask(deviceId, 'day', timestamp, db, function(){});
							// save oss
							saveOss('day', deviceId, timestamp);
						}
						async.setImmediate(function(){
							cb(null);
						});
					});
				}
				if (type == 'month'){
					reCalcMonth(deviceId, timestamp, function(err){
						if (!err){
							// delete month recalc task
							deleteTask(deviceId, 'month', timestamp, db, function(){});
							// save oss
							saveOss('month', deviceId, timestamp);
						}
						async.setImmediate(function(){
							cb(null);
						});
					});
				}
			});
		}
	});
}

// sync day table from oss to redis
function syncDayTable(deviceId, time, callback){
	var dayData = RTDataManager.dayData;
	// create day table in redis
	// dayData.create(deviceId, time, function(){
		// get hour data from oss or redis
		dayData.get(deviceId, time, function(err, data){
			if (data !== null) {
				var hourDataArray = data.hour;
				var toSetArray = [];
				// get xx-xx-xx 00:00 time
				var dayTime = moment(moment(time).format('YYMMDD'), 'YYMMDD').valueOf();
				// make hour data object
				for (var key in hourDataArray){
					toSetArray.push({
						time: dayTime + key*60*60*1000,
						value: hourDataArray[key].aqi
					});
				}
				// create day table in redis
				dayData.create(deviceId, time, function(){
					// set hour data in day table in turn
					var hourData = RTDataManager.hourData;
					async.eachSeries(toSetArray, function(hourObj, cb){
						hourData.set(deviceId, hourObj['time'], hourObj['value'], cb);
					}, function(err){
						if (err){
							logger.warn('sync day table error:', err);
							callback(err);
						}
						else{
							logger.info('sync day table:', deviceId, formatDate(time));
							callback(null);
						}
					});
				});
			}
			else{
				dayData.create(deviceId, time, function(){
					callback(null);
				});
			}
		});
	// });
}

// re-calculate hour average
function reCalcHour(deviceId, time, callback){
	var hourData = RTDataManager.hourData;
	var dayData = RTDataManager.dayData;
	// get mintue data
	hourData.get(deviceId, time, function(err, data){
		var avg = 0;
		if (data !== null) {
			// calculating average
			var minuteData = data.minute;
			var count = 0, sum = 0;
			for (var minute in minuteData){
				if (minute < 60 && minuteData[minute].aqi !== 0){
					sum += parseInt( minuteData[minute].aqi );
					count++;
				}
			}
			if (count == 0) {
				avg = 0;
				logger.info('no minute data to recalc:', deviceId, formatDate(time));
			}
			else{
				avg = Math.floor(sum / count);
			}
			// sync day table from oss to redis
			syncDayTable(deviceId, time, function(err){
				if (err){
					callback(err);
				}
				else{
					// set recalc data in day table
					hourData.set(deviceId, time, avg, function(err){
						logger.info('set recalc hour avg:', deviceId, formatDate(time), avg);
						callback(err);
					});	
				}
			});
		}
		else{
			logger.info('no hour table:', deviceId, formatDate(time));
			callback(true);
		}
	});
}

// re-calculate day average
function reCalcDay(deviceId, time, callback){
	var dayData = RTDataManager.dayData;
	var yearData = RTDataManager.yearData;
	// get hour data
	dayData.get(deviceId, time, function(err, data){
		var avg = 0;
		var dataObj = {
			aqi: avg,
			disturb: [[], [], [], [], []],
			rank: [200, 200]
		};
		if (data !== null) {
			// calculating average
			var hourData = data.hour;
			var count = 0, sum = 0;
			for (var hour in hourData){
				if (hour < 24 && hourData[hour].aqi !== 0){
					sum += parseInt( hourData[hour].aqi );
					count++;
				}
			}
			if (count == 0) {
				avg = 0;
				logger.info('no hour data to recalc:', deviceId, formatDate(time));
			}
			else{
				avg = Math.floor(sum / count);
				dataObj.aqi = avg;
			}
			// set recalc data in year table
			dayData.set(deviceId, time, dataObj, function(err){
				logger.info('set recalc day avg:', deviceId, formatDate(time), dataObj);
				callback(err);
			});
		}
		else{
			logger.info('no day table:', deviceId, formatDate(time));
			callback(true);
		}
	});
}

// re-calculate month average
function reCalcMonth(deviceId, time, callback){
	var yearData = RTDataManager.yearData;
	var monthData = RTDataManager.monthData;
	// get day data
	yearData.get(deviceId, time, function(err, data){
		var avg = 0;
		if (data !== null) {
			// calculating average
			var dayData = data.month[moment(parseInt(time)).month()].day;
			var count = 0, sum = 0;
			for (var day in dayData){
				if (day < 31 && dayData[day].aqi !== 0){
					sum += parseInt(dayData[day].aqi);
					count++;
				}
			}
			if (count == 0) {
				avg = 0;
				logger.info('no day data to recalc:', deviceId, formatDate(time));
			}
			else{
				avg = Math.floor(sum / count);
			}
			// set recalc data in year table
			monthData.set(deviceId, time, avg, function(err){
				logger.info('set recalc month avg:', deviceId, formatDate(time), avg);
				callback(err);	
			});
		}
		else{
			logger.info('no year table:', deviceId, formatDate(time));
			callback(true);
		}
	});
}

// update data in oss
function saveOss(type, deviceId, time){
	yearData = RTDataManager.yearData;
	dayData = RTDataManager.dayData;
	hourData = RTDataManager.hourData;
	if (type == 'minute'){
		// save hour table in oss
		hourData.save(deviceId, time, function(err){
			if (err) {
				logger.warn('save oss error:', type, deviceId, time);
			}
			else{
				logger.info('update hour data in oss:', deviceId, formatDate(time));
			}
		});
	}
	else if (type == 'hour'){
		// save day table in oss
		dayData.save(deviceId, time, function(err){
			if (err) {
				logger.warn('save oss error:', type, deviceId, time);
			}
			else{
				logger.info('update day data in oss:', deviceId, formatDate(time));
			}
		});
	}
	else if (type == 'day' || type == 'month') {
		// save year table in oss
		yearData.save(deviceId, time, function(err){
			if (err) {
				logger.warn('save oss error:', type, deviceId, time);
			}
			else{
				logger.info('update year data in oss:', deviceId, formatDate(time));
			}
		});
	}
}

module.exports = {
	run: main
};
