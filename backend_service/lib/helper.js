var resources = require('./resources');
var logger = resources.logger;

function JSONCallback(err, reply, callback) {
	try {
		callback(err, JSON.parse(reply));
	} catch (_error) {
		callback(_error, null);
		logger.warn('error when parse JSON: ' + reply, _error);
	}
}

module.exports = {
	JSONCallback: JSONCallback
};
