// manage data in redis save as bitmaps
// 1. every minutes data within a day. And every hour data.
// 2. every day data within a year. And every month data.
// format:
// version(1byte)DeviceId(15bytes)timestamp(4bytes)dataType(1byte)data(n bytes)
// day data: 24*60 minutes data, 24 hour data
// year data: 366 day data, 12 month data
var moment = require('moment');
var resources = require('./resources');
var client = resources.getRedis();
var logger = resources.logger;
var ossData = require('./ossData');
var dataStruct = require('./dataStruct');
var YearStruct = dataStruct.year[0];
var DayStruct = dataStruct.day[0];
var HourStruct = dataStruct.hour[0];
var StructDecode = dataStruct.decode;

var debug = require('debug')('RTData')

var HEADER_DATA_TYPE_DAY = 1;
var HEADER_DATA_TYPE_YEAR = 2;
var HEADER_DATA_TYPE_HOUR = 3;
var HEADER_VERSION = 1;
var DEVICE_ID_LEN = 15;
var DATA_LEN = 4;
var RANK_DATA_LEN = 4;
var ITEMS_IN_GROUP = 3;
var HEADER_LEN = 24
var MINUTE_OFFSET = 24;
var HOUR_OFFSET = MINUTE_OFFSET + DATA_LEN*60*24;
var HOUR_DATA_END = HOUR_OFFSET + DATA_LEN*24;
var DAY_OFFSET = 24;
var MONTH_OFFSET = DAY_OFFSET + DATA_LEN*366;
var MONTH_DATA_END = MONTH_OFFSET + DATA_LEN*12;
var RANK_OFFSET = MONTH_DATA_END;
var RANK_OFFSET_END = RANK_OFFSET + RANK_DATA_LEN*366;
// current data buffer len
var LATEST_DATA_LEN = 20;
// AQI rank
var AQI_RANK = 'AQI_RANK';
var AQI_RANK_DAY = 'AQI_RANK_DAY';
// key hash field define
var DEVICE_IP = 'ip';
var DEVICE_CITY = 'city';
// reference aqi
var REF_AQI = 'REF_AQI';
// tcp connection record
var CONN_IP_COUNT = 'CONNECTION_IP_COUNT';
var DEVICE_ONLINE_STATUS = 'DEVICE_ONLINE_STATUS';
var DEVICE_ONLINE_TIME = 'DEVICE_ONLINE_TIME';
var DEVICE_USERS_MAP = 'DEVICE_USERS_MAP';
var USER_DEVICES_MAP = 'USER_DEVICES_MAP';
var APIKEY_USER_MAP = 'APIKEY_USER_MAP';
var USER_FIELD_APIKEY = 'apiKey';
// 

debug('real debug!');

function setData(key, offset, bytes, callback) {
	client.setrange(key, offset, bytes, callback);
}

function getDataRange(key, from, to, callback) {
	client.getrange(key, from, to, callback);
}

function getData(key, callback) {
	client.get(key, function(err, reply) {
		setImmediate(callback, err, reply);
	});
}

function getHourTimestamp(timestamp) {
	return moment(timestamp).format('YYMMDDHH');
}

function getDayTimestamp(timestamp) {
	return moment(timestamp).format('YYMMDD');
}

function getYearTimestamp(timestamp) {
	return moment(timestamp).format('YY');
}

function mkHourDataKey(deviceId, timestamp) {
	// console.log('hour_' + getHourTimestamp(timestamp) + '_' + deviceId);
	return 'hour_' + getHourTimestamp(timestamp) + '_' + deviceId;
}

function mkDayDataKey(deviceId, timestamp) {
	return 'day_' + getDayTimestamp(timestamp) + '_' + deviceId;
}

function mkYearDataKey(deviceId, timestamp) {
	return 'year_' + getYearTimestamp(timestamp) + '_' + deviceId;
}

function mkCurrentDataKey(deviceId) {
	return 'current_' + deviceId;
}

function mkDisturbKey(deviceId, timestamp) {
	return 'year_disturb_' + getYearTimestamp(timestamp) + '_' + deviceId;
}

function mkDeviceEventKey(deviceId) {
	return 'device_event_' + deviceId;
}

function getMinuteOffset(timestamp) {
	var now = new Date(timestamp);
	return (now.getHours()*60 + now.getMinutes())*DATA_LEN + MINUTE_OFFSET;
}

function getHourOffset(timestamp) {
	var now = new Date(timestamp);
	return now.getHours()*DATA_LEN + HOUR_OFFSET;
}

function getDayOffset(timestamp) {
	return moment(timestamp).dayOfYear()*DATA_LEN + DAY_OFFSET;
}

function getRankOffset(timestamp) {
	return moment(timestamp).dayOfYear()*RANK_DATA_LEN + RANK_OFFSET;
}

function getMonthOffset(timestamp) {
	return moment(timestamp).month()*DATA_LEN + MONTH_OFFSET;
}

// =======================set binary data block======================
function getSetDataFunc(keyFunc, deviceId, timestamp) {
	var key = keyFunc(deviceId, timestamp);
	return setData.bind(undefined, key);
}
// =======================set binary data block end==================
function setDataToKeyBin(keyFunc, offsetFunc, deviceId, timestamp, dataBin, callback) {
	var key = keyFunc(deviceId, timestamp);
	var offset = offsetFunc(timestamp);
	debug('set bin data ', dataBin, ' to ', offset, ', ', key);
	setData(key, offset, dataBin, callback);
}

function setMinutesDataBin(deviceId, timestamp, dataBin, callback) {
	setDataToKeyBin(mkDayDataKey, getMinuteOffset, deviceId, timestamp, dataBin, callback);
}

function setHoursDataBin(deviceId, timestamp, dataBin, callback) {
	setDataToKeyBin(mkDayDataKey, getHourOffset, deviceId, timestamp, dataBin, callback);
}

function setDaysDataBin(deviceId, timestamp, dataBin, callback) {
	setDataToKeyBin(mkYearDataKey, getDayOffset, deviceId, timestamp, dataBin, callback);
}

function setRankDataBin(deviceId, timestamp, ranks, callback) {
	setDataToKeyBin(mkYearDataKey, getRankOffset, deviceId, timestamp, dataBin, callback);
}

function setMonthsDataBin(deviceId, timestamp, dataBin, callback) {
	setDataToKeyBin(mkYearDataKey, getMonthOffset, deviceId, timestamp, dataBin, callback);
}

function mkDataBin(dataList) {
	var data, i, len;
	var dataBin = new Buffer(dataList.length * DATA_LEN);
	for (i = 0, len = dataList.length; i < len; i++) {
		data = dataList[i];
		// A
		dataBin.writeUInt16BE(data[0], i*DATA_LEN);
		// T
		dataBin.writeUInt8(data[1], i*DATA_LEN+2);
		// H
		dataBin.writeUInt8(data[2], i*DATA_LEN+3);
	};
	return dataBin;
}

function mkRankBin(dataList) {
	var data, i, len;
	var dataBin = new Buffer(dataList.length * RANK_DATA_LEN);
	for (i = 0, len = dataList.length; i < len; i++) {
		data = dataList[i];
		// A
		dataBin.writeUInt16BE(data[0], i*DATA_LEN);
		// T
		dataBin.writeUInt16BE(data[1], i*DATA_LEN+2);
		// H
	};
	return dataBin;
}

function parseData(dataBin) {
	var data, i, len;
	var result = [];
	for (i = 0, len = dataBin.length/DATA_LEN; i < len; i++) {
		result.push([dataBin.readUInt16BE(i*DATA_LEN), dataBin.readUInt8(i*DATA_LEN+2), dataBin.readUInt8(i*DATA_LEN+3)]);
	};
	return result;
}

function parseRankData(dataBin) {
	var data, i, len;
	var result = [];
	for (i = 0, len = dataBin.length/RANK_DATA_LEN; i < len; i++) {
		result.push([dataBin.readUInt16BE(i*RANK_DATA_LEN), dataBin.readUInt16BE(i*RANK_DATA_LEN+2)]);
	};
	return result;
}

// data = aqi
function setMinuteData(deviceId, timestamp, data, callback) {
	logger.debug('rtdata minute set ', deviceId, timestamp, data);
	var setFunc = getSetDataFunc(mkHourDataKey, deviceId, timestamp);
	var time = moment(timestamp);
	HourStruct.minute[time.minute()].val.set(setFunc, data, callback);
}

function setCurrentMinuteData(deviceId, data, callback) {
	setMinuteData(deviceId, Date.now(), data, callback);
	setAqiRank(deviceId, data, function(err, reply) {
		if(err) {
			logger.warn('setAqiRank err: ', err, deviceId, data);
		}
	});
}

function setHourData(deviceId, timestamp, data, callback) {
	var setFunc = getSetDataFunc(mkDayDataKey, deviceId, timestamp);
	var time = moment(timestamp);
	DayStruct.hour[time.hour()].val.set(setFunc, data, callback);
}

/*
	@data = {aqi, disturb, rank}
*/
function setDayData(deviceId, timestamp, data, callback) {
	var setFunc = getSetDataFunc(mkYearDataKey, deviceId, timestamp);
	var time = moment(timestamp);
	YearStruct.month[time.month()].day[time.date()-1].val.set(setFunc, data, callback);
}

function setMonthData(deviceId, timestamp, data, callback) {
	var setFunc = getSetDataFunc(mkYearDataKey, deviceId, timestamp);
	var time = moment(timestamp);
	YearStruct.month[time.month()].val.set(setFunc, data, callback);
}

// def = {data1: [from, to], data2: [from, to]}
// return :{
	// 	timestamp: 0,
	// 	version: 0,
	// 	deviceId: '',
	// 	dataType: 0,
	// 	data1: [],
	// 	data2: []
	// }
function parseBlobData(dataBin, def) {
	if(dataBin == null || dataBin.length<HEADER_LEN) {
		return null;
	}
	var res = {
		timestamp: 0,
		version: 0,
		deviceId: '',
		dataType: 0
	};

	var readIndex = 0;
	// version
	debug('parse Blob: ', dataBin);
	res.version = dataBin.readUInt8(readIndex++);
	// device id
	var deviceIdBuf = new Buffer(DEVICE_ID_LEN);
	dataBin.copy(deviceIdBuf, 0, readIndex, readIndex+DEVICE_ID_LEN);
	debug('deviceId:', deviceIdBuf);
	res.deviceId = deviceIdBuf.toString().replace(/\0/g, '');
	readIndex += DEVICE_ID_LEN;
	// localtime timestamp, seconds -> ms
	res.timestamp = dataBin.readUInt32BE(readIndex)*1000;
	readIndex += 4;
	// data type, 0: day; 1: year
	res.dataType = dataBin.readUInt8(readIndex++);

	// parse blob data as def
	for (dataItem in def) {
		rangeDef = def[dataItem];
		var parsefunc = rangeDef[2] && typeof rangeDef[2] === 'function'?rangeDef[2]:parseData;
		res[dataItem] = dataBin.length>rangeDef[0] ? parsefunc(dataBin.slice(rangeDef[0], rangeDef[1])) : null;
	};
	return res;
}

function parseDayData(dataBin) {
	return (Buffer.isBuffer(dataBin) && dataBin.length>=dataStruct.day[1]) ? StructDecode(DayStruct, dataBin) : null;
}

function parseYearData(dataBin) {
	return (Buffer.isBuffer(dataBin) && dataBin.length>=dataStruct.year[1]) ? StructDecode(YearStruct, dataBin) : null;
}

function parseHourData(dataBin) {
	// debug('parseHourData:', dataStruct.day[1], dataBin.length);
	return (Buffer.isBuffer(dataBin) && dataBin.length>=dataStruct.hour[1]) ? StructDecode(HourStruct, dataBin) : null;
}

function parseDisturbData(dataBin) {
	if(dataBin == null) return null;
	var res = [];
	for(var i=0;i<366;i++) {
		var byteIndex = Math.floor(i/8);
		if (dataBin.length <= byteIndex) break;
		var bitIndex = i%8;
		var bitValue = (dataBin[byteIndex] & (0x80>>bitIndex)) != 0;
		res.push(bitValue);
	}
	return res;
}

function getDataFromKey(keyFunc, ParseFunc, deviceId, timestamp, callback) {
	var key = keyFunc(deviceId, timestamp);
	// console.log(key);
	debug('get data from key: ', key);
	getData(new Buffer(key), function(err, replyBin) {
		if (err) {
			callback(err, null);
		} else {
			debug('get data: ', replyBin);
			callback(null, ParseFunc(replyBin))
		}
	});
}

function getHisDataFromKey(keyFunc, ParseFunc, deviceId, timestamp, callback) {
	var key = keyFunc(deviceId, timestamp);
	debug('get oss data from key: ', key);
	ossData.get(key, function(err, oData) {
		if (err) {
			callback(err, null);
		} else {
			debug('get oss data: ', oData);
			callback(null, ParseFunc(oData['Body']))
		}
	});
}

function reloadDayData(deviceId, timestamp, callback) {
	getHisDataFromKey(mkDayDataKey, function(data){
		return data
	}, deviceId, timestamp, function(err, oData) {
		if(err) callback(err);
		else {
			setDataToKeyBin(mkDayDataKey, constRet(0), deviceId, timestamp, oData, callback);
		}
	});
}


function processDataFromKey(keyFunc, processFunc, deviceId, timestamp, callback) {
	var key = keyFunc(deviceId, timestamp);
	debug('get data from key: ', key);
	getData(new Buffer(key), function(err, replyBin) {
		if (err) {
			callback(err, null);
		} else {
			debug('get data: ', replyBin);
			processFunc(key, replyBin, callback);
		}
	});
}

function saveData2oss(keyFunc, deviceId, timestamp, callback) {
	processDataFromKey(keyFunc, ossData.put, deviceId, timestamp, callback);
}

function getHourData(deviceId, timestamp, callback) {
	debug('getHourData');
	getDataFromKey(mkHourDataKey, parseHourData, deviceId, timestamp, function(err, hour){
		if (hour) {
			callback(err, hour);
		}
		else {
			console.log('hour data from oss');
			getHisDataFromKey(mkHourDataKey, parseHourData, deviceId, timestamp, callback);
		}
	});
}

function getDayData(deviceId, timestamp, callback) {
	getDataFromKey(mkDayDataKey, parseDayData, deviceId, timestamp, function(err, day){
		if (day) {
			callback(err, day);
		}
		else {
			console.log('day data from oss');
			getHisDataFromKey(mkDayDataKey, parseDayData, deviceId, timestamp, callback);
		}
	});
}

function getYearData(deviceId, timestamp, callback) {
	getDataFromKey(mkYearDataKey, parseYearData, deviceId, timestamp, callback);
}

function setRank(key, score, member, callback) {
	client.zadd(key, score, member, callback);
}

function setAqiRank(deviceId, aqi, callback) {
	// 
	setRank(AQI_RANK, aqi, deviceId, callback);
	// get city then set city rank
	getDeviceCity(deviceId, function(err, city) {
		if(err) {
			logger.warn('get city of ' + deviceId + ' err: ' + err);
			callback(err);
		} else {
			setAqiRankInCity(city, deviceId, aqi);
		}
	});
}

function setAqiRankInCity(city, deviceId, aqi, callback) {
	setRank(AQI_RANK+city, aqi, deviceId, callback);
}

function setDayAqiRank(deviceId, aqi, callback) {
	// 
	setRank(AQI_RANK_DAY, aqi, deviceId, callback);
	// get city then set city rank
	getDeviceCity(deviceId, function(err, city) {
		if(err) {
			logger.warn('get city of ' + deviceId + ' err: ' + err);
			callback(err);
		} else {
			setDayAqiRankInCity(city, deviceId, aqi);
		}
	});
}

function setDayAqiRankInCity(city, deviceId, aqi, callback) {
	setRank(AQI_RANK_DAY+city, aqi, deviceId, callback);
}

// save day rank to day data table
// this function should be called after all the devices aqi been set
function saveDayAqiRank(deviceId, timestamp, callback) {
	getDayAqiRank(deviceId, function(err, aqiList) {
		setRankDataBin(deviceId, timestamp, mkRankBin([aqiList]), callback);
	});
}

function getKeyRankOfMember(key, member, callback) {
	client.zcard(key, function(err, replyCard) {
		var rCard = parseInt(replyCard);
		// replyCard could be zero
		if (err) {
			callback(err);
		} else if(rCard == 0) {
			callback(null, null);
		} else {
			client.zrank(key, member, function(err, replyRank) {
				var rRank = parseInt(replyRank);
				// replyRank could be null
				var res = null
				if (rRank != null) {
					res = (rCard - rRank)/(rCard + 1);
					// logger.debug('getKeyRankOfMember', member, rCard, rRank, res);
				}
				callback(err, res);
			});
		}
	});
}

// res: 
// 	null: device is not found
// [rank, cityRank] : 0~1: percent of rank
function getAqiRank(deviceId, callback) {
	debug('getKeyAqiRank of ', deviceId);
	getKeyAqiRank(AQI_RANK, deviceId, callback);
}

function getKeyAqiRank(key, deviceId, callback) {
	debug('getKeyAqiRank:', key, deviceId);
	getKeyRankOfMember(key, deviceId, function(err, aqi) {
		debug('getKeyRankOfMember err, result:', err, aqi);
		debug('getDeviceCity of ', deviceId);
		getDeviceCity(deviceId, function(err2, city) {
			debug('getDeviceCity err2, city:', err2, city)
			if(err2) {
				logger.warn('get city of ' + deviceId + ' err: ' + err2);
				callback(err2);
			} else {
				getKeyRankOfMember(key+city, deviceId, function(err3, cityAqi) {
					callback(err3, [aqi, cityAqi]);
				});
			}
		});
	});
}


function getDayAqiRank(deviceId, callback) {
	getKeyAqiRank(AQI_RANK_DAY, deviceId, callback);
}
// data = [A, T, H]
function setCurrentData(deviceId, data, timestamp, callback) {
	// set current value, push to list
	var val = [timestamp, data];
	var key = mkCurrentDataKey(deviceId);
	client.rpush(key, JSON.stringify(val), callback);
	client.ltrim(key, -LATEST_DATA_LEN, -1);
	// setAqiRank(deviceId, data[0]);
}

function getCurrentData(deviceId, callback) {
	// get from list
	client.lrange(mkCurrentDataKey(deviceId), -1, -1, function(e, r) {
		JSONCallback(e, r, callback);
	});
}

/*
dataList={
	city1: aqi
	city2: aqi2
	updateTime: timestamp
}
*/
function setRefAqiData(dataList, callback) {
	// todo:
	// logger.info('hmset', 'setRefAqiData', 'key', REF_AQI, 'data', dataList);
	client.hmset(REF_AQI, dataList, callback);
}

function getRefAqiData(city, callback) {
	client.hget(REF_AQI, city, function(err, res){
		var iRes = parseInt(res);
		callback(err, isNaN(iRes) ? null: iRes);
	});
}

function setDisturbRecord(deviceId, timestamp, callback) {
	// todo: set do not disturb record of a day within a year.
	client.setbit(mkDisturbKey(deviceId, timestamp), moment(timestamp).dayOfYear(), 1, callback);
}

function getDisturbRecord(deviceId, timestamp, callback) {
	// todo: get do not disturb record of everyday within a year.
	getDataFromKey(mkDisturbKey, parseDisturbData, deviceId, timestamp, callback);
}

function mkHeader(deviceId, timestamp, type) {
	var header = new Buffer(HEADER_LEN);
	header.fill(0);
	var index = 0;
	header.writeUInt8(HEADER_VERSION, index++);
	header.write(deviceId, index, DEVICE_ID_LEN);
	index += DEVICE_ID_LEN;
	header.writeUInt32BE(timestamp, index);
	index += 4;
	header.writeUInt8(type, index++);
	return header;
}

function constRet(val) {
	return function() { return val; };
}
// timestamp is in ms.
function createYearTable(deviceId, timestamp, callback) {
	var timestampSecond = Math.floor(timestamp/1000);
	setDataToKeyBin(mkYearDataKey, constRet(dataStruct.year[1]), deviceId, timestamp, mkHeader(deviceId, timestampSecond, HEADER_DATA_TYPE_YEAR), callback);
}
// timestamp is in ms.
function createDayTable(deviceId, timestamp, callback) {
	var timestampSecond = Math.floor(timestamp/1000);
	setDataToKeyBin(mkDayDataKey, constRet(dataStruct.day[1]), deviceId, timestamp, mkHeader(deviceId, timestampSecond, HEADER_DATA_TYPE_DAY), callback);
}

function createHourTable(deviceId, timestamp, callback) {
	var timestampSecond = Math.floor(timestamp/1000);
	setDataToKeyBin(mkHourDataKey, constRet(dataStruct.hour[1]), deviceId, timestamp, mkHeader(deviceId, timestampSecond, HEADER_DATA_TYPE_HOUR), callback);
}


function addDeviceEvent(deviceId, event, callback) {
	var val = [Date.now(), event];
	client.rpush(mkDeviceEventKey(deviceId), JSON.stringify(val), callback);
}

function getOnlineList(callback) {
	client.hkeys(DEVICE_ONLINE_STATUS, callback);
}

function getDeviceEvent(deviceId, callback) {
	client.lrange(mkDeviceEventKey(deviceId), 0, -1, callback);
}

function clearDeviceEvent(deviceId, callback) {
	client.del(mkDeviceEventKey(deviceId), callback);
}

function clearDayData(deviceId, timestamp, callback) {
	client.del(mkDayDataKey(deviceId, timestamp), callback);
}

function clearHourData(deviceId, timestamp, callback) {
	client.del(mkHourDataKey(deviceId, timestamp), callback);
}

function clearRank() {
	// get all keys
	client.KEYS(AQI_RANK_DAY + '*', function(err, keys){
		// console.log(keys);
		if (keys) {
			client.DEL(keys, function(err2){
				if (err2) {
					console.log(err2);
				}
				else{
					console.log('DEL ' + keys);
				}
			});
		}
	});
}

function mkDeviceDataKey (deviceId, timestamp) {
	return 'device_data_' + deviceId;
}

function mkUserDataKey (user) {
	return 'user_data_' + user;
}

function setDeviceOnlineAndData(deviceId, dataObj, callback) {
	setDeviceData(deviceId, dataObj, callback);
	setDeviceOnline(deviceId);
}

function setDeviceOnline(deviceId, callback) {
	debug('setDeviceOnline');
	logger.debug('set ' + deviceId + ' online.');
	client.hset(DEVICE_ONLINE_STATUS, deviceId, 'online', callback);
	// set online time
	client.hset(DEVICE_ONLINE_TIME, deviceId, moment().valueOf(), callback);
	debug('setDeviceOnline end.');
}

// get devices online time list
// {device_id: online_time, ...}
function getDevicesList(callback) {
	client.hkeys(DEVICE_ONLINE_TIME, callback);
}

function setDeviceOffline(deviceId, callback) {
	logger.debug('set ' + deviceId + ' offline.');
	client.hset(DEVICE_ONLINE_STATUS, deviceId, 'offline', callback);
}

// get device online status
function getDeviceStatus(deviceId, callback) {
	debug('getDeviceStatus ', deviceId);
	client.hget(DEVICE_ONLINE_STATUS, deviceId, callback);
}

function HMset(key, dataObj, callback) {
	// logger.info('hmset', 'HMset', 'key', key, 'data', dataObj);
	if(typeof callback === "undefined" || callback===null || typeof callback !== 'function') {
		client.hmset(key, dataObj);
	} else {
		client.hmset(key, dataObj, callback);		
	}
}

function getDeviceCity(deviceId, callback) {
	debug('getDeviceCity ', deviceId);
	getDeviceDataItem(deviceId, DEVICE_CITY, callback);
}

function setDeviceData(deviceId, dataObj, callback) {
	// 
	debug('setDeviceData');
	// logger.info('hmset', 'setDeviceData', 'key', mkDeviceDataKey(deviceId), 'data', dataObj);
	HMset(mkDeviceDataKey(deviceId), dataObj, callback);
	debug('setDeviceData end.');
}

// get all properties of device
function getDeviceData(deviceId, callback) {
	debug('getDeviceData', deviceId);
	client.hgetall(mkDeviceDataKey(deviceId), callback);
}

function setDeviceDataItem(deviceId, field, value, callback) {
	client.hset(mkDeviceDataKey(deviceId), field, value, callback);
}

function getDeviceDataItem(deviceId, field, callback) {
	client.hget(mkDeviceDataKey(deviceId), field, callback);
}

function setUserData(user, dataObj, callback) {
	// logger.info('hmset', 'setUserData', 'key', mkUserDataKey(user), 'data', dataObj);
	HMset(mkUserDataKey(user), dataObj, callback);
}
function getUserData(user, callback){
	client.hgetall(mkUserDataKey(user), callback);
}
function setUserDataItem(user, field, value, callback) {
	client.hset(mkUserDataKey(user), field, value, callback);
}
function getUserDataItem(user, field, callback) {
	client.hget(mkUserDataKey(user), field, callback);
}

// clear api key
function clearApiKey(user) {
	// get last apikey
	getUserDataItem(user, USER_FIELD_APIKEY, function(err, lastKey){
		if (lastKey !== null) {
			// clear last apikey 
			client.hdel(APIKEY_USER_MAP, lastKey, function(err2, ret){
				console.log('clear ' + lastKey);
			});
		}
	});
}

function setUserApiKey(user, apiKey, callback) {
	// clear last api key
	clearApiKey(user);
	
	setUserDataItem(user, USER_FIELD_APIKEY, apiKey, callback);
	client.hset(APIKEY_USER_MAP, apiKey, user);
}


function getApiKeyUser(apiKey, callback) {
	client.hget(APIKEY_USER_MAP, apiKey, callback);
}

function mkConnectionKey(ip) {
	return 'connection_from_';
}
// record tcp connection data:
// ======ip connection count======
// key: CONNECTION_IP_COUNT
// value: hash table
// field: ip
// value: connection count
function setConnectionData(ip, connCount, callback) {
	client.hset(CONN_IP_COUNT, ip, connCount, callback);
}

function JSONCallback(err, reply, callback) {
	var e = err;
	var res = null;
	try {
		if(!err) {
			res = JSON.parse(reply);
		}
	} catch (_error) {
		e = err;
		logger.warn('error when parse JSON: ' + reply, _error);
	} finally {
		setImmediate(callback, e, res);
	}

}

function getDeviceUsers(deviceId, callback) {
	client.hget(DEVICE_USERS_MAP, deviceId, function(err, reply) {
		JSONCallback(err, reply, callback);
	});
}
function getUserDevices(user, callback) {
	client.hget(USER_DEVICES_MAP, user, function(err, reply) {
		JSONCallback(err, reply, callback);
	});
}

function setDeviceUsers(deviceId, userList, callback) {
	client.hset(DEVICE_USERS_MAP, deviceId, JSON.stringify(userList), callback);
}
function setUserDevices(user, deviceList, callback) {
	client.hset(USER_DEVICES_MAP, user, JSON.stringify(deviceList), callback);
}

function initRTDATA(callback) {
	client.del(DEVICE_ONLINE_STATUS, callback);
}

function getUsersList(callback) {
	client.hkeys(USER_DEVICES_MAP, callback);
}

module.exports = {
	test: {
		constRet: constRet,
		mkHeader: mkHeader
	},
	RTDataManager: {
		init: initRTDATA,
		currentData: {
			get: getCurrentData,	//(deviceId, callback)
			set: setCurrentData		// (deviceId, data, timestamp, callback)
		},
		yearData: {
			get: getYearData,			// (deviceId, timestamp, callback)
			create: createYearTable,
			save: saveData2oss.bind(undefined, mkYearDataKey),
			getHis: getHisDataFromKey.bind(undefined, mkYearDataKey, parseYearData)
		},
		monthData: {
			set: setMonthData 		// (deviceId, timestamp, dataList, callback)
		},
		dayData: {
			get: getDayData,			// (deviceId, timestamp, callback)
			set: setDayData, 			// (deviceId, timestamp, dataList, callback)
			freezeRank: saveDayAqiRank,	// (deviceId, timestamp, callback)
			create: createDayTable,
			save: saveData2oss.bind(undefined, mkDayDataKey),
			reload: reloadDayData,
			getHis: getHisDataFromKey.bind(undefined, mkDayDataKey, parseDayData),
			setRankAqi: setDayAqiRank,
			getRank: getDayAqiRank,
			del: clearDayData,
			clearRank: clearRank
		},
		hourData: {
			create: createHourTable,
			get: getHourData,
			set: setHourData,  		// (deviceId, timestamp, dataList, callback)
			save: saveData2oss.bind(undefined, mkHourDataKey),
			getHis: getHisDataFromKey.bind(undefined, mkHourDataKey, parseHourData),
			del: clearHourData
		},
		minuteData: {
			set: setMinuteData, 	// (deviceId, timestamp, dataList, callback)
			setCurrent: setCurrentMinuteData
		},
		// disturb: {
		// 	set: setDisturbRecord,	// (deviceId, timestamp, callback)
		// 	get: getDisturbRecord 	// (deviceId, timestamp, callback)
		// },
		rank: {
			get: getAqiRank 				// (deviceId, callback)
		},
		refAqi: {
			set: setRefAqiData,			// (dataList, callback)
			get: getRefAqiData 			// (city, callback) {
		},
		deviceEvent: {
			add: addDeviceEvent,
			list: getOnlineList, 
			get: getDeviceEvent, 
			clear: clearDeviceEvent
		},
		deviceData: {
			list: getDevicesList, 
			online: setDeviceOnlineAndData,
			offline: setDeviceOffline,
			getStatus: getDeviceStatus, 
			setDataItem: setDeviceDataItem,
			getDataItem: getDeviceDataItem,
			setData: setDeviceData, 
			getData: getDeviceData, 
			setUsers: setDeviceUsers,
			getUsers: getDeviceUsers,
			getCity: getDeviceCity
		},
		userData: {
			setDevices: setUserDevices,
			getDevices: getUserDevices,
			setApiKey: setUserApiKey,
			setData: setUserData,
			getData: getUserData, 
			setDataItem: setUserDataItem,
			getDataItem: getUserDataItem,
			getList: getUsersList
		},
		apiKey: {
			getUser: getApiKeyUser, 
			clear: clearApiKey
		},
		connection: {
			set: setConnectionData
		}
	}
};
