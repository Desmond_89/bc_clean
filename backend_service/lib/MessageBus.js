// Generated by CoffeeScript 1.9.2
(function() {
  var EM, MessageBus, debug, listen, resources,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  EM = require('events').EventEmitter;

  resources = require('./resources');

  debug = require('debug')('MB');

  MessageBus = (function(superClass) {
    extend(MessageBus, superClass);

    function MessageBus(channel1) {
      var client;
      this.channel = channel1;
      client = resources.getRedis();
      client.on('subscribe', function(p, c) {
        return debug('subscribed: ', p, ', count: ', c);
      });
      client.on('message', (function(_this) {
        return function(channel, message) {
          debug(message, ' got from ', channel);
          return _this.emit('message', JSON.parse(message));
        };
      })(this));
      client.subscribe(this.channel);
      debug('subscribe on channel: ', this.channel);
    }

    return MessageBus;

  })(EM);

  listen = function(channel) {
    return new MessageBus(channel);
  };

  module.exports = {
    listen: listen
  };

}).call(this);
