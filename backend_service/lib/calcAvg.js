var RTDataManager = require('./RTDataManager').RTDataManager;
var MSG = require('./RTMessage');
var logger = require('./logging');

var async = require('async');
var moment = require('moment');

var later = require('later');
later.date.localTime();

PARALLEL_COUNT = 100;

// return last hour time
function getTime() {
    var hourString = moment().format('YYYYMMDDHH');
    // for test
    // var hourString = '2015070101';
    var lastHour = (moment(hourString, 'YYYYMMDDHH').valueOf() - 60*60*1000);
    var dayString = moment(parseInt(lastHour)).format('YYYYMMDD');
    var monthString = moment(parseInt(lastHour)).format('YYYYMM');
    var yearString = moment(parseInt(lastHour)).format('YYYY');
    return {
        hour: lastHour, 
        day: moment(dayString, 'YYYYMMDD').valueOf(),
        month: moment(monthString, 'YYYYMM').valueOf(),
        year: moment(yearString, 'YYYY').valueOf()
    };
}

// get device disturb
function getDisturb(deviceId, callback) {
    var deviceData = RTDataManager.deviceData;
    deviceData.getDataItem(deviceId, 'noDisturb', function(err, string){
        var disturbObj = JSON.parse(string);
        // console.log(disturb);
        var result = [];
        for (var key in disturbObj) {
            var disturb = disturbObj[key];
            if (disturb['switch'] == 'ON') {
                switch (disturb.repeat) {
                    case 'everyday': {
                        result.push([
                            parseInt(disturb.from.slice(0, 2)),
                            parseInt(disturb.from.slice(2)),
                            parseInt(disturb.to.slice(0, 2)),
                            parseInt(disturb.to.slice(2))
                        ]);
                    }
                    break;
                    case 'never': {
                        // today
                        var current = parseInt(moment().format('YYMMDD'));
                        if (current >= parseInt(disturb.from.slice(0, 6)) &&
                            current <= parseInt(disturb.to.slice(0, 6))) {
                            result.push([
                                parseInt(disturb.from.slice(6, 8)),
                                parseInt(disturb.from.slice(8, 10)),
                                parseInt(disturb.to.slice(6, 8)),
                                parseInt(disturb.to.slice(8, 10))
                            ]);
                        }
                    }
                    break;
                    default: {
                        var type = disturb.repeat.slice(6);
                        // console.log(type);
                        if (type == moment().format('dddd').toLowerCase()) {
                            result.push([
                                parseInt(disturb.from.slice(0, 2)),
                                parseInt(disturb.from.slice(2)),
                                parseInt(disturb.to.slice(0, 2)),
                                parseInt(disturb.to.slice(2))
                            ]);
                        }
                    }
                }   
            }
        }
        // check length
        for (var l = result.length; l < 5; l++) {
            result.push([]);
        }
        // console.log(result);
        callback(null, result);
    });
}
// set hourly data
function setHourData(devices, time, db, cb) {
    var hour = RTDataManager.hourData;
    var day = RTDataManager.dayData;
    // traverse devices
    var deviceIdArray = [];
    for (var key in devices){
        deviceIdArray.push(devices[key]);
    }
    async.eachLimit(deviceIdArray, PARALLEL_COUNT, function(deviceId, callback){
        // create next hour table
        hour.create(deviceId, time.hour + 1000*60*60*2, function(err){
            if (!err) {
                logger.info(
                    'create hour table:',
                    deviceId,
                    moment(parseInt(time.hour + 1000*60*60*2)).format('YYMMDDHH')
                );
            }
        });
        // get mintue data
        hour.get(deviceId, time.hour, function(err, hourData){
            var avg = 0;
            if (hourData !== null) {
                // calculating average
                var minuteData = hourData.minute;
                var count = 0, sum = 0;
                for (var minute in minuteData){
                    if (minute < 60 && minuteData[minute].aqi !== 0){
                        sum += parseInt( minuteData[minute].aqi );
                        count++;
                    }
                }
                if (count == 0) {
                    avg = 0;
                }
                else{
                    avg = Math.floor(sum / count);
                }

                // check minute data complete
                if (count == 0){
                    saveCallup(db, deviceId, 'minute', time.hour);
                    saveCallup(db, deviceId, 'hour', time.hour);
                }
                /*
                else if (count < 60){
                    saveCallup(db, deviceId, 'hour', time.hour);
                }
                */
            }
            else{
                saveCallup(db, deviceId, 'minute', time.hour);
                saveCallup(db, deviceId, 'hour', time.hour);
            }
            // set hour in day
            day.create(deviceId, time.day, function(){
                hour.set(deviceId, time.hour, avg, function(err){
                    if (!err) {
                        logger.info(
                            'set hour avg:',
                            deviceId,
                            moment(parseInt(time.hour)).format('YYMMDDHH'),
                            avg
                        );
                    }
                    callback(err);
                });
            });
        });
    }, function(err) {
        if (err){
            logger.info('error', err);
        }
        cb();   // next
    });
}

// set daily data
function setDayData(devices, time, db, cb) {
    // if (time.hour == time.day) {
        var day = RTDataManager.dayData;
        var year = RTDataManager.yearData;
        // get all devices hour data
        async.mapLimit(devices, PARALLEL_COUNT, function(deviceId, callback){
            // calculate avg
            day.get(deviceId, time.hour, function(err, dayData){
                var avg = 0;
                if (dayData !== null) {
                    // calculating average
                    var hourData = dayData.hour;
                    var count = 0, sum = 0;
                    for (var hour in hourData){
                        if (hour < 24 && hourData[hour].aqi !== 0){
                            sum += parseInt( hourData[hour].aqi );
                            count++;
                        }
                    }
                    if (count == 0) {
                        avg = 0;
                        saveCallup(db, deviceId, 'day', time.hour);
                    }
                    else{
                        avg = Math.floor(sum / count);
                    }
                }
                else{
                    saveCallup(db, deviceId, 'day', time.hour);
                }
				
                // get no disturb
                getDisturb(deviceId, function(err, disturb){
                    logger.debug('set rank data', deviceId, avg);
                    // set rank aqi
                    if (avg !== 0) {
                        day.setRankAqi(deviceId, avg, function(err){
                            callback(err, {
                                id: deviceId, 
                                avg: avg,
                                disturb: disturb
                            });
                        });
                    }
                    else {
                        callback(err, {
                            id: deviceId, 
                            avg: avg,
                            disturb: disturb
                        });
                    }
                });
            });
        }, function(err, dayDataArray){
            // logger.error('setDayData error', err);
            if (!err){
                async.eachLimit(dayDataArray, PARALLEL_COUNT, function(data, cb2){
                    // get rank
                    day.getRank(data.id, function(err, rank){
                        logger.debug('get rank ', data.id, data.avg, rank);
                        
                        var aRank = 200;
                        var cRank = 200;
                        if (rank[0]) {
                            aRank = Math.round(rank[0]*100);
                            cRank = Math.round(rank[1]*100);
                        }
                        
                        var dataObj = {
                            aqi: data.avg,
                            disturb: data.disturb,
                            rank: [aRank, cRank]
                        };

                        // set day in year
                        year.create(data.id, time.hour, function(err){
                            day.set(data.id, time.hour, dataObj, function(err){
                                if (!err) {
                                    logger.info(
                                        'after set day data:',
                                        data.id,
                                        moment(parseInt(time.hour)).format('YYMMDD'),
                                        dataObj
                                    );
                                }
                                cb2(err);
                            });
                        });
                    });
                }, function(){
                    day.clearRank();
                    cb();   // next
                });
            }
            else{
                logger.info('error:', err);
                cb();   //next
            }
        });
    // }
}

// set monthly data
function setMonthData(devices, time, db, cb) {
    // if (time.hour == time.month) {
        var year = RTDataManager.yearData;
        var month = RTDataManager.monthData;
        // devices
        async.eachLimit(devices, PARALLEL_COUNT, function(deviceId, callback){
            // calculate avg
            year.get(deviceId, time.hour, function(err, yearData){
                var avg = 0;
                if (yearData !== null) {
                    // calculating average
                    var dayData = yearData.month[moment(parseInt(time.hour)).month()].day;
                    var count = 0, sum = 0;
                    for (var day in dayData){
                        if (day < 31 && dayData[day].aqi !== 0){
                            sum += parseInt(dayData[day].aqi);
                            count++;
                        }
                    }
                    if (count == 0) {
                        avg = 0;
                        saveCallup(db, deviceId, 'month', time.hour);
                    }
                    else{
                        avg = Math.floor(sum / count);
                    }
                }
                else{
                    saveCallup(db, deviceId, 'month', time.hour);
                }
                // set month in year
                year.create(deviceId, time.hour, function(err){
                    month.set(deviceId, time.hour, avg, function(err){
                        if (!err) {
                            logger.info(
                                'set month avg:',
                                deviceId,
                                moment(parseInt(time.hour)).format('YYMM'),
                                avg
                            );
                        }
                        callback(err);
                    });
                });
            });
        }, function(err){    
            if (err){
                logger.info('error', err);
            }
            cb();   // next
        });
    // }
}

// create current and next hour tables when init
function createInitTable(){
    var hourTime = moment().valueOf();
    var timeArray = [hourTime, hourTime + 1000*60*60];	// current hour and next hour time
    async.eachLimit(timeArray, PARALLEL_COUNT, function(hour, callback){
        // get exist device list
        var device = RTDataManager.deviceData;
        device.list(function(err, list){
            if (list !== null){
                var deviceIdArray = [];
                for (var key in list){
                    deviceIdArray.push(list[key]);
                }
                // traverse devices
                async.each(deviceIdArray, function(deviceId, callback){
                    // create hour table
                    var hourData = RTDataManager.hourData;
                    hourData.create(deviceId, hour, function(err){
                        if (!err)
                            logger.info('create hour table:', deviceId, moment(parseInt(hour)).format('YYMMDDHH'));
                        callback(err);
                    });
                });
            }
        });
    }, function(err){
        if (err)
            logger.info('error:', err);
    });
}

// calculating average every hour
function calcAvg(db){
    // get last hour time
    var sTime = getTime();
    var device = RTDataManager.deviceData;
    device.list(function(err, list){
        if (list) {
            setHourData(list, sTime, db, function(){
                logger.info('setHourData finished!', sTime);
                setDayData(list, sTime, db, function(){
                    logger.info('setDayData finished!', sTime);
                    setMonthData(list, sTime, db, function(){
                        logger.info('setMonthData finished!', sTime);
                    });
                });
            });
        }
    });
}

// save call up device - time in 2 months
function saveCallup(db, deviceId, type, time){
	/*
	var realDevices = [
		'030083009174',
		'330083009174',
		'520053009174',
		'620083009174',
		'710043009174',
		'8300B100B074',
		'920083009174',
		'9300D100B074',
		'C300F2004174',
		'F200C2004174',
		'G10053009174',
		'G10083009174'
	];
	// check for test
	//for (var key in realDevices){
	//	if (deviceId == realDevices[key]){
	*/
			db.callup.findOrCreate({
				deviceId: deviceId, 
				callUpType: type,  
				callUpTime: time
			}, function(err, res){
				if (err) {
					logger.info('save call up error: ', err);
				}
				else {
					logger.info('save call up: ', deviceId, type, time);
				}
			});
	//	}
	//}
}

function calcAvgForMan(time){
    var device = RTDataManager.deviceData;
    device.list(function(err, list){
        if (list) {
            setHourData(list, time, function(){
                logger.info('setHourData finished!', time);
                setDayData(list, time, function(){
                    logger.info('setDayData finished!', time);
                    setMonthData(list, time, function(){
                        logger.info('setMonthData finished!', time);
                    });
                });
            });
        }
    });
}

// main run
function task(db) {
    createInitTable();
    // calcAvg();
    // setInterval(calcAvg, 60*60*1000);
    later.setInterval(function(){
		calcAvg(db);
	}, later.parse.cron('1 * * * *'));
}

function createTable(err, deviceId) {
    var hourTime = moment().valueOf();
    // create hour table
    var hourData = RTDataManager.hourData;
    hourData.create(deviceId, hourTime, function(err){
        if (!err)
            logger.info('create hour table:', deviceId, moment(parseInt(hourTime)).format('YYMMDDHH'));
        var next = (hourTime + 60*60*1000);
        hourData.create(deviceId, next, function(err){
            if (!err)
                logger.info('create hour table:', deviceId, moment(parseInt(next)).format('YYMMDDHH'));
        });
    });
}

function listenOnlineQueue(createTable) {
    MSG.deviceOnlineQueue.pop(function(err, deviceId) {
        setImmediate(createTable, err, deviceId);
        setImmediate(listenOnlineQueue, createTable);
    });
}

listenOnlineQueue(createTable);

// for test
// task();

module.exports = {
    run: task,
    calcAvg: calcAvg,
    calcAvgForMan: calcAvgForMan
};