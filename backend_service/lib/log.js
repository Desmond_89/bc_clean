var winston = require('winston');
var bunyan = require('bunyan');

// var logger = new (winston.Logger)({
//   transports: [
//     new (winston.transports.File)({
//       name: 'info-file',
//       filename: './log/bs-info.log',
//       level: 'info'
//     }),
//     new (winston.transports.DailyRotateFile)({
//       name: 'rotate-file',
//       filename: './log/bs-day.log',
//       level: 'debug'
//     }),
//     new (winston.transports.File)({
//       name: 'error-file',
//       filename: './log/bs-error.log',
//       level: 'error'
//     })
//   ]
// });

var logger = bunyan.createLogger({
  name: 'commServer',
  streams: [
    {
      level: 'info',
      path: './log/bs-info.log'            // log INFO and above to stdout
    },
    {
      type: 'rotating-file',
      path: './log/bs-day.log',
      period: '1d',   // daily rotation
      level: 'debug',
      count: 3        // keep 3 back copies
    },
    {
      level: 'error',
      path: './log/bs-error.log'  // log ERROR and above to a file
    }
  ]
});

module.exports = logger;