// todo: global config utility, which can be updated alive.
var CONFIG_FILE = './config/config.json';
var resources = require('./resources');
var logger = resources.logger;
var dbg = resources.debug('the_config');
var fs = require('fs');
var MB = require('./MessageBus');

var CHANNEL_DEF = require('./message_channel_def');
var client = resources.getRedis();

process._the_config = {};

function reloadConfig() {
	console.log('reloadConfig:');
	fs.readFile(CONFIG_FILE, function (err, data) {
		console.log('read config file: ', data, 'error: ', err);
		try{
			process._the_config = JSON.parse(data);
			console.log('config:', process._the_config);
		} catch (_error) {
			logger.error('config file parse error: ', _error);
		}
	});
}

function listenUpdate() {
	var listener = MB.listen(CHANNEL_DEF.CONFIG_UPDATE)
	listener.on('message', function onConfigUpdate() {
		reloadConfig();
	});
}

function update() {
	client.publish(CHANNEL_DEF.CONFIG_UPDATE, 'config_update');
}

function getConfig() {
	console.log('get config');
	return process._the_config;
}

reloadConfig();
listenUpdate();

module.exports = {
	config: process._the_config,
	get: getConfig,
	reload: update
};