// var winston = require('winston');
var bunyan = require('bunyan');

// var logger = new (winston.Logger)({
//   transports: [
//     new (winston.transports.File)({
//       name: 'info-file',
//       filename: './log/bs-info.log',
//       level: 'info'
//     }),
//     new (winston.transports.DailyRotateFile)({
//       name: 'rotate-file',
//       filename: './log/bs-day.log',
//       level: 'debug'
//     }),
//     new (winston.transports.File)({
//       name: 'error-file',
//       filename: './log/bs-error.log',
//       level: 'error'
//     })
//   ]
// });
function procName() {
  return process.title + '.' + process.pid;
}

var logger = bunyan.createLogger({
  name: procName(),
  streams: [
    {
      level: 'warning',
      path: '/var/log/' + procName() + '-info.log'            // log INFO and above to stdout
    },
    {
      type: 'rotating-file',
      path: '/var/log/' + procName() + '-day.log',
      period: '1d',   // daily rotation
      level: 'info',
      count: 3        // keep 3 back copies
    },
    {
      level: 'error',
      path: '/var/log/' + procName() + '-error.log'  // log ERROR and above to a file
    }
  ]
});

module.exports = logger;
