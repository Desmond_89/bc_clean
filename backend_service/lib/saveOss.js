var RTDataManager = require('./RTDataManager').RTDataManager;
var logger = require('./logging');

var async = require('async');
var moment = require('moment');

var later = require('later');
later.date.localTime();

PARALLEL_COUNT = 20;

function saveAll() {
    deviceData = RTDataManager.deviceData;
    yearData = RTDataManager.yearData;
    dayData = RTDataManager.dayData;
    hourData = RTDataManager.hourData;
    // get devices list
    deviceData.list(function(err, devicesList){
        if (devicesList) {
            // traverse devices list
            var list = [];
            for (var key in devicesList){
                list.push(devicesList[key]);
            }
            
            //list = [
            //    '030083009174',
            //    '330083009174',
            //    '520053009174',
            //    '620083009174',
            //    '920083009174',
            //    '9300D100B074',
            //    'C300F2004174',
            //    'F200C2004174',
            //    'G10083009174'
            //];
            
            // save oss
            var current = moment().valueOf();
            async.eachLimit(list, PARALLEL_COUNT, function(deviceId, callback){
                if (deviceId) {
                    // year
                    yearData.save(deviceId, current, function(err){
                        if (err) {
                            callback(err);
                        }
                        else {
                            // all days
                            var allDays = [];
                            var dayStart = moment('2015', 'YYYY').valueOf();
                            while (dayStart < current) {
                                allDays.push(dayStart);
                                dayStart += 24*60*60*1000;
                            }
                            // console.log(allDays.length);
                            
                            async.eachLimit(allDays, PARALLEL_COUNT, function(dayTime, c1){
                                dayData.save(deviceId, dayTime, function(err){
                                    if (!err) {
                                        // console.log('save day ' + moment(dayTime).format('YYMMDD'));
                                    }
                                    c1(null);
                                });
                            }, function(err){
                                // all hours
                                var allHours = [];
                                var hourStart = moment('2015', 'YYYY').valueOf();
                                while (hourStart < current) {
                                    allHours.push(hourStart);
                                    hourStart += 60*60*1000;
                                }
                                // console.log(allHours.length);
                                async.eachLimit(allHours, PARALLEL_COUNT, function(hourTime, c2){
                                    hourData.save(deviceId, hourTime, function(err){
                                        if (err) {
                                            // console.log('save hour ' + moment(hourTime).format('YYMMDDHH'));
                                        }
                                        c2(null);
                                    });
                                }, function(err){
                                    // console.log(deviceId + ' finished!');
                                    logger.info(deviceId, 'save OSS finished!')
                                    callback();
                                });
                            });
                        }
                    });
                }
            });
        }
    });
}

// 
function saveHourly() {
    deviceData = RTDataManager.deviceData;
    yearData = RTDataManager.yearData;
    dayData = RTDataManager.dayData;
    hourData = RTDataManager.hourData;
    // get devices list
    deviceData.list(function(err, devicesList){
        if (devicesList) {
            // traverse devices list
            var list = [];
            for (var key in devicesList){
                list.push(devicesList[key]);
            }
            // save oss
            async.eachLimit(list, PARALLEL_COUNT, function(deviceId, callback){
                if (deviceId) {
                    // sleep
                    setTimeout(function(){
                        // save hour data
                        var lastHour = (moment().valueOf() - 60 * 60 * 1000);
                        hourData.save(deviceId, lastHour, function(err){
                            if (err) {
                                // console.log('save OSS ' + deviceId + ' hour ' + moment(lastHour).format('YYMMDDHH') + ' error: ' + err);
                                callback(null);
                            }
                            else {
                                // save day data
                                if ( moment().hours() == 0) {  // save every day 0 clock
                                    dayData.save(deviceId, lastHour, function(err){
                                        if (err) {
                                            // console.log('save OSS ' + deviceId + ' day ' + moment().format('YYMMDD') + ' error: ' + err);
                                            callback(null);
                                        }
                                        else {
                                            if (moment().dayOfYear() == 1) {  // save when 1-1
                                                yearData.save(deviceId, lastHour, function(err){
                                                    if (err) {
                                                        callback(null);
                                                    }
                                                    else{
                                                        logger.info(deviceId, 'save OSS finished!');
                                                        callback(null);
                                                    }
                                                });
                                            }
                                            else {
                                                logger.info(deviceId, 'save OSS finished!');
                                                callback(null);
                                            }
                                        }
                                    });  
                                }
                                else {
                                    logger.info(deviceId, 'save OSS finished!');
                                    callback(null);
                                }
                            }
                        });
                    }, 10); 
                }
                
            });
        }
    });
}

// 
function saveOss() {
    // first run
    // saveHourly();
    // run on time
    later.setInterval(function(){
        saveHourly();
    }, later.parse.cron('20 * * * *'));
}

// for test
// saveHourly();

// init save all data in this year
// saveAll();

module.exports = {
    run: saveOss
};