var RTMessage = require('./RTMessage');

function send(to, subject, text) {
	var mail = {
		to: to,
		subject: subject,
		text: text
	};
	RTMessage.mailQueue.append(mail);
}

module.exports = send;