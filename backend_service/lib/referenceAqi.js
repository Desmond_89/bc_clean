var request = require('request');
var RTDataManager = require('./RTDataManager').RTDataManager;
var logger = require('./logging');

// config parameter
var token = '35wYo2vLB7pBdWHumKTF';
var querysUrl = 'http://www.pm25.in/api/querys.json';
var citiesAqiUrl = 'http://www.pm25.in/api/querys/aqi_ranking.json';

// make url
function addToken(url) {
    return url + '?token=' + token;
}

// get all city list
function getCities(callback) {
    var url = addToken(querysUrl);
    request(url, function (error, response, body){
        if (body !== null) {
            var cities;
            try {
                cities = JSON.parse(body);
            } catch (error) {
                callback('error data', null);
            }
            callback(null, cities);
        }
        else{
            callback('no data', null);
        }
    });
}

// save cities aqi
function saveRefAqi(callback){
    // get all cities aqi
    var url = addToken(citiesAqiUrl);
    request(url, function (error, response, body){
        if (body !== null) {
            // format data
            var aqi;
            try {
                aqi = JSON.parse(body);
            } catch (error) {
                callback('error reference data', null);
            }
            var citiesAqi = {};
            for(var i in aqi){
                // console.log(aqi[i].area);
                // console.log(Buffer(aqi[i].area));
                citiesAqi[aqi[i].area] = aqi[i].aqi;
            }
            // save
            // console.log(citiesAqi);
            if (Object.keys(citiesAqi).length > 0) {
                var refAqi = RTDataManager.refAqi;
                refAqi.set(citiesAqi, callback);
            }
            else {
                callback('no reference data', null);
            }
        }
        else{
            callback('no reference data', null);
        }
    });
}

// save task
function saveTask(){
    setInterval(function(){
        saveRefAqi(function(err, aqi){
            if (err) {
                logger.error(err); 
            }
            else {
                logger.info('save reference aqi.');   
            }
        });
    }, 20 * 60 * 1000);
}

module.exports = {
    cities: getCities, 
    save: saveRefAqi,
    task: saveTask
};