var oss = require('./oss');
var BUCKET_NAME= 'bosch-clean';

function ossPut(key, data, callback) {
	oss.putObject({
		Bucket: BUCKET_NAME,
		Key: key,
		Body: data,
		AccessControlAllowOrigin: '',
		ContentType: 'application/x-tar',
		CacheControl: 'no-cache',
		ContentDisposition: '',
		// ServerSideEncryption: 'AES256',
		Expires: 60
	}, callback);
}

function ossGet(key, callback) {
	oss.getObject({
      Bucket: BUCKET_NAME,
      Key: key
    }, callback);
}

module.exports = {
	put: ossPut,
	get: ossGet
};
