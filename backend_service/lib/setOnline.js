var moment = require('moment');
var resources = require('./resources');
var client = resources.getRedis();
var async = require('async');

var PARALLEL_COUNT = 100;
var DEVICE_ONLINE_STATUS = 'DEVICE_ONLINE_STATUS';
var DEVICE_ONLINE_TIME = 'DEVICE_ONLINE_TIME';

function setOnline(){
    // get all online status
    client.hgetall(DEVICE_ONLINE_STATUS, function(err, status){
        if (status) {
            var i = [];
            for (var deviceId in status) {
                i.push({
                    d: deviceId,
                    s: status[deviceId]
                });
            }
            async.eachLimit(i, PARALLEL_COUNT, function(ds, callback){
                if (ds.s === 'online') {
                    client.hset(DEVICE_ONLINE_TIME, ds.d, moment().valueOf(), callback);
                    console.log('set ' + ds.d + ' online.');
                }
                else{
                    client.hset(DEVICE_ONLINE_TIME, ds.d, moment().valueOf(), callback);
                    console.log('set ' + ds.d + ' offline.');
                }
            });
        }
    });
}

setOnline();