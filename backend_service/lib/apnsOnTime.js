var RTDataManager = require('./RTDataManager').RTDataManager;
var MSG = require('./RTMessage');
var logger = require('./logging');
var apnPush = require('./apnHelper');

var async = require('async');
var moment = require('moment');

var later = require('later');
later.date.localTime();

var PARALLEL_COUNT = 100;

// APNS schedule
var scheduleArray = [
    ['MORNING_REPORT', '8', '30'],
    ['NOONTIME_REPORT', '12', '30'],
    ['EVENING_REPORT', '20', '30']
];

// return loc-key
function alarmLevel(aqi, level_1) {
    aqi = parseInt(aqi);
    level_1 = parseInt(level_1);
    
    var result = ['WARNING_LEVEL_0', 'EXCELLENT_MSG'];
    
    if (aqi <= 35) {
	result = ['WARNING_LEVEL_0', 'EXCELLENT_MSG'];
    }
    else if (aqi <= level_1) {
        result = ['WARNING_LEVEL_1', 'GOOD_MSG'];
    }
    else if (aqi <= 115) {
        result = ['WARNING_LEVEL_2', 'SLIGHT_MSG'];
    }
    else if (aqi <= 150) {
        result = ['WARNING_LEVEL_3', 'MODERATE_MSG'];
    }
    else if (aqi <= 250) {
        result = ['WARNING_LEVEL_4', 'HIGH_MSG'];
    }
    else {
        result = ['WARNING_LEVEL_5', 'SEVERE_MSG'];
    }
    logger.info('alarmLevel: ', aqi, level_1, result);
    return result;
}

// return no disturbing
function isNoDist(string, hour, minute) {
    var disturbObj;
    try {
        disturbObj = JSON.parse(string);
    } catch (err) {
        disturbObj = [];
    }
    // console.log(disturb);
    for (var key in disturbObj) {
        var disturb = disturbObj[key];
        if (disturb['switch'] == 'ON') {
            var from = parseInt(disturb.from);
            var to = parseInt(disturb.to);
            switch (disturb.repeat) {
                case 'everyday': {
                    var current = parseInt(hour + minute);
                    if (current >= from && current <= to) {
                        return false;
                    }
                }
                break;
                case 'never': {
                    var current = parseInt(moment().format('YYMMDD') + hour + minute + '00');
                    if (current >= from && current <= to) {
                        return false;
                    }
                }
                break;
            }
        }
    }
    return true;
}

// apns on time
function sendApns(key, hour, minute) {
    var userData = RTDataManager.userData;
    var currentData = RTDataManager.currentData;
    var deviceData = RTDataManager.deviceData;
    // get users list
    userData.getList(function(err, users){
        // console.log(users);
        async.eachLimit(users, PARALLEL_COUNT, function(username, callback){
            // get apns token
            userData.getDataItem(username, 'apns_token', function(err, token){
                // console.log(token);
                if (token !== null && token !== 'null'){
                    // get device data
                    userData.getDevices(username, function(err, devicesList){
                        for (var deviceId in devicesList) {
                            var payload = {
                                "aps": {
                                    "alert":{
                                        "title-loc-key": key,
                                        "loc-key": '',
                                        "loc-args":['', '']
                                    },
                                    "badge": null,
                                    "sound": "default"
                                },
                                "acme-dev-id": deviceId, 
                                "acme-temp": null,
                                "acme-humi": null
                            };
                            // get default device only one
                            if (devicesList[deviceId]['isDefault'] == 1) {
                                // get current data
                                currentData.get(deviceId, function(err, cData){
                                    // send apns
                                    if (cData !== null) {
                                        // payload['acme-pm25'] = currentData[1][0];
                                        payload['acme-temp'] = cData[1][1];
                                        payload['acme-humi'] = cData[1][2];
                                        payload.aps.badge = cData[1][0];
                                        payload.aps.alert['loc-args'][1] = "PM2.5: " + cData[1][0];
                                    }
                                    // get device name
                                    deviceData.getData(deviceId, function(err, deviceObj){
                                    if (deviceObj['DevName'] !== null) {
                                        payload.aps.alert['loc-args'][0] = deviceObj['DevName'];
                                    }
                                    // get sensitivity and set alert level
                                    var level_1 = 75;   // default
                                    if (deviceObj['alarmAqi']) {
                                        level_1 = deviceObj['alarmAqi'];
                                    }
                                    if (cData !== null) {
                                        payload.aps.alert['loc-key'] = alarmLevel(cData[1][0], level_1)[1];
                                    }
                                    
                                    var isSend = true;
                                    if (deviceObj['noDisturb'] !== null) {
                                        isSend = isNoDist(deviceObj['noDisturb'], hour, minute);
                                    }
                                    
                                    if (isSend && cData) {
                                        if (cData[0] > (moment().valueOf() - 3*60*1000) &&
                                            cData[0] < (moment().valueOf() + 60*1000)) {
                                            // send apns 
                                            logger.info('apns on time, ', username, token, payload);
                                            apnPush(token, payload);
                                        }
                                        else {
                                            logger.info(deviceId, 'not send alert when lineoff ', cData[0], moment().valueOf());
                                        }
                                        
                                    }
                                    else {
                                        logger.info(deviceId, 'do not disturb alert in ' + hour + ':' + minute);
                                    }
                                        
                                    });
                                });
                                break;
                            }
                        }
                    });
                }
            });
            callback();
        });
    });
}

function apnsOnTime() {
    later.setInterval(function(){
        sendApns(scheduleArray[0][0], scheduleArray[0][1], scheduleArray[0][2]);
    }, later.parse.cron(scheduleArray[0][2] + ' ' + scheduleArray[0][1] + ' ? * *'));
    later.setInterval(function(){
        sendApns(scheduleArray[1][0], scheduleArray[1][1], scheduleArray[1][2]);
    }, later.parse.cron(scheduleArray[1][2] + ' ' + scheduleArray[1][1] + ' ? * *'));
    later.setInterval(function(){
        sendApns(scheduleArray[2][0], scheduleArray[2][1], scheduleArray[2][2]);
    }, later.parse.cron(scheduleArray[2][2] + ' ' + scheduleArray[2][1] + ' ? * *'));
}

function sendAlert(err, alertObj){
    var deviceId = alertObj['DevID'];
    var pm25 = alertObj['pm25'];
    if (deviceId){
        // get users
        deviceData = RTDataManager.deviceData;
        deviceData.getUsers(deviceId, function(err, usersList){
            logger.info('sendAlert usersList: ', usersList);
            if (usersList){
                var list = [];
                for (var username in usersList) {
                    list.push(username);
                }
                // get current value
                var currentData = RTDataManager.currentData;
                currentData.get(deviceId, function(err, data){
                    // console.log(data);
                    if (data.length !== 0) {
                        // payload
                        var payload = {
                            "aps": {
                                "alert":{
                                    "title-loc-key": '',
                                    "loc-key": '',
                                    "loc-args":['', '']
                                },
                                "badge": '',
                                "sound": 'default',
                                "category": 'ALARM_CATEGORY'
                            },
                            "acme-dev-id": deviceId, 
                            "acme-temp": null,
                            "acme-humi": null
                        };
                        // set pm2.5
                        payload['acme-temp'] = data[1][1];
                        payload['acme-humi'] = data[1][2];
                        payload.aps.badge = pm25;
                        payload.aps.alert['loc-args'][1] = "PM2.5: " + pm25;
                        // users
                        async.each(list, function(username, callback){
                            // console.log(username);
                            // get user token
                            var userData = RTDataManager.userData;
                            userData.getDataItem(username, 'apns_token', function(err, token){
                                // console.log(token);
                                if (token !== null && token !== 'null'){
                                    // get device info
                                    deviceData.getData(deviceId, function(err, deviceObj){
                                        // set device name
                                        if (deviceObj['DevName'] !== null) {
                                            payload.aps.alert['loc-args'][0] = deviceObj['DevName'];
                                        }
                                        // get sensitivity and set alert level
                                        var level_1 = 75;   // default
                                        if (deviceObj['alarmAqi']) {
                                            level_1 = deviceObj['alarmAqi'];
                                        }
                                        payload.aps.alert['title-loc-key'] = alarmLevel(pm25, level_1)[0];
                                        payload.aps.alert['loc-key'] = alarmLevel(pm25, level_1)[1];
                                        // DND
                                        var isSend = true;
                                        if (deviceObj['noDisturb'] !== null) {
                                            isSend = isNoDist(deviceObj['noDisturb'], moment().hour(), moment().minute());
                                        }
                                        // apns
                                        if (isSend) {
                                            // send apns
					    logger.info('send alert, ', username, token, payload);
                                            apnPush(token, payload);
                                        }
                                        else {
                                            logger.info(deviceId, 'do not disturb alert in ' + moment().hour() + ':' + moment().minute());
                                        }
                                        callback();
                                    });
                                }
                            });
                        });
                    }
                });
            }
        });
    }
}

function sendEvent(eventObj, db) {
    var deviceId = eventObj['DevID'];
    var event = eventObj['event'];
    if (deviceId){
        // get users
        deviceData = RTDataManager.deviceData;
        deviceData.getUsers(deviceId, function(err, usersList){
            logger.info('sendEvent usersList: ', usersList);
            if (usersList){
                var list = [];
                for (var username in usersList) {
                    list.push(username);
                }
                // payload
                var payload = {
                    "aps": {
                        "alert":{
                            "title-loc-key": '',
                            "loc-key": '',
                            "loc-args":['']
                        },
                        "badge": 0,
                        "sound": 'default'
                    },
                    "acme-dev-id": deviceId
                };
                if (event == 'automute') {
                    payload.aps.alert['title-loc-key'] = 'DEVICE_AUTO_MUTE';
                    payload.aps.alert['loc-key'] = 'DEVICE_AUTO_MUTE_MSG';
                }
                else if (event == 'online') {
                    // payload.aps.alert['title-loc-key'] = 'DEVICE_ONLINE';
                    // payload.aps.alert['loc-key'] = 'DEVICE_ONLINE_MSG';
                    setUserConfig(db, deviceId);
                }
                else if (event == 'offline') {
                    // payload.aps.alert['title-loc-key'] = 'DEVICE_OFFLINE';
                    // payload.aps.alert['loc-key'] = 'DEVICE_OFFLINE_MSG';
                }
                deviceData.getData(deviceId, function(err, deviceObj){
                    // set device name
                    if (deviceObj['DevName'] !== null) {
                        payload.aps.alert['loc-args'][0] = deviceObj['DevName'];
                    }
                    // DND
                    var isSend = true;
                    if (deviceObj['noDisturb'] !== null) {
                        isSend = isNoDist(deviceObj['noDisturb'], moment().hour(), moment().minute());
                    }
                    // users
                    async.each(list, function(username, callback){
                        // console.log(username);
                        // get user token
                        var userData = RTDataManager.userData;
                        userData.getDataItem(username, 'apns_token', function(err, token){
                            console.log(token);
                            if (token !== null && token !== 'null'){
                                // apns
                                if (isSend && payload.aps.alert['title-loc-key'] !== '') {
                                    // send apns
				    logger.info('send event, ', username, token, payload);
                                    apnPush(token, payload);
                                }
                                else {
                                    logger.info(deviceId, 'not send event when' + moment().hour() + ':' + moment().minute());
                                }
                            }
                        });
                    });
                });
            }
        });
    }
}

// get device disturb
function getDisturb(disturbString) {
    var disturbObj;
    try {
        disturbObj = JSON.parse(disturbString);
    } catch (err) {
        disturbObj = [];
    }
    var result = [];
    for (var key in disturbObj) {
        var disturb = disturbObj[key];
        if (disturb['switch'] == 'ON') {
            switch (disturb.repeat) {
                case 'everyday': {
                    result[0] = [
                        (disturb.from.slice(0, 2)),
                        (disturb.from.slice(2)),
                        (disturb.to.slice(0, 2)),
                        (disturb.to.slice(2))
                    ];
                }
                break;
                case 'never': {
                    result[1] = [
                        disturb.from,
                        disturb.to
                    ];
                }
                break;
            }   
        }
    }
    // check length
    for (var i = 0; i < 5; i++) {
        if (result[i] == null || typeof(result[i]) == 'undefined') {
            result[i] = [];
        }
    }
    return result;
}


// set user config when device online
function setUserConfig(db, deviceId) {
    db.device.findOne({deviceId: deviceId}, function(err, deviceObj){
        if (err) {
            logger.warn(err);
        }
        // console.log(deviceObj);
        if (deviceObj !== undefined) {
            // send cmd
            var config = {
                SiDayBe: '000000000000', 
                SiDayEn: '000000000000',
                SiHrBe: 22,
                SiMinBe: 0,
                SiHrEn: 6,
                SiMinEn: 0,
                Pm25Throd: (deviceObj.alarmAqi == null) ? 75 : deviceObj.alarmAqi,
                Pm25Unit: (deviceObj.aqiUnit == 'a') ? 'AQICN' : 'MG',
                TempUnit: (deviceObj.tempUnit == 'f') ? true : false,
                ScnBright: (deviceObj.brightness == null) ? 100 : deviceObj.brightness,
                ScnTime: (deviceObj.lightingTime == null) ? 60 : deviceObj.lightingTime,
                VolMax: (deviceObj.volume == null) ? 100 : deviceObj.volume
            };
            var d = [];
            if (deviceObj.noDisturb !== null) {
                d = getDisturb(deviceObj.noDisturb);
                // console.log(d);
            }
            if (d.length > 0 && d[0].length > 0) {
                config.SiHrBe = parseInt(d[0][0]);
                config.SiMinBe = parseInt(d[0][1]);
                config.SiHrEn = parseInt(d[0][2]);
                config.SiMinEn = parseInt(d[0][3]);
            }
            if (d.length > 0 && d[1].length > 0) {
                config.SiDayBe = d[1][0];
                config.SiDayEn = d[1][1];
            }
            logger.info('online send cmd: ', config);
            MSG.deviceCmd.commands.userConfig(deviceObj.deviceId, config);
        }
    });
}

function listenAlertQueue(sendAlert) {
    // alert apns
    MSG.deviceAlertQueue.pop(function(err, alertObj) {
        setImmediate(sendAlert, err, alertObj);
        setImmediate(listenAlertQueue, sendAlert);
    });
}

listenAlertQueue(sendAlert);

function send(db) {
	var deviceEvents = MSG.deviceEventChannel; 
	deviceEvents.sub(function(msg) {
		sendEvent(msg, db);
	});
}

module.exports = {
    run: apnsOnTime, 
    send: send
};
