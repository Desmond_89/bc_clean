var RTDataManager = require('./RTDataManager').RTDataManager;
var request = require('request');
var MSG = require('./RTMessage');
var logger = require('./logging');

var url = 'http://api.map.baidu.com/location/ip';
var ak = '4616bebaef4e59716e1cc9004226620e';

function handlerCitiesRequest(err, deviceId) {
    // console.log(deviceId);
    // get device ip
    var deviceData =  RTDataManager.deviceData;
    deviceData.getDataItem(deviceId, 'ip', function(err, ip){
        if (ip) {
            // console.log(deviceId + ': ' + ip);
            // ::ffff:112.65.189.177
            ip = ip.slice(7);
            // parse city
            request(url + '?ak=' + ak + '&ip=' + ip, function (error, response, body){
                // console.log(body);
                if (body !== null) {
                    var obj = JSON.parse(body);
                    // "CN|�Ϻ�|�Ϻ�|None|CHINANET|0|0"
                    var cityString = obj['address'];
                    var city = cityString.split('|')[2];
                    // set city
                    deviceData.setDataItem(deviceId, 'ipCity', city, function(){
                        deviceData.setDataItem(deviceId, 'city', city, function(){
                            logger.info('set ', deviceId, ' city ', city);
                        });
                    });
                   
                }
            });
        }
        else {
            console.log(deviceId + ': no-ip');
            // MSG.cityParseQueue.append(deviceId);
        }
    });
}

function listenCitiesQueue(handlerCitiesRequest) {
    MSG.cityParseQueue.pop(function(err, deviceId) {
        setImmediate(handlerCitiesRequest, err, deviceId);
        setImmediate(listenCitiesQueue, handlerCitiesRequest);
    });
}

listenCitiesQueue(handlerCitiesRequest);
