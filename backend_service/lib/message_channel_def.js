module.exports = {
	DOWNLINK_CMD: 'device_downlink_cmd',
	CONFIG_UPDATE: 'config_update',
	DEVICE_ALERT: 'device_alert_queue',
	APNS_MESSAGE: 'apns_message_queue',
	MAIL_QUEUE: 'mail_queue',
	DEVICE_CITY_PARSE_QUEUE: 'device_city_parse_queue',
	DEVICE_ONLINE_QUEUE: 'device_online_queue',
	DEVICE_EVENT_CHANNEL: 'device_event_channel', 
	HISTORY_CHANNEL: 'history_channel'
};