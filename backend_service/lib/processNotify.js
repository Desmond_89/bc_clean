var os = require('os');
var util = require('util');
var sendMail = require('./mailHelper');
var adminMail = 'bclean@aliyun.com';

function sendProcessStartMail(processName) {
	var serverInfo = os.hostname() + ', ' + os.type() + os.platform() 
		+ ', ' + os.arch() + ', ver:' + os.release() + ', mem: ' + os.totalmem()
		+ ', free: ' + os.freemem() + '\r\ncpus: ' + util.inspect(os.cpus()) 
		+ '\r\nnic: '	+ util.inspect(os.networkInterfaces());
	// send start mail
	sendMail(adminMail, processName+' started', 'started at ' + new Date() + '.\r\nServerInfo: ' + serverInfo);
}

sendProcessStartMail(process.title);