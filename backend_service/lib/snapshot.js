var heapdump = require('heapdump');
var resources = require('./resources');
var logger = resources.logger;
var SNAPSHORT_INTERVAL = 1000*60*60;

setInterval(function saveSnapshot(){
	var file = './heapdump/' + Date.now() + '.heapsnapshot';
	logger.info('heapdump write snapshot to ' + file);
	heapdump.writeSnapshot(file);
}, SNAPSHORT_INTERVAL);
