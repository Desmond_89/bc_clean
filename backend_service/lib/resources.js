var redis = require('redis');
// var config = require('./theconfig');
var debug = require('debug');
var logger = require('./logging');


function createRedisClient() {
	var client = redis.createClient(6379, 'f613d55dd75211e4.m.cnsza.kvstore.aliyuncs.com', {detect_buffers: true});
	client.auth("f613d55dd75211e4:M3kan4gBWbY8", redis.print);
	return client;
}

function createRedisTestClient() {
	return redis.createClient({detect_buffers: true});
}

module.exports = {
	getRedis: createRedisTestClient,
	debug: debug,
	logger: logger
};
