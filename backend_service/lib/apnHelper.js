var MSG = require('./RTMessage');

/*
 {
	"aps":{
		"alert":{
			"title-loc-key":"NOONTIME_REPORT",
			"loc-key":"MODERATE_MSG",
			"loc-args":["DeviceOfPercy","PM2.5 : 120"]
		},
		"badge":120,
		"sound":"default"
	},
	"acme-dev-id":"330083009174",
	"acme-temp":29,
	"acme-humi":63
 }
 */
function push(token, payload) {
	var obj = {
		token: token,
		payload: payload
	}
	MSG.APNsQueue.append(obj);
}

module.exports = push;