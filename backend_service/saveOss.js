var RTDataManager = require('./lib/RTDataManager').RTDataManager;

var async = require('async');
var moment = require('moment');

PARALLEL_COUNT = 20;

// get back up devices list
function getDeviceList(callback) {
    // all device once online
    deviceData = RTDataManager.deviceData;
    deviceData.list(function(err, devicesList){
        if (devicesList) {
            callback(null, devicesList);
        }
        else{
            callback(null, []);
        }
    });
}

// only save real devices list
function saveReal(){
    var deviceList = [
        '030083009174', 
        '220083009174', 
        '330083009174', 
        '520053009174', 
        '620083009174', 
        '710043009174', 
        '8300B100B074', 
        '910053009174', 
        '920083009174', 
        '9300D100B074', 
        'C300F2004174', 
        'F200C2004174', 
        'G10053009174', 
        'G10083009174'
    ];
    saveCurrent(deviceList);
}

saveReal();

// save devices year table, today's day table and current hour table from redis to oss
function saveCurrent(deviceList) {
    
    yearData = RTDataManager.yearData;
    dayData = RTDataManager.dayData;
    hourData = RTDataManager.hourData;

    // save oss
    async.eachLimit(deviceList, PARALLEL_COUNT, function(deviceId, callback){
        if (deviceId) {
            // save current hour table
            hourData.save(deviceId, moment().valueOf(), function(err){
                if (!err) {
                    console.log('save hour oss:', deviceId);
                }
            });
            // save today day table
            dayData.save(deviceId, moment().valueOf(), function(err){
                if (!err) {
                    console.log('save day oss:', deviceId);
                }
            });
            // save current year table
            yearData.save(deviceId, moment().valueOf(), function(err){
                if (!err) {
                    console.log('save year oss:', deviceId);
                }
            });
        }
    });
}
