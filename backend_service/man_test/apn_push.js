var apn = require('apn');
var options = {
	pfx: '../cert/Pro_Certificates.p12',
	passphrase: 'Bosch1234',
	production: true
};

var apnConnection = new apn.Connection(options);
apnConnection.on("connected", function() {
	console.log("Connected");
});

var myDevice = new apn.Device('784a6e2e32593b29c73c4c28938ee455c75cfe6db2a571ec373cb17f010b5432');
var note = new apn.Notification();
note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
note.badge = 777;
note.sound = 'default'; // "ping.aiff";
note.alert = 'This is a clean test message.'; //"\uD83D\uDCE7 \u2709 You have a new message";
note.payload = {'messageFrom': 'CBServer'};

apnConnection.pushNotification(note, myDevice);

// Payload：{\"aps\":{\"alert\":\"This is a test message.\",\"badge\”:777,\"sound\":\"default\"}}
// openssl pkcs12 -in Dev_Certificates.p12 -info -nokeys
// % openssl pkcs12 -nocerts -out PushChatKey.pem -in Dev_Certificates.p12
// Enter Import Password:
// Mac verify error: invalid password?

