var debug = require('debug')('pubsubTest');
var RTMSG = require('../lib/RTMessage');
var pubsub = RTMSG.testPubSub;

debug('listen...')

pubsub.sub(function(msg) {
	debug('one got message: ', msg);
});
pubsub.sub(function(msg) {
	debug('two got message: ', msg);
});
pubsub.sub(function(msg) {
	debug('three got message: ', msg);
});
setTimeout(function() {
	pubsub.pub('RTMSG.testPubSub');
	pubsub.pub(RTMSG);
}, 1000)
