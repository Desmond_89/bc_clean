net = require 'net'
async = require 'async'
debug = require('debug')('server')
MFSM = require '../src/MasterFSM'

showResponseThenCB = (cb) ->
	(err, res) ->
		debug 'error: ', err
		debug 'response: ', res
		cb err, res

main = (port)->
	userConfigData = 
		# Type: 'UserConfig'
		SiDayBe: '150108203622'
		SiDayEn: '150109203622'
		SiHrBe: 6
		SiHrEn: 22
		Pm25Throd: 80
		Pm25Unit: 'MG'
		TempUnit: false
		ScnBright: 90
		ScnTime: 10
		VolMax: 95

	# 'RecReport' 'Pm25AlertPre' 'Pm25AlertOn' 'Pm25AlertOff' 'SyncTime' 'ConfigResult' 'RecHistory' 'Pm25AlertMute' 

	server = net.createServer (c)->
		dev = MFSM.create c
		dev.on 'DevOnline', ->
			debug 'device Online'
			async.series 
				syncTime: (cb) ->
					debug 'syncTime'
					dev.syncTime showResponseThenCB(cb)
				# todo userConfig
				userConfig: (cb) ->
					debug 'userConfig'
					dev.userConfig userConfigData, showResponseThenCB(cb)
				# recQuery = (date, reso, unit)
				recQuery: (cb) ->
					debug 'recQuery'
					dev.recQuery new Date(), 'Min', 'Day', showResponseThenCB(cb)

				, (err, results) ->
						debug 'err: ', err if err
						debug 'results: ', results

		# mute
		dev.on 'Pm25AlertPre', (msg) ->
			debug 'send mute command after 1s'
			setTimeout dev.mute, 1000, (err, res) ->
				debug res, ' with err: ', err

		# handle err
		dev.on 'error', (err) ->
			debug err

		debug 'client connected from: ', c.remoteAddress, ':', c.remotePort

	server.listen port, ->
		debug 'server started at ', port

main(7777)