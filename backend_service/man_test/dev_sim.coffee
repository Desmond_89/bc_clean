net = require 'net'
debug = require('debug')('dev_sim')

RES_ONLINE = '{"DevInfo":{"DevName":"BoschIAQ","DevID":"7300G100B074","SwVer":"DevBuild"},"Payloads":[{"Type":"DevOnline","BusyTime":0,"AlertState":"Off","SystemState":"Work","Date":"150428204806"}]}\0'
RES_SYNCTIME = '{"DevInfo":{"DevName":"BoschIAQ","DevID":"7300G100B074","SwVer":"DevBuild"},"Payloads":[{"Type":"TimeUpdate","LocalTime":"150429145236","ServerTime":"150429145226"}]}\0'

c = net.connect
	port: 7777
, ->
	debug 'connected'
	c.write RES_ONLINE
	debug 'send online'

c.on 'data', (data) ->
	debug 'recv: ', data.toString()
	setTimeout ->
		debug 'send timeupdate'
		c.write RES_SYNCTIME
	, 500
