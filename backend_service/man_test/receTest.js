var moment = require('moment');

function transformMinData(t, datalist) {
	var block, i, item, lastMin, len, newT, ret;
	ret = [];
	newT = moment(t);
	lastMin = 90;
	block = null;
	for (i = 0, len = datalist.length; i < len; i++) {
	  item = datalist[i];
	  if (++lastMin !== item[0]) {
	    lastMin = item[0];
	    newT.minutes(lastMin);
	    block = [newT.valueOf()];
	    ret.push(block);
	  }
	  block.push(item.slice(1));
	}
	return ret;
}

var t = moment().valueOf();
var Min = [[50,21,129,76],[51,20,129,76],[52,21,129,76],[53,19,129,76],[54,20,129,76],[55,20,129,76],[56,20,129,76],[57,21,129,76],[58,18,129,76],[59,20,129,76]];
var Min = [];
var dataBlocks = transformMinData(t, Min);
console.log(dataBlocks);

// []
/*
[ [ 1440161437774,
    [ 21, 129, 76 ],
    [ 20, 129, 76 ],
    [ 21, 129, 76 ],
    [ 19, 129, 76 ],
    [ 20, 129, 76 ],
    [ 20, 129, 76 ],
    [ 20, 129, 76 ],
    [ 21, 129, 76 ],
    [ 18, 129, 76 ],
    [ 20, 129, 76 ] ] ]
*/

// RTDATA.minuteData.set DevID, block.shift(), block[0] for block in dataBlocks

/*
if dataBlocks.length > 0
	blocks = dataBlocks[0]
	timestamp = blocks.shift()
	RTDATA.minuteData.set DevID, (timestamp + key * 60*1000), value[0] for value, key in blocks
*/		

if (dataBlocks.length > 0) {
	var blocks = dataBlocks[0];
	var timestamp = blocks.shift();
	for(var key in blocks){
		console.log('timestamp', (timestamp + key * 60*1000));
		console.log('data', blocks[key][0]);
	}
}