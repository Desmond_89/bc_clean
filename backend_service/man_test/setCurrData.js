var RTDATA = require('../lib/RTDataManager').RTDataManager;
var RTMSG = require('../lib/RTMessage');
var moment = require('moment');

var DevID = 'cleanid003';

// set device online
RTMSG.deviceOnlineQueue.append(DevID);
RTMSG.deviceEventChannel.pub({
    DevID: DevID,
    event: 'online'
});

// set current data
RTDATA.currentData.set(DevID, [100, 10, 10], moment().valueOf(), function(){
    console.log('set current data ok.');
});

// set monute data
/*
RTDATA.minuteData.setCurrent(DevID, 300.123, function(err, rep){
    var hourData = RTDATA.hourData.get('T00287251123', moment().valueOf(), function(err2, ret){
        console.log(ret);
    });
});
*/

