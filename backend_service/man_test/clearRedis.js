var RTDataManager = require('../lib/RTDataManager').RTDataManager;
// var logger = require('./logging');

var async = require('async');
var moment = require('moment');

var later = require('later');
later.date.localTime();

PARALLEL_COUNT = 20;

function clearDaily() {
    deviceData = RTDataManager.deviceData;
    dayData = RTDataManager.dayData;
    hourData = RTDataManager.hourData;
    
    // get devices list
    deviceData.list(function(err, devicesList){
        if (devicesList) {
            // traverse devices list
            var list = [];
            for (var key in devicesList){
                list.push(devicesList[key]);
            }
            
            //list = [
                //'030083009174',
                //'330083009174',
                //'520053009174',
                //'620083009174',
                //'920083009174',
                //'9300D100B074',
                //'C300F2004174',
                //'F200C2004174',
                //'G10083009174'
            //];
            
            // save oss
            var current = moment().valueOf();
            async.eachLimit(list, PARALLEL_COUNT, function(deviceId, callback){
                if (deviceId) {
                    // var current = moment().valueOf();
                    var current = moment('150722', 'YYMMDD').valueOf();
                    var last = (current - 24*60*60*1000);
                    // clear last day
                    dayData.del(deviceId, last, function(err){
                        if (!err) {
                            // console.log('clear day ' + moment(last).format('YYMMDD') + ' data of ' + deviceId);
                        }
                        // clear last day hour
                        var allHours = [];
                        var start = moment(moment(last).format('YYMMDD'), 'YYMMDD').valueOf();
                        for (var i=0; i < 24; i++) {
                            allHours.push(start);
                            start += 60*60*1000;
                        }
                        async.eachLimit(allHours, PARALLEL_COUNT, function(hourTime, c1){
                            hourData.del(deviceId, hourTime, function(err){
                                if (!err) {
                                    // console.log('clear hour ' + moment(hourTime).format('YYMMDDHH') + ' data of ' + deviceId);
                                }
                                c1();
                            });
                        }, function(err){
                            console.log('clear ' + moment(last).format('YYMMDD') + ' data of ' + deviceId);
                            // logger.info('clear redis data', deviceId,  moment(last).format('YYMMDD'));
                            callback();
                        });
                    });
                }
            });
        }
    });
}

clearDaily();

