var later = require('later');
// later.date.localTime();

// APNS schedule
var scheduleArray = {
    'MORNING_REPORT': [7, 21],
    'NOONTIME_REPORT': [7, 22],
    'EVENING_REPORT': [7, 23]
};

for (var key in scheduleArray) {
    var schedule = later.parse.cron(
        scheduleArray[key][1] + ' ' +
        scheduleArray[key][0] + ' ' +
        '? * *'
    );
    
    var task = later.setInterval(function(){
        console.log(key);
    }, schedule);
}