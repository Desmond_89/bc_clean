var moment = require('moment');

// clear data
function clearData(deviceId, day){
	
}

// set call up task
function setCallupTaskInDay(db, deviceId, day){
	// traverse hour value in day
	var dayTimestamp = moment(day, 'YYMMDD').valueOf();
	for (var i = 0; i < 24; i++){
		var type = ['minute', 'hour', 'day', 'month'];
		// make task obj
		var tasksArr = [];
		for (var key in type){
			var taskObj = {
				deviceId: deviceId, 
				callUpType: type[key], 
				callUpTime: dayTimestamp + i*60*60*1000
			};
			tasksArr.push(taskObj);
		}
		// set in mysql
		db.callup.findOrCreate(taskObj, function(err){
			if (err){
				console.log('set task error:', err);
			}
			else{
				console.log('set task in day', deviceId, day);
			}
		});
	}
}

// waterline orm
var orm = require('../../../web_service/lib/orm');
orm(function(err, models){
    if(err) {
        throw err;
    }
    // db handle
    db = models.collections;
	setCallupTaskInDay(db, '330083009174', '150821');
});
