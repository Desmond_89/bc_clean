var callUp = require('../../lib/callUp');

// waterline orm
var orm = require('../../../web_service/lib/orm');
orm(function(err, models){
    if(err) {
        throw err;
    }
    // db handle
    db = models.collections;
    callUp.run(db);
});
