var RTDataManager = require('../lib/RTDataManager').RTDataManager;
var moment = require('moment');

function checkYear(device, time) {
    var yearData = RTDataManager.yearData;
    yearData.get(device, time, function(err, year){
	yearData.getHis(device, time, function(err, year2){
	    if (JSON.stringify(year) === JSON.stringify(year2)) {
		console.log('year ok');
	    }
	    else {
		console.log(year);
		console.log(year2);
	    }
	});
    });
}

function checkDay(device, time) {
    var dayData = RTDataManager.dayData;
    dayData.get(device, time, function(err, day){
	dayData.getHis(device, time, function(err, day2){
	    if (JSON.stringify(day) === JSON.stringify(day2)) {
		console.log('day ok');
	    }
	    else {
		console.log(day);
		console.log(day2);
	    }
	});
    });
}

function checkHour(device, time) {
    var hourData = RTDataManager.hourData;
    hourData.get(device, time, function(err, hour){
	hourData.getHis(device, time, function(err, hour2){
	    if (JSON.stringify(hour) === JSON.stringify(hour2)) {
		console.log('hour ok');
	    }
	    else {
		console.log(hour);
		console.log(hour2);
	    }
	});
    });
}

var device = '030083009174';
var time = moment('15071600', 'YYMMDDHH').valueOf();

checkYear(device, time);
checkDay(device, time);
checkHour(device, time);