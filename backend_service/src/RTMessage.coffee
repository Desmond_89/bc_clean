resources = require('./resources')
MB = require('./MessageBus')
CMD = require('./BCDevice').requestCmds
helper = require('./helper')
CHANNEL_DEF = require('./message_channel_def')
resources = require './resources'
logger = resources.logger
debug = resources.debug 'RTMSG'

QUEUE_BLOCK_TIME = 0

client = resources.getRedis()

createQueue = (queue) ->
	queueClient = resources.getRedis()
	ret = 
		append: queueAppendMessageObj.bind(undefined, queue)
		pop: queuePopMessageObj.bind(undefined, queueClient, queue)

queueAppendMessageObj = (queue, Obj) ->
	client.lpush queue, JSON.stringify(Obj), (err, reply)->
		logger.warn 'queueAppendMessageObj '+err if err

queuePopMessageObj = (popClent, queue, callback) ->
	popClent.brpop queue, QUEUE_BLOCK_TIME, (err, reply) ->
		debug 'pop from ', queue, reply
		helper.JSONCallback err, reply[1], callback
		if err
			logger.warn 'queuePopMessageObj '+err 

setupCmd = ->
	ret = {}
	for k, v of CMD
		ret[k] = sendCommand.bind null, k

	ret
	
sendCommand = (argsAll...) ->
	[cmd, deviceId, args...] = argsAll
	logger.info('send command: ',argsAll);
	message = 
		DevID: deviceId
		cmd: cmd
		args: args
	sendDownlinkCommand message


sendDownlinkCommand = (msg) ->
	client.publish CHANNEL_DEF.DOWNLINK_CMD, JSON.stringify(msg)

listen_downlink_cmd = ->
	MB.listen(CHANNEL_DEF.DOWNLINK_CMD)

# create pub/sub channel
# everytime sub function is called, one redis client will be created. Be careful, don't create too much of them.
create_pubsub = (chann) ->
	ret = 
		pub: (msg)->
			client.publish chann, JSON.stringify(msg)
		sub: (onMessage) ->
			subClient = MB.listen(chann)
			subClient.on 'message', onMessage


module.exports = 
	deviceCmd:
		listen: listen_downlink_cmd
		commands: setupCmd()
	APNsQueue: createQueue(CHANNEL_DEF.APNS_MESSAGE)
	deviceAlertQueue: createQueue(CHANNEL_DEF.DEVICE_ALERT)
	mailQueue: createQueue(CHANNEL_DEF.MAIL_QUEUE)
	cityParseQueue: createQueue(CHANNEL_DEF.DEVICE_CITY_PARSE_QUEUE)
	deviceOnlineQueue: createQueue(CHANNEL_DEF.DEVICE_ONLINE_QUEUE)
	deviceEventChannel: create_pubsub(CHANNEL_DEF.DEVICE_EVENT_CHANNEL)
	receiveHistoryChannel: create_pubsub(CHANNEL_DEF.HISTORY_CHANNEL)
	# testPubSub: create_pubsub('pub_sub_test')


