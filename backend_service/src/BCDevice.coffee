# Device communication protocol
moment = require('moment')
debug = require('debug')('Protocol')
MSG_SPLITTER = '\0'
DATE_FORMAT = 'YYMMDDHHmmss'
resources = require './resources'
logger = resources.logger

formatInt = (n, len) ->
	str = '0000' + n 
	ret = str[-len..]

now = ->
	d = new Date()
	formatDate(d)

formatDate = (d) ->
	moment(d).format(DATE_FORMAT)

parseDate = (dateStr) ->
	moment(dateStr, DATE_FORMAT).valueOf()

parse = (strBuff) ->
	ret = null
	hasMessage = strBuff.indexOf(MSG_SPLITTER)
	if hasMessage > 0
		msgStr = strBuff.substr(0, hasMessage)
		# strBuff = strBuff.substr(hasMessage+1)
		try 
			msg = JSON.parse(msgStr)
			ret = 
				Type: msg.Payloads[0].Type
				message: msg
				len: hasMessage
		catch e
			debug 'parse: ', msgStr
			debug 'JSON format error: ', e
			logger.warn 'Got format error message: '+msgStr
			ret = 
				len: hasMessage

	return ret

syncTime = ->
	ret = 
		Type: 'SyncTime'
		Date: now()
		ResponseMsg: 'TimeUpdate'

userConfig = (config) ->
	config.Type = 'UserConfig'
	config.ResponseMsg = 'ConfigResult'

	config

recQuery = (date, reso, unit) ->
	ret = 
		Type: 'RecQuery'
		Date: formatDate(date)
		Reso: reso
		Unit: unit

mute = ->
	ret = 
		Type: 'MuteAlert'
		Date: now()

module.exports = 
	execute: parse
	MSG_SPLITTER: MSG_SPLITTER
	parseDate: parseDate
	requestCmds: 
		syncTime: syncTime
		userConfig: userConfig
		recQuery: recQuery
		mute: mute

		