moment = require('moment')
util = require ('util')
RTMSG = require('./RTMessage')
apnPush = require('./apnHelper')
RTDATA = require('./RTDataManager').RTDataManager
Dev = require './BCDevice'
RESOURCE = require './resources'
logger = RESOURCE.logger
debug = RESOURCE.debug('DeviceDataHandler')
od = RESOURCE.od

TIMER_MINUTE = 1000*60

od = (o) ->
	debug util.inspect(o,{depth: null})

handleEvent = (rawMsg) ->
	{DevInfo:{DevID}, Payloads: [alert]} = rawMsg
	RTDATA.deviceEvent.add DevID, alert

# transform [[min, a, t, h]...] to [[timestamp, [a, t, h], ...], ...] if min is continous
transformMinData = (t, datalist) ->
	ret = []
	newT = moment(t)
	lastMin = 90
	block = null
	if (typeof datalist) isnt 'undefined'
		for item in datalist
			if ++lastMin isnt item[0]
				lastMin = item[0]
				newT.minutes(lastMin)
				block = [newT.valueOf()]
				ret.push block
			block.push item.slice(1)
	ret

handlerFunc = 
	error: (msg) ->
		logger.error @_sock_id + ':' + msg

	closed: ()->
		handler.remove @

	DevReady: (rawMsg)->
		handleEvent rawMsg

	DevFault: (rawMsg)->
		{DevInfo:{DevName, SwVer, HwVer, DevID}, Payloads:[{Type}]} = rawMsg
		RTDATA.deviceEvent.add DevID, Type

	ConfigResult: (rawMsg)->
		{DevInfo:{DevID}, Payloads: [{Status, Type}]} = rawMsg
		debug 'recv ', Type, ' from ', DevID

	TimeUpdate: (rawMsg)->
		{DevInfo:{DevID}, Payloads: [{LocalTime, ServerTime, Type}]} = rawMsg
		logger.info 'TimeUpdate', 
			DevID: DevID
			LocalTime: LocalTime
			ServerTime: ServerTime

	RecReport: (rawMsg)->
		# (deviceId, data, timestamp, callback)
		{DevInfo:{DevID}, Payloads: [{Date, pm25, temp, humi, Type}]} = rawMsg
		logger.debug DevID + ' current data: ' + [Date, pm25, temp, humi]
		RTDATA.currentData.set DevID, [pm25, temp, humi], Dev.parseDate(Date)
		handler.appendRecentData DevID, pm25

	RecHistory: (rawMsg)->
		{DevInfo:{DevID}, Payloads: [{Date, Sec, Min, Type}]} = rawMsg
		t = Dev.parseDate(Date)
		dataBlocks = transformMinData(t, Min)
		# RTDATA.minuteData.set DevID, block.shift(), block[0] for block in dataBlocks
		if dataBlocks.length > 0
			blocks = dataBlocks[0]
			timestamp = blocks.shift()
			RTDATA.minuteData.set DevID, (timestamp + key * 60*1000), value[0] for value, key in blocks
		RTMSG.receiveHistoryChannel.pub {DevID, Date, data: Min}

	DevOnline: (rawMsg) ->
		handler.online rawMsg, @
		{DevInfo: {DevID}} = rawMsg
		RTDATA.deviceEvent.add DevID, {Type: 'online', Date: moment().format 'YYMMDDHHmmss'}
		
	Pm25AlertPre: (rawMsg) ->
		handleEvent rawMsg
	Pm25AlertOn: (rawMsg) ->
		handleEvent rawMsg
		{DevInfo: {DevID}, Payloads: [{Date, pm25}]} = rawMsg
		RTMSG.deviceAlertQueue.append {DevID, Date, pm25}
		
	Pm25AlertMute: (rawMsg) ->
		handleEvent rawMsg
		# auto mute apns
		{DevInfo: {DevID}, Payloads: [{Date, pm25, auto}]} = rawMsg
		if auto
			# RTMSG.deviceEventQueue.append {DevID, event: 'automute'}
			RTMSG.deviceEventChannel.pub {DevID, event: 'automute'}
		else
			# RTMSG.deviceEventQueue.append {DevID, event: 'mute'}
			RTMSG.deviceEventChannel.pub {DevID, event: 'mute'}
			
	Pm25AlertOff: (rawMsg) ->
		handleEvent rawMsg

minuteTaskHelper = (theDeviceDataHandler) ->
	theDeviceDataHandler.minuteTask()

calcMinuteAvg = (deviceId, datalist) ->
	if datalist.length >= 10
		sum = 0
		sum += data for data in datalist
		avg = sum/datalist.length

		RTDATA.minuteData.setCurrent deviceId, avg
		logger.debug('calcMinuteAvg:', deviceId, datalist)
		datalist.splice 0, 10

class DeviceDataHandler
	constructor: ->
		# 
		@devList = {}
		@recentData = {}
		# @setupTaskTimer()
		@setupCmdListener()

	setupTaskTimer: ->
		_this = @
		setInterval minuteTaskHelper, TIMER_MINUTE, _this

	minuteTask: ->
		# calculate minute average aqi
		@calcMinuteAvg deviceId, datalist for deviceId, datalist of @recentData when datalist.length>0

	appendRecentData: (deviceId, data) ->
		@recentData[deviceId] ?= []
		@recentData[deviceId].push data
		calcMinuteAvg deviceId, @recentData[deviceId]

	setupCmdListener: ->
		@cmdListener = RTMSG.deviceCmd.listen()
		# message = 
		# 	DevID: deviceId
		# 	cmd: cmd
		# 	args: args
		@cmdListener.on 'message', (message, channel) =>
			if message? and message.DevID
				{DevID, cmd, args} = message
				dev = @devList[DevID]
				if dev?
					debug 'send command: ', message
					dev[cmd] args..., (err, res) ->
						debug 'result: ', res, ', with error: ', err
				else
					debug DevID, ' is not managed by this process.'


	handle: (dev) ->
		# device is a MasterFMS
		for msg, Func of handlerFunc
			dev.on msg, Func

	remove: (dev) ->
		if dev.deviceId?
			deviceId = dev.deviceId
			debug 'devList:', @devList
			oin = @devList[deviceId]
			debug 'oin:', oin, ', dev:', dev
			if oin? and oin._sock_id is dev._sock_id
				logger.info 'remove '+deviceId+' at '+dev._sock_id+' from register.'
				delete @devList[deviceId] 
				RTDATA.deviceData.offline dev.deviceId
				RTDATA.deviceEvent.add dev.deviceId, {Type: 'offline', Date: moment().format 'YYMMDDHHmmss'}
				# offline apns
				# RTMSG.deviceEventQueue.append {DevID: deviceId, event: 'offline'}
				RTMSG.deviceEventChannel.pub {DevID: deviceId, event: 'offline'}

	online: (rawMsg, dev) ->
		{DevInfo:{DevName, SwVer, HwVer, DevID}, Payloads:[{Type, BusyTime, AlertState, SystemState}]} = rawMsg
		dev.online(DevID)
		oldLink = @devList[DevID]
		@devList[DevID] = dev
		if oldLink?
			logger.info DevID + ' online from ' +dev._sock_id+', so close '+oldLink._sock_id
			oldLink.close()
		# set device info
		# debug 'RTDATA.online:', RTDATA.deviceData
		# od RTDATA
		RTDATA.deviceData.online DevID, 
			onlineTime: Date.now()
			ip: dev._remoteAddress
			port: dev._remotePort
			ipCity: ''
			SwVer: SwVer
			HwVer: HwVer
			DevName: DevName
			DevID: DevID
			BusyTime: BusyTime
			AlertState: AlertState
			SystemState: SystemState

		# synctime
		dev.syncTime (err, response) ->
			logger.info 'synctime to '+DevID
			
		# device online: create current hour table
		RTMSG.deviceOnlineQueue.append DevID;
		
		# online apns
		# RTMSG.deviceEventQueue.append {DevID, event: 'online'}
		RTMSG.deviceEventChannel.pub {DevID, event: 'online'}

handler = new DeviceDataHandler()

module.exports = 
	handle: handler.handle
