net = require 'net'
debug = require('debug')('netServer')
MFSM = require './MasterFSM'
Device = require './deviceDataHandler'
resources = require './resources'
logger = resources.logger


class NetServer
	constructor: ->

	start: (port)->
		server = net.createServer (c)->
			dev = MFSM.create c
			Device.handle dev
			logger.info 'client connected from: '+ c.remoteAddress+ ':'+ c.remotePort
			debug 'client connected from: ', c.remoteAddress, ':', c.remotePort

		server.listen port, ->
			debug 'server started at ', port
			logger.info 'server started at '+ port



module.exports = NetServer
