util = require('util')
debug = require('debug')('datastruct')

# log = console.log
# yearData = 
# 	val: 123
# 	month: 
# 		1: 
# 			val: 12
# 			day: 
# 				1: 
# 					val: 123
# 					disturb: [[3,5],]
# 					rank: [99 ,87]
# 		2: 2

decodeItem = (holder, buf) ->
	# debug 'decode: ', holder
	ret = holder.val.decode buf
	for item, list of holder when item isnt 'val'
		if util.isArray(list)
			ret[item] = []
			ret[item].push(decodeItem(item1, buf)) for item1 in list
		else
			ret[item] = decodeItem(list, buf)

	ret


class ValueHolder
	constructor: (@val)->

	# get: ()->
	# 	@val.get()
	# getAll: (buf)->
	# 	# 
	# 	decodeItem @, buf

	# set: (value) ->
	# 	@val.set(value)


class BinaryVal
	byteLen: 2
	constructor: (@binOffset)->

	encode: (value)->
		buf = new Buffer(@byteLen);
		buf.writeUInt16BE(value, 0)
		buf

	decode: (buf) ->
		ret = 
			aqi: buf.readUInt16BE(@binOffset)

	get: (getfunc, callback) =>
		getfunc @binOffset, @binOffset+@byteLen, (err, buf) =>
			callback err, @decode(buf)

	set: (setfunc, value, callback) ->
		setfunc(@binOffset, @encode(value), callback)
# aqi:2,
class DayBinaryVal extends BinaryVal
	byteLen: 26
	decode: (buf) ->
		offset = @binOffset
		# debug 'day binary decode:'
		# debug 'buffer len:', buf.length
		# debug 'read from offset:', offset
		aqi = buf.readUInt16BE offset
		disturb = []
		offset += 2
		for i in [0...5]
			item = []
			for j in [0...4]
				item.push buf.readUInt8(offset++)
			disturb.push item
		rank = []
		for i in [0...2]
			rank.push buf.readUInt16BE(offset)
			offset += 2
		ret = 
			aqi: aqi
			disturb: disturb
			rank: rank
	encode: (value) ->
		# value = {aqi, disturb:[[h1,m1, h2, m2], [3,4],[1,2], [3,4],[1,2]], rank:[1,2]}
		{aqi, disturb, rank} = value
		buf = new Buffer(@byteLen)
		buf.fill(0)
		i = 0
		buf.writeUInt16BE(aqi, i)
		i+=2
		for item in disturb
			buf.writeUInt8(item[0], i++)
			buf.writeUInt8(item[1], i++)
			buf.writeUInt8(item[2], i++)
			buf.writeUInt8(item[3], i++)

		buf.writeUInt16BE(rank[0], i)
		i += 2
		buf.writeUInt16BE(rank[1], i)
		i+=2
		buf




yearScheme = 
	type: BinaryVal
	month: 
		type: BinaryVal
		day:
			type: DayBinaryVal
			count: 31
		count: 12
	# quarter:
	# 	type: BinaryVal
	# 	count: 1

dayScheme = 
	type: BinaryVal
	hour:
		type: BinaryVal
		count: 24

hourScheme = 
	type: BinaryVal
	minute: 
		type: BinaryVal
		count: 60


createItem = (itemDef, offset) ->
	binnVal = new itemDef.type(offset)
	# debug 'create ', itemDef.type
	ret = new ValueHolder(binnVal)

	offset += binnVal.byteLen 

	for item, def of itemDef when item isnt 'type' and item isnt 'count'
		# debug 'parse: ', item
		[newItem, newOffset] = parseItem(def, offset)
		offset = newOffset
		ret[item] = newItem

	[ret, offset]


parseItem = (itemDef, offset) ->
	count = itemDef.count ? 1
	# debug itemDef.type
	ret = null
	if count is 1
		[ret , offset] = createItem itemDef, offset
	else if count > 1
		ret = []
		for i in [0..count]
			[item , offset] = createItem itemDef, offset
			ret.push(item)

	[ret, offset]

ys = parseItem(yearScheme, 0)
ds = parseItem(dayScheme, 0)
hs = parseItem(hourScheme, 0)

debug 'ys: ', ys[1]
debug 'ds: ', ds[1]
debug 'hs: ', hs[1]

module.exports = 
	year: ys
	day: ds
	hour: hs
	decode: decodeItem




