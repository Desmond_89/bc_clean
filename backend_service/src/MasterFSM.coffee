util = require 'util'
EventEmitter = require('events').EventEmitter
NodeState = require 'node-state'
debug = require('debug')('MFSM')
Dev = require './BCDevice'
resources = require './resources'
logger = resources.logger

BUFF_LEN = 1024


od = (o) ->
	debug util.inspect(o,{depth: null})

REQ_TIMEOUT = 1000
NO_TRAFFIC_TIMEOUT = 60*1000
ONLINE_TIMEOUT = 30*1000
CONNECTIONS_PER_IP = 3

ip_connections = {}

isObjectEmpty = (o) ->
	Object.keys(o).length is 0

arrayRemoveEle = (arr, ele) ->
	index = arr.indexOf ele
	arr.splice index, 1 if index>=0

# check the connection from single ip, if too much connection from one ip,
# close one of previous
# ip_connections = {
# 	remoteAddress: {
# 		port1: sock1
# 		port2: sock2
# 		ports: [port1, port] which record the sequence
# 	}
# }

checkAddress = (c) ->
	ret = true
	ip_connections[c.remoteAddress] ?= 
		ports: []
	ip_connections[c.remoteAddress].ports.push c.remotePort
	ip_connections[c.remoteAddress][c.remotePort] = c
	connCountOnIp = ip_connections[c.remoteAddress].ports.length
	if connCountOnIp > CONNECTIONS_PER_IP
		toClosePort = ip_connections[c.remoteAddress].ports[0]
		toCloseConnection = ip_connections[c.remoteAddress][toClosePort]
		logger.warn 'over '+CONNECTIONS_PER_IP+' connections from ' + c.remoteAddress + ', close ' + toClosePort
		toCloseConnection?.destroy()

	ret

removeConnection = (address, port) ->
	# od ip_connections
	debug 'remove ', address, ':', port
	delete ip_connections[address][port]
	arrayRemoveEle ip_connections[address].ports, port
	# od ip_connections


class MasterFSM extends EventEmitter
	constructor: (@connection) ->
		checkAddress @connection
		@setupEventEmitter()
		@setupConnection(@connection)
		@setupRequestFunctions()
		@setupOnlineTimer()

		@recvBuff = ''
		@reqQueue = {}

	setupEventEmitter: ->

	close: ->
		@connection.destroy()

	setupOnlineTimer: =>
		@_onlineTimer = setTimeout =>
			@close()
			debug('Can not recv online message in specific time!')
		, ONLINE_TIMEOUT

	online: (deviceId)->
		@deviceId = deviceId
		clearTimeout @_onlineTimer
		@setupTrafficTimer()

	setupTrafficTimer: =>
		clearTimeout @_trafficTimer if @_trafficTimer?
		@_trafficTimer = setTimeout =>
			@close()
			debug('Have not recv any data in specific time!')
			logger.info 'Have not recv any data in specific time! close it.'+@_sock_id
		, NO_TRAFFIC_TIMEOUT

	destroy: ->
		clearTimeout @_trafficTimer if @_trafficTimer?
		clearTimeout @_onlineTimer if @_onlineTimer?
		removeConnection @_remoteAddress, @_remotePort
		delete @connection.bc_device
		delete @connection
		@emit 'closed'
		setImmediate (em)->
			em.removeAllListeners()
			logger.debug('device close all event listeners.')
		, @

	# potential leak
	setupRequestFunctions: =>
		createFunc = (f) =>
			(argsAll...) =>
				[args..., cb] = argsAll
				req = f(args...)
				debug 'func args: ', args, ', cb:', cb
				@request req, cb
		for name, method of Dev.requestCmds
			@[name] = createFunc method
			debug 'setup function: ', name


	setupConnection: (connection) ->
		@_remoteAddress = connection.remoteAddress
		@_remotePort = connection.remotePort
		@_sock_id = connection.remoteAddress + ':' + connection.remotePort
		connection.bc_device = @

		connection.on 'data', (buf) =>
			@setupTrafficTimer()
			@onData buf

		connection.on 'timeout', ->
			debug 'timeout, we have to close it manually.'

		connection.on 'error', (Error) ->
			debug 'Error: ', Error
			logger.info 'socket error on ' + @_sock_id, Error

		connection.on 'end', =>
			debug 'closed by other side.'
			connection.destroy()
			# logger.info @_sock_id+' closed by other side'

		connection.on 'close', (had_error) =>
			debug 'closed with error: ', had_error
			logger.info @_sock_id+' closed with error:'+had_error
			_bc = connection.bc_device
			_bc.destroy() if _bc?
			debug 'MasterFSM deleted.'

	onData: (buf) ->
		debug 'recv: ', buf
		logger.debug @_sock_id+' recv: '+ buf
		if buf.length> BUFF_LEN
			@emit 'error', new Error('Buff over flow')
		else
			@recvBuff += buf
			if @recvBuff.length> BUFF_LEN
				@emit 'error', new Error('Buff over flow 2')
				@recvBuff = ''
			else
				debug 'recvBuff: ', @recvBuff
				@parseAndProcess()

	parseAndProcess: ->
		msg = @parseMessage()
		# debug 'recvBuff: ', @recvBuff
		@processMessage msg if msg?.Type?


	parseMessage: ->
		msg = Dev.execute @recvBuff
		if msg
			debug 'got message: '
			od msg
			@recvBuff = @recvBuff.substr(msg.len+1)

		msg

	processMessage: (msg) ->
		req = @reqQueue[msg.Type]
		@dev_info = msg.message.DevInfo
		if req
			clearTimeout req.timeoutObj
			delete @reqQueue[msg.Type]
			setImmediate req.callback, null, msg.message

		@emit msg.Type, msg.message

		@parseAndProcess()

	request: (req, cb) ->
		debug 'request: ', req
		resMsg = req.ResponseMsg ? req.Type
		delete req['ResponseMsg']
		reqData = 
			callback: cb
			data: req
			timeoutObj: setTimeout =>
				delete @reqQueue[resMsg]
				cb new Error('timeout'), null
			, REQ_TIMEOUT
		@sendRequest reqData

		@reqQueue[resMsg] = reqData

	sendRequest: (req) ->
		cmd = 
			DevInfo: @dev_info
			Payloads: [req.data]

		dataFrame = JSON.stringify(cmd) + Dev.MSG_SPLITTER
		debug 'send: ', dataFrame
		logger.info 'FSM send: ', dataFrame
		@connection.write dataFrame



newDevice = (connection) ->
	new MasterFSM(connection)

module.exports = 
	create: newDevice