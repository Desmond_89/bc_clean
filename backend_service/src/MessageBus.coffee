EM = require('events').EventEmitter
resources = require('./resources')
	# client = resources.getRedis();

debug = require('debug')('MB')

class MessageBus extends EM
	constructor: (@channel) ->
		client = resources.getRedis()
		client.on 'subscribe', (p, c) ->
			debug 'subscribed: ', p, ', count: ', c

		client.on 'message', (channel, message) =>
			debug message, ' got from ', channel
			@emit 'message', JSON.parse(message)
			
		client.subscribe @channel
		debug 'subscribe on channel: ', @channel

listen = (channel) ->
	new MessageBus(channel)

module.exports = 
	listen: listen
