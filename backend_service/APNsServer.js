// APNs push service here
process.title = 'apnsServer';
var MSG = require('./lib/RTMessage');
var resources = require('./lib/resources');
// var client = resources.getRedis();
var logger = resources.logger;

var debug = require('debug')('APNS');

var apn = require('apn');

require('./lib/processNotify');
logger.info('start apn server');

var options = {
	pfx: '/home/desmond/bc_clean/backend_service/cert/Pro_Certificates.p12',
	passphrase: 'Bosch1234',
	production: true
};
// cert.pem and key.pem in local dir
var apnConnection = new apn.Connection(options);

// todo: pop from APNs queue
function listenApnsQueue(handleApnsMessage) {
	// MSG.APNsQueue.pop(handleApnsMessage);
	MSG.APNsQueue.pop(function(err, apnsRequest) {
		setImmediate(handleApnsMessage,err, apnsRequest);
		setImmediate(listenApnsQueue, handleApnsMessage);
	});
}

function handlerApnsRequest(err, msgObj) {
	// todo: log err if err
	if(err)
		logger.warn('pop apns message error: ' + err);
	sendApnsMessage(msgObj.token, msgObj.payload);
}

function sendApnsMessage(token, apnsMessage) {
	var myDevice = new apn.Device(token);
	var note = new apn.Notification();

	// todo: change the following item
	note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
	// note.badge = 777;
	// note.sound = 'default';
	// note.alert = JSON.stringify(alert);
	note.payload = apnsMessage;

	apnConnection.pushNotification(note, myDevice);
}


// feedback from apns feedback service
function setupFeedback() {
	var options = {
	    "batchFeedback": true,
	    "interval": 300
	};

	var feedback = new apn.Feedback(options);
	feedback.on("feedback", function(devices) {
	    devices.forEach(function(item) {
	    	// todo:
				// Do something with item.device and item.time;
	    });
	});
}

listenApnsQueue(handlerApnsRequest);

logger.info('apn server started.');

